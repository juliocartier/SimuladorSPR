require 'mathn'
require 'csv'

class SensogramaController < ApplicationController

	def simulaCamada3

			@angulo_entrada = params[:anguloEntrada].to_f
			@angulo_saida = params[:anguloSaida].to_f
			@angulo_soma = params[:passo2].to_f
			
			@comprimento_onda = params['onda'].to_f

			@espessura1aux = params['espessuras'] || []
			#puts @espessura1aux

			@espessura1 = 0
			@espessura2 = 0
			@espessura3_Custom1 = 0
			@espessura3_Custom2 = 0
			@espessura3_Custom3 = 0

			if @espessura1aux.length == 2 then
			   @espessura1 = @espessura1aux[0].to_f
			   @espessura2 = @espessura1aux[1].to_f
			   puts 'entrei l'
			end

				@espessura3_Custom1 = params['EspessuraCustom1'].to_f
			    @espessura3_Custom2 = params['EspessuraCustom2'].to_f		 
			 	@espessura3_Custom3 = params['EspessuraCustom3'].to_f
	
			

				logger.info("SPR Criado".yellow)

				@refracao1 = calculoRefracao( params['elementos'] ? params['elementos'][0] : 0, @comprimento_onda)
				@refracao2 = calculoRefracao( params['elementos'] ? params['elementos'][1] : 0, @comprimento_onda)
			
				@refracao3_Custom1 = params['RefracaoCustom1'].to_f
			    @refracao3_Custom2 = params['RefracaoCustom2'].to_f
			    @refracao3_Custom3 = params['RefracaoCustom3'].to_f

				@tempo_InicioAux = params['tempoInicio'].to_f
				@tempo_TotalAux = params['tempoTotal'].to_f
				@tempo_PassoAux = params['tempoPasso'].to_f

				@tempo_InjecaoAux = params['tempoInjecao'].to_f
			    @tempo_InjecaoAux2 = params['tempoInjecao2'].to_f
			    @tempo_InjecaoAux3 = params['tempoInjecao3'].to_f
		
			@espessura1 = (0.000000001 * @espessura1)
			@espessura2 = (0.000000001 * @espessura2)
			@espessura3_Custom1 = (0.000000001 * @espessura3_Custom1)
			@espessura3_Custom2 = (0.000000001 * @espessura3_Custom2)
			@espessura3_Custom3 = (0.000000001 * @espessura3_Custom3)

			@valor = []
			@valor_Custom2 = []
			@valor_Custom3 = []
			@valor1 = []
			@valorSensograma = []
			@assimetria = []
			@assimetriaCustom2 = []
			@assimetriaCustom3 = []

				while @angulo_entrada < @angulo_saida do
					@angulo_th = ((@angulo_entrada * (Math::PI))/(180))
					@k0 = ((2 * (Math::PI))/(0.000000001 * @comprimento_onda))
					@n1 = @refracao1

					#Inicio do qj1
					#qj1 está correto
					@valor1_qj1 = Complex((@refracao1 * @refracao1))
					@valor2aux_qj1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj1 = (@valor2aux_qj1 * @valor3aux_qj1)
					@valor2_qj1 = Math.sqrt(@valor1_qj1 - @valor4aux_qj1)
					@qj1 = Complex(((@valor2_qj1)/(@valor1_qj1)))
					#puts  "Valor do Qj1 = #{@qj1}"
					#Final do qj1

					#Inicio do qj2
					#A variavel @valor6aux_qj2 está pegando a parte real de um numero Complexo do valor @valor5aux_qj2
					#A variavel @valor7aux_qj2 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj2
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj2 = @refracao2 * @refracao2
					@valor2aux_qj2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj2 = (@valor2aux_qj2 * @valor3aux_qj2)
					@valor5aux_qj2 = @valor1_qj2 - @valor4aux_qj2
					@valor6aux_qj2 = @valor5aux_qj2.real
					@valor7aux_qj2 = @valor5aux_qj2.imaginary
					@valor2_qj2 = sqrt2(@valor6aux_qj2,@valor7aux_qj2)
					@qj2 = ((@valor2_qj2)/(@valor1_qj2))
					#Final do qj2

					#Inicio do qj3 para a espessura 3 Custom 1 e refração 3 Custom 1
					#Essa parte da diminuição não foi preciso fazer a função
					@valor1_qj3_Custom1 = @refracao3_Custom1 * @refracao3_Custom1
					@valor2aux_qj3_Custom1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj3_Custom1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj3_Custom1 = ((@valor2aux_qj3_Custom1) * (@valor3aux_qj3_Custom1))
					@valor5aux_qj3_Custom1 = (@valor1_qj3_Custom1 - @valor4aux_qj3_Custom1)
					@valor6aux_qj3_Custom1 = @valor5aux_qj3_Custom1.real
					@valor7aux_qj3_Custom1 = @valor5aux_qj3_Custom1.imaginary
					@valor2_qj3_Custom1 = sqrt2(@valor6aux_qj3_Custom1,@valor7aux_qj3_Custom1)
					@qj3_Custom1 = ((@valor2_qj3_Custom1)/(@valor1_qj3_Custom1))
					#puts @qj3
					#Fim do qj3 para a espessura 3 Custom 1 e refração 3 Custom 1

					#Inicio do qj3 para a espessura 3 Custom 2 e refração 3 Custom 2
					#Essa parte da diminuição não foi preciso fazer a função
					@valor1_qj3_Custom2 = @refracao3_Custom2 * @refracao3_Custom2
					@valor2aux_qj3_Custom2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj3_Custom2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj3_Custom2 = ((@valor2aux_qj3_Custom2) * (@valor3aux_qj3_Custom2))
					@valor5aux_qj3_Custom2 = (@valor1_qj3_Custom2 - @valor4aux_qj3_Custom2)
					@valor6aux_qj3_Custom2 = @valor5aux_qj3_Custom2.real
					@valor7aux_qj3_Custom2 = @valor5aux_qj3_Custom2.imaginary
					@valor2_qj3_Custom2 = sqrt2(@valor6aux_qj3_Custom2,@valor7aux_qj3_Custom2)
					@qj3_Custom2 = ((@valor2_qj3_Custom2)/(@valor1_qj3_Custom2))
					#puts @qj3
					#Fim do qj3 para a espessura 3 Custom 2 e refração 3 Custom 2

					#Inicio do qj3 para a espessura 3 Custom 3 e refração 3 Custom 3
					#Essa parte da diminuição não foi preciso fazer a função
					@valor1_qj3_Custom3 = @refracao3_Custom3 * @refracao3_Custom3
					@valor2aux_qj3_Custom3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj3_Custom3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj3_Custom3 = ((@valor2aux_qj3_Custom3) * (@valor3aux_qj3_Custom3))
					@valor5aux_qj3_Custom3 = (@valor1_qj3_Custom3 - @valor4aux_qj3_Custom3)
					@valor6aux_qj3_Custom3 = @valor5aux_qj3_Custom3.real
					@valor7aux_qj3_Custom3 = @valor5aux_qj3_Custom3.imaginary
					@valor2_qj3_Custom3 = sqrt2(@valor6aux_qj3_Custom3,@valor7aux_qj3_Custom3)
					@qj3_Custom3 = ((@valor2_qj3_Custom3)/(@valor1_qj3_Custom3))
					#puts @qj3
					#Fim do qj3 para a espessura 3 Custom 3 e refração 3 Custom 3

					#beta2 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta2 = Complex((@k0 * @espessura2))
					@valor1aux_beta2 = Complex(@refracao2 * @refracao2)
					@valor2aux_beta2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta2 = ((@valor2aux_beta2) * (@valor3aux_beta2))
					@valor5aux_beta2 = ((@valor1aux_beta2) - (@valor4aux_beta2))
					@valor6aux_beta2 = @valor5aux_beta2.real
					@valor7aux_beta2 = @valor5aux_beta2.imaginary
					@valor2beta2 = sqrt2(@valor6aux_beta2,@valor7aux_beta2)
					@beta2 = ((@valor1beta2) * (@valor2beta2))


					@i = (Complex(0,-1))
					
					@M2 = [[],[]]
					@M2[0][0] = Complex((Math.cos(@beta2)))
					@M2[0][1] = Complex(((@i * (Math.sin(@beta2)))/(@qj2)))
					@M2[1][0] = Complex(((@i * (Math.sin(@beta2)))*(@qj2)))
					@M2[1][1] = Complex((Math.cos(@beta2)))

					#Primeiro calculo da soma de matriz do coeficiente de fresnel para a espessura3_Custom1
					#Que é o qj3_Custom1
					@r_n = ((((@M2[0][1] * @qj3_Custom1) + @M2[0][0]) * @qj1) - ((@M2[1][1] * @qj3_Custom1) + (@M2[1][0])))
					@r_d = ((((@M2[0][1] * @qj3_Custom1) + @M2[0][0]) * @qj1) + ((@M2[1][1] * @qj3_Custom1) + (@M2[1][0])))
					#Segundo calculo da soma de matriz do coeficiente de fresnel para a espessura3_Custom2
					#Que é o qj3_Custom2
					@r_n_custom2 = ((((@M2[0][1] * @qj3_Custom2) + @M2[0][0]) * @qj1) - ((@M2[1][1] * @qj3_Custom2) + (@M2[1][0])))
					@r_d_custom2 = ((((@M2[0][1] * @qj3_Custom2) + @M2[0][0]) * @qj1) + ((@M2[1][1] * @qj3_Custom2) + (@M2[1][0])))
					#Terceiro calculo da soma de matriz do coeficiente de fresnel para a espessura3_Custom3
					#Que é o qj3_Custom3
					@r_n_custom3 = ((((@M2[0][1] * @qj3_Custom3) + @M2[0][0]) * @qj1) - ((@M2[1][1] * @qj3_Custom3) + (@M2[1][0])))
					@r_d_custom3 = ((((@M2[0][1] * @qj3_Custom3) + @M2[0][0]) * @qj1) + ((@M2[1][1] * @qj3_Custom3) + (@M2[1][0])))

					#Divisão do resultado @r_n/@r_d
					@r = Complex(((@r_n)/(@r_d)))

					#puts @r

					#Divisão do resultado @r_n_custom2/@r_d_custom2
					@r_Custom2 = Complex(((@r_n_custom2)/(@r_d_custom2)))

					#puts @r_Custom2
					#Divisão do resultado @r_n_custom3/@r_d_custom3
					@r_Custom3 = Complex(((@r_n_custom3)/(@r_d_custom3)))

					#puts @r_Custom3

					#valor de r da espessura3_custom1
					@valor_r = Complex(((@r * (@r.conjugate))).real)

					#valor de r da espessura3_custom2
					@valor_r_custom2 = Complex(((@r_Custom2 * (@r_Custom2.conjugate))).real)

					#valor de r da espessura3_custom3
					@valor_r_custom3 = Complex(((@r_Custom3 * (@r_Custom3.conjugate))).real)

					@valor << { :id => 'io', :x => @angulo_entrada, :y => @valor_r}

					@valor_Custom2 << { :id => 'io', :x => @angulo_entrada, :y => @valor_r_custom2}

					@valor_Custom3 << { :id => 'io', :x => @angulo_entrada, :y => @valor_r_custom3}

					@angulo_entrada += @angulo_soma
					
					@assimetria <<  {"r" => @valor_r, "angulo" => @angulo_entrada}        
				   	@assimetriaCustom2 << {"r" => @valor_r_custom2, "angulo" => @angulo_entrada}
				   	@assimetriaCustom3 << {"r" => @valor_r_custom3, "angulo" => @angulo_entrada}
				   
					#puts @assimetriaCustom2

				end

					#Angulo Minimo
				  @angulo = @assimetria.min_by { |x| x["r"] }
				  	@anguloCustom2 = @assimetriaCustom2.min_by {|angCustom2| angCustom2["r"] }
						@anguloCustom3 = @assimetriaCustom3.min_by {|angCustom3| angCustom3["r"] }
					   if @angulo != nil || @anguloCustmo2 != nil || @anguloCustom3 != nil then 
					   	
					   	@angulo_min = @angulo["angulo"]
					   	@angleCustom2 = @anguloCustom2["angulo"]
				   	    @angleCustom3 = @anguloCustom3["angulo"]
				   	   
				   	    end
				   	    /
				   	    	Aqui começa o inicio do sensograma com o tempo inicial
				   	    		ele faz uma verificação se os tempos eles são diferentes de 0.0, se o tempo
				   	    		for diferente de 0 ele começa a incrementação da primeira substancia
				   	    		com a variavel @EspessuraCustom1 e RefracaoCustom1. Começa com o tempo de inicio e vai 
				   	    		até o tempo_InjecaoAux
				   	    /
						   	if @tempo_InicioAux != 0.0 && @tempo_PassoAux != 0.0 && @tempo_InjecaoAux != 0.0 then
									while @tempo_InicioAux <= @tempo_InjecaoAux do
												@valorSensograma << {:id => 'io', :x => @tempo_InicioAux, :y => @angulo_min}	
										@tempo_InicioAux += @tempo_PassoAux
										#puts @valorSensograma
									end

									
									/
									Essas duas variaveis são para receber os dois angulos minimos que tem,
									O primeiro angulo minimo é o resultado da @EspessuraCustom1 e @RefracaoCustom1
									
									O Segundo Angulo Minimo denominado @anguloAuxMenor ele recebe os valores
									da @EspessuraCustom2 e @RefracaoCustom2
									/
										@anguloAuxMaior = @angulo["angulo"]
										@anguloAuxMenor = @anguloCustom2["angulo"]
									#Essa variavel @tempo_Injecao2 recebe o valor do Tempo_InjecaoAux
									@tempo_Injecao2 = @tempo_InjecaoAux
									#Essa variavel @tempoTotal2 recebe o Tempo total menos o tempo de injeção 
									#3 da terceira substancia
									@tempoTotal2 = @tempo_TotalAux - @tempo_InjecaoAux3	
										#puts @tempoTotal2
								
								    /
									 Aqui começa uma decrementação do angulo ou incrementação de acordo com
									 angulo minimo que é o resultado das variaveis @anguloAuxMaior e @anguloAuxMenor
									 Faz a verificação do Angulo se ele é maior ou menor 	
									/	
									@auxInjecao2 = @tempo_Injecao2
								 if @anguloAuxMaior >= @anguloAuxMenor then
									while @tempo_Injecao2 <= @auxInjecao2 do	
										while @anguloAuxMaior >= @anguloAuxMenor do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao2, :y => @anguloAuxMaior}
											#puts @valorSensograma
											 @anguloAuxMaior -= 0.1
											 @auxInjecao2 = @tempo_Injecao2
											@tempo_Injecao2 += @tempo_PassoAux
										end
									end
								elsif 
									while @tempo_Injecao2 <= @auxInjecao2 do	
										while @anguloAuxMaior <= @anguloAuxMenor do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao2, :y => @anguloAuxMaior}
											#puts @valorSensograma
											 @anguloAuxMaior += 0.1
											 @auxInjecao2 = @tempo_Injecao2
											@tempo_Injecao2 += @tempo_PassoAux
										end
									end
								 end
										
									/Faz um while do tempo aonde ele parou e vai até o tempoTotal2 = @tempo_TotalAux - @tempo_InjecaoAux3
										fazendo a inserção no sensograma.
									/
									while @tempo_Injecao2 <= @tempoTotal2 do
												@valorSensograma << {:id => 'io', :x => @tempo_Injecao2, :y => @angleCustom2}	
										@tempo_Injecao2 += @tempo_PassoAux
										#puts @valorSensograma
									end


									/Essa variavel @tempo_Injecao3 ela recebe o tempo @tempo_Injecao2 aonde parou
									/
									@tempo_Injecao3 = @tempo_Injecao2



									/Essas duas variaveis ela recebe os dois angulos minimos/
									@anguloAuxMaior2 = @anguloCustom2["angulo"]
									@anguloAuxMenor2 = @anguloCustom3["angulo"]
									/Fazendo a verficação para saber se é maior ou menor
									A partir dai, faz uma incrementação do angulo ou decrementação
									/
									@auxInjecao3 = @tempo_Injecao3
								 if @anguloAuxMaior2 >= @anguloAuxMenor2 then
									while @tempo_Injecao3 <= @auxInjecao3 do	
										while @anguloAuxMaior2 >= @anguloAuxMenor2 do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao3, :y => @anguloAuxMaior2}
											#puts @valorSensograma
											 @anguloAuxMaior2 -= 0.1
											 @auxInjecao3 = @tempo_Injecao3
											@tempo_Injecao3 += @tempo_PassoAux
										end
									end
								elsif 
									while @tempo_Injecao3 <= @auxInjecao3 do	
										while @anguloAuxMaior2 <= @anguloAuxMenor2 do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao3, :y => @anguloAuxMaior2}
											#puts @valorSensograma
											 @anguloAuxMaior2 += 0.1
											 @auxInjecao3 = @tempo_Injecao3
											@tempo_Injecao3 += @tempo_PassoAux
										end
									end
								 end

									#puts @tempo_Injecao3
									/Essa parte faz a inserção no sensograma até o tempo Total/
									while @tempo_Injecao3 <= @tempo_TotalAux do
												@valorSensograma << {:id => 'io', :x => @tempo_Injecao3, :y => @angleCustom3}	
											@tempo_Injecao3 += @tempo_PassoAux

										#puts @valorSensograma
									end
		                     end

		                     
					   

					
					 @teta_max = @assimetria.max_by { |z| z["r"] }
					 @teta_min = @assimetria.min_by { |y| y["r"] }
					 @teta_maxCustom2 = @assimetriaCustom2.max_by { |custom2max| custom2max["r"] }
					 @teta_minCustom2 = @assimetriaCustom2.min_by { |custom2min| custom2min["r"] }
					 @teta_maxCustom3 = @assimetriaCustom3.max_by { |custom3max| custom3max["r"] }
					 @teta_minCustom3 = @assimetriaCustom3.min_by { |custom3min| custom3min["r"] }
					  if (@teta_max != nil && @teta_min != nil) || (@teta_maxCustom2 != nil && @teta_minCustom2 != nil) || (@teta_maxCustom3 != nil && @teta_minCustom3 != nil) then 
						@teta_m1 = @teta_max["r"]
						@teta_m2 = @teta_min["r"]

						@tetaCustom2M1 = @teta_maxCustom2["r"]
						@tetaCustom2M2 = @teta_minCustom2["r"]

						@tetaCustom3M1 = @teta_maxCustom3["r"]
						@tetaCustom3M2 = @teta_minCustom3["r"]

						@teta_medio = ((@teta_m1 + @teta_m2)/2).round(2)
						@teta_medioCustom2 = ((@tetaCustom2M1 + @tetaCustom2M2)/2).round(2)
						@teta_medioCustom3 = ((@tetaCustom3M1 + @tetaCustom3M2)/2).round(2)
						
						@teta_aux_v1 = (@teta_min["angulo"]).round(2)
						@teta_aux_v1Custom2 = (@teta_minCustom2["angulo"]).round(2)
						@teta_aux_v1Custom3 = (@teta_minCustom3["angulo"]).round(2)

						/Variaveis começando em 0 para pegar os valores da curva
						como angulo minimo, assimetria e largura da curva/
						@v1 = 0
						@v2 = 0
						@v1Custom2 = 0
						@v2Custom2 = 0
						@v1Custom3 = 0
						@v2Custom3 = 0
						@aux = 0
						@auxCustom2 = 0
						@auxCustom3 = 0
						@vetor = []
						@vetorCustom2 = []
						@vetorCustom3 = []
						@vetor1 = []
						@vetor1Custom2 = []
						@vetor1Custom3 = []

						#For do Hash aonde tem o meus valores de R e o angulo
						#A partir dai vou pegar o meu @valor medio e procurar o 
						# minino do meus angulos
						@assimetria.each do |a|
						  if a["r"] <= @teta_medio then
							if @aux == 0 then
								@v1 = a["angulo"]
							    @aux += 1
							 else
								@v2 = a["angulo"]
							end
						   end
						end

						#For do Hash aonde tem o meus valores de R e o angulo
						#A partir dai vou pegar o meu @valor medio e procurar o 
						# minino do meus angulos dos valores da assimetriaCustom2
						@assimetriaCustom2.each do |custom2|
							if custom2["r"] <= @teta_medioCustom2 then
								if @auxCustom2 == 0 then
									@v1Custom2 = custom2["angulo"]
									@auxCustom2 += 1
								else
									@v2Custom2 = custom2["angulo"]
								 end
							end
						end

						#For do Hash aonde tem o meus valores de R e o angulo
						#A partir dai vou pegar o meu @valor medio e procurar o 
						# minino do meus angulos dos valores da assimetriaCustom3
						@assimetriaCustom3.each do |custom3|
							if custom3["r"] <= @teta_medioCustom3 then
								if @auxCustom3 == 0 then
									@v1Custom3 = custom3["angulo"]
									@auxCustom3 += 1
								else
									@v2Custom3 = custom3["angulo"]
								 end
							end
						end

						#@assimetria.each do |valorR|
						    

						#end

						# if @tempo_InicioAux != nil then
					 	  #while @tempo_InicioAux <= @tempo_InjecaoAux do
					   	  #	@valorSensograma << {"r" => @valor_r, "tempo" => @tempo_InjecaoAux}
					   	  #	@tempo_InjecaoAux += @tempo_PassoAux
					   	 # end
					# end

						#puts "O valor do V1Custom3 = #{@v1Custom3}"
						#puts @v2Custom3
						/Calculo da assimetria e largura
						a variavel @vetor ele está fazendo o calculo da largura
						e @vetor1 está fazendo o calculo da assimetria/
						@vetor = ((@v1 - @v2).abs).round(4)
						@vetor1 = ((@v1/@v2).abs).round(4)

						@vetorCustom2 = ((@v1Custom2 - @v2Custom2).abs).round(4)
						@vetor1Custom2 = ((@v1Custom2/@v2Custom2).abs).round(4)

						@vetorCustom3 = ((@v1Custom3 - @v2Custom3).abs).round(4)
						@vetor1Custom3 = ((@v1Custom3/@v2Custom3).abs).round(4)

						#puts @vetor1Custom3  
					
						@valor_aux = Complex((@n1) * (Math.sin(@angulo_th)))
						@valor_aux1 = Complex((@n1) * (Math.sin(@angulo_th)))
						@aux = @valor_aux * @valor_aux1
						@indice_aux = (@refracao2 * @refracao2).real
						#puts @aux   

						@n3 = Math.sqrt(((@indice_aux*@aux)/(@indice_aux-@aux))).round(4)
						#puts @n3 
					   end
					 
					result = Hash.new
					#result["chartAIM"] = @valor
					result["IRCustom1"] = @refracao3_Custom1
					result["IRCustom2"] = @refracao3_Custom2
					result["IRCustom3"] = @refracao3_Custom3
					result["chartSensograma"] = @valorSensograma
					result["valor_medio"] = @teta_medio
					result["valor_medioCustom2"] = @teta_medioCustom2
					result["valor_medioCustom3"] = @teta_medioCustom3
					result["angulo_minimo"] = "#{@teta_aux_v1} º"
					result["angulo_minimoCustom2"] = "#{@teta_aux_v1Custom2} º"
					result["angulo_minimoCustom3"] = "#{@teta_aux_v1Custom3} º"
					result["largura_certo"] = "#{@vetor} º"
					result["largura_certoCustom2"] = "#{@vetorCustom2} º"
					result["largura_certoCustom3"] = "#{@vetorCustom2} º"
					result["assimetria_certo"] = @vetor1
					result["assimetria_certoCustom2"] = @vetor1Custom2
					result["assimetria_certoCustom3"] = @vetor1Custom3
					result["indiceRefracaoCerto"] = @n3

					respond_to do |format|
					 # format.html { grafico }
					  format.html { graficoSensograma }
					  format.js { render :json => result }
					end
		 
	end

	def simulaCamada4

			@angulo_entrada = params[:anguloEntrada].to_f
			@angulo_saida = params[:anguloSaida].to_f
			@angulo_soma = params[:passo2].to_f
			
			@comprimento_onda = params['onda'].to_f

			@espessura1aux = params['espessuras'] || []
			#puts @espessura1aux

			@espessura1 = 0
			@espessura2 = 0
			@espessura3 = 0
			@espessura4_Custom1 = 0
			@espessura4_Custom2 = 0
			@espessura4_Custom3 = 0

			

			if @espessura1aux.length == 3 then
			   @espessura1 = @espessura1aux[0].to_f
			   @espessura2 = @espessura1aux[1].to_f
			   @espessura3 = @espessura1aux[2].to_f
			   puts 'entrei l'
			end

				@espessura4_Custom1 = params['EspessuraCustom1'].to_f
			    @espessura4_Custom2 = params['EspessuraCustom2'].to_f		 
			 	@espessura4_Custom3 = params['EspessuraCustom3'].to_f
				
				puts @espessura1
				puts @espessura2
				puts @espessura3
				puts @espessura4_Custom1
				puts @espessura4_Custom2
				puts @espessura4_Custom3
			

				logger.info("SPR Criado".yellow)

				@refracao1 = calculoRefracao( params['elementos'] ? params['elementos'][0] : 0, @comprimento_onda)
				@refracao2 = calculoRefracao( params['elementos'] ? params['elementos'][1] : 0, @comprimento_onda)
				@refracao3 = calculoRefracao( params['elementos'] ? params['elementos'][2] : 0, @comprimento_onda)
			
				@refracao4_Custom1 = params['RefracaoCustom1'].to_f
			    @refracao4_Custom2 = params['RefracaoCustom2'].to_f
			    @refracao4_Custom3 = params['RefracaoCustom3'].to_f

			    puts @refracao1
				puts @refracao2
				puts @refracao3
				puts @refracao4_Custom1
				puts @refracao4_Custom2
				puts @refracao4_Custom3

				@tempo_InicioAux = params['tempoInicio'].to_f
				@tempo_TotalAux = params['tempoTotal'].to_f
				@tempo_PassoAux = params['tempoPasso'].to_f

				@tempo_InjecaoAux = params['tempoInjecao'].to_f
			    @tempo_InjecaoAux2 = params['tempoInjecao2'].to_f
			    @tempo_InjecaoAux3 = params['tempoInjecao3'].to_f
		
			@espessura1 = (0.000000001 * @espessura1)
			@espessura2 = (0.000000001 * @espessura2)
			@espessura3 = (0.000000001 * @espessura3)
			@espessura4_Custom1 = (0.000000001 * @espessura4_Custom1)
			@espessura4_Custom2 = (0.000000001 * @espessura4_Custom2)
			@espessura4_Custom3 = (0.000000001 * @espessura4_Custom3)

			@valor = []
			@valor_Custom2 = []
			@valor_Custom3 = []
			@valor1 = []
			@valorSensograma = []
			@assimetria = []
			@assimetriaCustom2 = []
			@assimetriaCustom3 = []

				while @angulo_entrada < @angulo_saida do
					@angulo_th = ((@angulo_entrada * (Math::PI))/(180))
					@k0 = ((2 * (Math::PI))/(0.000000001 * @comprimento_onda))
					@n1 = @refracao1

					#Inicio do qj1
					#qj1 está correto
					@valor1_qj1 = Complex((@refracao1 * @refracao1))
					@valor2aux_qj1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj1 = (@valor2aux_qj1 * @valor3aux_qj1)
					@valor2_qj1 = Math.sqrt(@valor1_qj1 - @valor4aux_qj1)
					@qj1 = Complex(((@valor2_qj1)/(@valor1_qj1)))
					#puts  "Valor do Qj1 = #{@qj1}"
					#Final do qj1

					#Inicio do qj2
					#A variavel @valor6aux_qj2 está pegando a parte real de um numero Complexo do valor @valor5aux_qj2
					#A variavel @valor7aux_qj2 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj2
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj2 = @refracao2 * @refracao2
					@valor2aux_qj2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj2 = (@valor2aux_qj2 * @valor3aux_qj2)
					@valor5aux_qj2 = @valor1_qj2 - @valor4aux_qj2
					@valor6aux_qj2 = @valor5aux_qj2.real
					@valor7aux_qj2 = @valor5aux_qj2.imaginary
					@valor2_qj2 = sqrt2(@valor6aux_qj2,@valor7aux_qj2)
					@qj2 = ((@valor2_qj2)/(@valor1_qj2))
					#Final do qj2

					#Inicio do qj3
					#A variavel @valor6aux_qj3 está pegando a parte real de um numero Complexo do valor @valor5aux_qj3
					#A variavel @valor7aux_qj3 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj3
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj3 = @refracao3 * @refracao3
					@valor2aux_qj3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj3 = (@valor2aux_qj3 * @valor3aux_qj3)
					@valor5aux_qj3 = @valor1_qj3 - @valor4aux_qj3
					@valor6aux_qj3 = @valor5aux_qj3.real
					@valor7aux_qj3 = @valor5aux_qj3.imaginary
					@valor2_qj3 = sqrt2(@valor6aux_qj3,@valor7aux_qj3)
					@qj3 = ((@valor2_qj3)/(@valor1_qj3))
					#Final do qj3

					#Inicio do qj4 para a espessura 3 Custom 1 e refração 3 Custom 1
					#Essa parte da diminuição não foi preciso fazer a função
					@valor1_qj4_Custom1 = @refracao4_Custom1 * @refracao4_Custom1
					@valor2aux_qj4_Custom1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj4_Custom1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj4_Custom1 = ((@valor2aux_qj4_Custom1) * (@valor3aux_qj4_Custom1))
					@valor5aux_qj4_Custom1 = (@valor1_qj4_Custom1 - @valor4aux_qj4_Custom1)
					@valor6aux_qj4_Custom1 = @valor5aux_qj4_Custom1.real
					@valor7aux_qj4_Custom1 = @valor5aux_qj4_Custom1.imaginary
					@valor2_qj4_Custom1 = sqrt2(@valor6aux_qj4_Custom1,@valor7aux_qj4_Custom1)
					@qj4_Custom1 = ((@valor2_qj4_Custom1)/(@valor1_qj4_Custom1))
					#puts @qj4
					#Fim do qj4 para a espessura 3 Custom 1 e refração 3 Custom 1

					#Inicio do qj4 para a espessura 3 Custom 2 e refração 3 Custom 2
					#Essa parte da diminuição não foi preciso fazer a função
					@valor1_qj4_Custom2 = @refracao4_Custom2 * @refracao4_Custom2
					@valor2aux_qj4_Custom2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj4_Custom2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj4_Custom2 = ((@valor2aux_qj4_Custom2) * (@valor3aux_qj4_Custom2))
					@valor5aux_qj4_Custom2 = (@valor1_qj4_Custom2 - @valor4aux_qj4_Custom2)
					@valor6aux_qj4_Custom2 = @valor5aux_qj4_Custom2.real
					@valor7aux_qj4_Custom2 = @valor5aux_qj4_Custom2.imaginary
					@valor2_qj4_Custom2 = sqrt2(@valor6aux_qj4_Custom2,@valor7aux_qj4_Custom2)
					@qj4_Custom2 = ((@valor2_qj4_Custom2)/(@valor1_qj4_Custom2))
					#puts @qj4
					#Fim do qj4 para a espessura 3 Custom 2 e refração 3 Custom 2

					#Inicio do qj4 para a espessura 3 Custom 3 e refração 3 Custom 3
					#Essa parte da diminuição não foi preciso fazer a função
					@valor1_qj4_Custom3 = @refracao4_Custom3 * @refracao4_Custom3
					@valor2aux_qj4_Custom3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj4_Custom3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj4_Custom3 = ((@valor2aux_qj4_Custom3) * (@valor3aux_qj4_Custom3))
					@valor5aux_qj4_Custom3 = (@valor1_qj4_Custom3 - @valor4aux_qj4_Custom3)
					@valor6aux_qj4_Custom3 = @valor5aux_qj4_Custom3.real
					@valor7aux_qj4_Custom3 = @valor5aux_qj4_Custom3.imaginary
					@valor2_qj4_Custom3 = sqrt2(@valor6aux_qj4_Custom3,@valor7aux_qj4_Custom3)
					@qj4_Custom3 = ((@valor2_qj4_Custom3)/(@valor1_qj4_Custom3))
					#puts @qj4
					#Fim do qj4 para a espessura 3 Custom 3 e refração 3 Custom 3

					#beta2 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta2 = Complex((@k0 * @espessura2))
					@valor1aux_beta2 = Complex(@refracao2 * @refracao2)
					@valor2aux_beta2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta2 = ((@valor2aux_beta2) * (@valor3aux_beta2))
					@valor5aux_beta2 = ((@valor1aux_beta2) - (@valor4aux_beta2))
					@valor6aux_beta2 = @valor5aux_beta2.real
					@valor7aux_beta2 = @valor5aux_beta2.imaginary
					@valor2beta2 = sqrt2(@valor6aux_beta2,@valor7aux_beta2)
					@beta2 = ((@valor1beta2) * (@valor2beta2))

					#beta3 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta3 = Complex((@k0 * @espessura3))
					@valor1aux_beta3 = Complex(@refracao3 * @refracao3)
					@valor2aux_beta3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta3 = ((@valor2aux_beta3) * (@valor3aux_beta3))
					@valor5aux_beta3 = ((@valor1aux_beta3) - (@valor4aux_beta3))
					@valor6aux_beta3 = @valor5aux_beta3.real
					@valor7aux_beta3 = @valor5aux_beta3.imaginary
					@valor2beta3 = sqrt2(@valor6aux_beta3,@valor7aux_beta3)
					@beta3 = ((@valor1beta3) * (@valor2beta3))


					@i = (Complex(0,-1))
					
					@M2 = [[],[]]
					@M2[0][0] = Complex((Math.cos(@beta2)))
					@M2[0][1] = Complex(((@i * (Math.sin(@beta2)))/(@qj2)))
					@M2[1][0] = Complex(((@i * (Math.sin(@beta2)))*(@qj2)))
					@M2[1][1] = Complex((Math.cos(@beta2)))

					@M3 = [[],[]]
					@M3[0][0] = Complex((Math.cos(@beta3)))
					@M3[0][1] = Complex(((@i * (Math.sin(@beta3)))/(@qj3)))
					@M3[1][0] = Complex(((@i * (Math.sin(@beta3)))*(@qj3)))
					@M3[1][1] = Complex((Math.cos(@beta3)))

					@M4 = [[],[]]
					@M4[0][0] = Complex(((@M2[0][0] * @M3[0][0]) + (@M2[0][1] * @M3[1][0])))
					@M4[0][1] = Complex(((@M2[0][0] * @M3[0][1]) + (@M2[0][1] * @M3[1][1])))
					@M4[1][0] = Complex(((@M2[1][0] * @M3[0][0]) + (@M2[1][1] * @M3[1][0])))
					@M4[1][1] = Complex(((@M2[1][0] * @M3[0][1]) + (@M2[1][1] * @M3[1][1])))

					#Primeiro calculo da soma de matriz do coeficiente de fresnel para a espessura3_Custom1
					#Que é o qj3_Custom1
					@r_n = ((((@M4[0][1] * @qj4_Custom1) + @M4[0][0]) * @qj1) - ((@M4[1][1] * @qj4_Custom1) + (@M4[1][0])))
					@r_d = ((((@M4[0][1] * @qj4_Custom1) + @M4[0][0]) * @qj1) + ((@M4[1][1] * @qj4_Custom1) + (@M4[1][0])))
					#Segundo calculo da soma de matriz do coeficiente de fresnel para a espessura3_Custom2
					#Que é o qj3_Custom2
					@r_n_custom2 = ((((@M4[0][1] * @qj4_Custom2) + @M4[0][0]) * @qj1) - ((@M4[1][1] * @qj4_Custom2) + (@M4[1][0])))
					@r_d_custom2 = ((((@M4[0][1] * @qj4_Custom2) + @M4[0][0]) * @qj1) + ((@M4[1][1] * @qj4_Custom2) + (@M4[1][0])))
					#Terceiro calculo da soma de matriz do coeficiente de fresnel para a espessura3_Custom3
					#Que é o qj3_Custom3
					@r_n_custom3 = ((((@M4[0][1] * @qj4_Custom3) + @M4[0][0]) * @qj1) - ((@M4[1][1] * @qj4_Custom3) + (@M4[1][0])))
					@r_d_custom3 = ((((@M4[0][1] * @qj4_Custom3) + @M4[0][0]) * @qj1) + ((@M4[1][1] * @qj4_Custom3) + (@M4[1][0])))

					#Divisão do resultado @r_n/@r_d
					@r = Complex(((@r_n)/(@r_d)))

					#puts @r

					#Divisão do resultado @r_n_custom2/@r_d_custom2
					@r_Custom2 = Complex(((@r_n_custom2)/(@r_d_custom2)))

					#puts @r_Custom2
					#Divisão do resultado @r_n_custom3/@r_d_custom3
					@r_Custom3 = Complex(((@r_n_custom3)/(@r_d_custom3)))

					#puts @r_Custom3

					#valor de r da espessura3_custom1
					@valor_r = Complex(((@r * (@r.conjugate))).real)

					#valor de r da espessura3_custom2
					@valor_r_custom2 = Complex(((@r_Custom2 * (@r_Custom2.conjugate))).real)

					#valor de r da espessura3_custom3
					@valor_r_custom3 = Complex(((@r_Custom3 * (@r_Custom3.conjugate))).real)

					@valor << { :id => 'io', :x => @angulo_entrada, :y => @valor_r}

					@valor_Custom2 << { :id => 'io', :x => @angulo_entrada, :y => @valor_r_custom2}

					@valor_Custom3 << { :id => 'io', :x => @angulo_entrada, :y => @valor_r_custom3}

					@angulo_entrada += @angulo_soma
					
					@assimetria <<  {"r" => @valor_r, "angulo" => @angulo_entrada}        
				   	@assimetriaCustom2 << {"r" => @valor_r_custom2, "angulo" => @angulo_entrada}
				   	@assimetriaCustom3 << {"r" => @valor_r_custom3, "angulo" => @angulo_entrada}
				   
				end

					#Angulo Minimo
				  @angulo = @assimetria.min_by { |x| x["r"] }
				  	@anguloCustom2 = @assimetriaCustom2.min_by {|angCustom2| angCustom2["r"] }
						@anguloCustom3 = @assimetriaCustom3.min_by {|angCustom3| angCustom3["r"] }
					   if @angulo != nil || @anguloCustmo2 != nil || @anguloCustom3 != nil then 
					   	
					   	@angulo_min = @angulo["angulo"]
					   	@angleCustom2 = @anguloCustom2["angulo"]
				   	    @angleCustom3 = @anguloCustom3["angulo"]
				   	   
				   	    end
				   	    /
				   	    	Aqui começa o inicio do sensograma com o tempo inicial
				   	    		ele faz uma verificação se os tempos eles são diferentes de 0.0, se o tempo
				   	    		for diferente de 0 ele começa a incrementação da primeira substancia
				   	    		com a variavel @EspessuraCustom1 e RefracaoCustom1. Começa com o tempo de inicio e vai 
				   	    		até o tempo_InjecaoAux
				   	    /
						   	if @tempo_InicioAux != 0.0 && @tempo_PassoAux != 0.0 && @tempo_InjecaoAux != 0.0 then
									while @tempo_InicioAux <= @tempo_InjecaoAux do
												@valorSensograma << {:id => 'io', :x => @tempo_InicioAux, :y => @angulo_min}	
										@tempo_InicioAux += @tempo_PassoAux
										#puts @valorSensograma
									end

									
									/
									Essas duas variaveis são para receber os dois angulos minimos que tem,
									O primeiro angulo minimo é o resultado da @EspessuraCustom1 e @RefracaoCustom1
									
									O Segundo Angulo Minimo denominado @anguloAuxMenor ele recebe os valores
									da @EspessuraCustom2 e @RefracaoCustom2
									/
										@anguloAuxMaior = @angulo["angulo"]
										@anguloAuxMenor = @anguloCustom2["angulo"]
									#Essa variavel @tempo_Injecao2 recebe o valor do Tempo_InjecaoAux
									@tempo_Injecao2 = @tempo_InjecaoAux
									#Essa variavel @tempoTotal2 recebe o Tempo total menos o tempo de injeção 
									#3 da terceira substancia
									@tempoTotal2 = @tempo_TotalAux - @tempo_InjecaoAux3	
										#puts @tempoTotal2
								
								    /
									 Aqui começa uma decrementação do angulo ou incrementação de acordo com
									 angulo minimo que é o resultado das variaveis @anguloAuxMaior e @anguloAuxMenor
									 Faz a verificação do Angulo se ele é maior ou menor 	
									/	
									@auxInjecao2 = @tempo_Injecao2
								 if @anguloAuxMaior >= @anguloAuxMenor then
									while @tempo_Injecao2 <= @auxInjecao2 do	
										while @anguloAuxMaior >= @anguloAuxMenor do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao2, :y => @anguloAuxMaior}
											#puts @valorSensograma
											 @anguloAuxMaior -= 0.1
											 @auxInjecao2 = @tempo_Injecao2
											@tempo_Injecao2 += @tempo_PassoAux
										end
									end
								elsif 
									while @tempo_Injecao2 <= @auxInjecao2 do	
										while @anguloAuxMaior <= @anguloAuxMenor do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao2, :y => @anguloAuxMaior}
											#puts @valorSensograma
											 @anguloAuxMaior += 0.1
											 @auxInjecao2 = @tempo_Injecao2
											@tempo_Injecao2 += @tempo_PassoAux
										end
									end
								 end
										
									/Faz um while do tempo aonde ele parou e vai até o tempoTotal2 = @tempo_TotalAux - @tempo_InjecaoAux3
										fazendo a inserção no sensograma.
									/
									while @tempo_Injecao2 <= @tempoTotal2 do
												@valorSensograma << {:id => 'io', :x => @tempo_Injecao2, :y => @angleCustom2}	
										@tempo_Injecao2 += @tempo_PassoAux
										#puts @valorSensograma
									end


									/Essa variavel @tempo_Injecao3 ela recebe o tempo @tempo_Injecao2 aonde parou
									/
									@tempo_Injecao3 = @tempo_Injecao2



									/Essas duas variaveis ela recebe os dois angulos minimos/
									@anguloAuxMaior2 = @anguloCustom2["angulo"]
									@anguloAuxMenor2 = @anguloCustom3["angulo"]
									/Fazendo a verficação para saber se é maior ou menor
									A partir dai, faz uma incrementação do angulo ou decrementação
									/
									@auxInjecao3 = @tempo_Injecao3
								 if @anguloAuxMaior2 >= @anguloAuxMenor2 then
									while @tempo_Injecao3 <= @auxInjecao3 do	
										while @anguloAuxMaior2 >= @anguloAuxMenor2 do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao3, :y => @anguloAuxMaior2}
											#puts @valorSensograma
											 @anguloAuxMaior2 -= 0.1
											 @auxInjecao3 = @tempo_Injecao3
											@tempo_Injecao3 += @tempo_PassoAux
										end
									end
								elsif 
									while @tempo_Injecao3 <= @auxInjecao3 do	
										while @anguloAuxMaior2 <= @anguloAuxMenor2 do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao3, :y => @anguloAuxMaior2}
											#puts @valorSensograma
											 @anguloAuxMaior2 += 0.1
											 @auxInjecao3 = @tempo_Injecao3
											@tempo_Injecao3 += @tempo_PassoAux
										end
									end
								 end

									#puts @tempo_Injecao3
									/Essa parte faz a inserção no sensograma até o tempo Total/
									while @tempo_Injecao3 <= @tempo_TotalAux do
												@valorSensograma << {:id => 'io', :x => @tempo_Injecao3, :y => @angleCustom3}	
											@tempo_Injecao3 += @tempo_PassoAux

										#puts @valorSensograma
									end
		                     end

		                     
					   

					
					 @teta_max = @assimetria.max_by { |z| z["r"] }
					 @teta_min = @assimetria.min_by { |y| y["r"] }
					 @teta_maxCustom2 = @assimetriaCustom2.max_by { |custom2max| custom2max["r"] }
					 @teta_minCustom2 = @assimetriaCustom2.min_by { |custom2min| custom2min["r"] }
					 @teta_maxCustom3 = @assimetriaCustom3.max_by { |custom3max| custom3max["r"] }
					 @teta_minCustom3 = @assimetriaCustom3.min_by { |custom3min| custom3min["r"] }
					  if (@teta_max != nil && @teta_min != nil) || (@teta_maxCustom2 != nil && @teta_minCustom2 != nil) || (@teta_maxCustom3 != nil && @teta_minCustom3 != nil) then 
						@teta_m1 = @teta_max["r"]
						@teta_m2 = @teta_min["r"]

						@tetaCustom2M1 = @teta_maxCustom2["r"]
						@tetaCustom2M2 = @teta_minCustom2["r"]

						@tetaCustom3M1 = @teta_maxCustom3["r"]
						@tetaCustom3M2 = @teta_minCustom3["r"]

						@teta_medio = ((@teta_m1 + @teta_m2)/2).round(2)
						@teta_medioCustom2 = ((@tetaCustom2M1 + @tetaCustom2M2)/2).round(2)
						@teta_medioCustom3 = ((@tetaCustom3M1 + @tetaCustom3M2)/2).round(2)
						
						@teta_aux_v1 = (@teta_min["angulo"]).round(2)
						@teta_aux_v1Custom2 = (@teta_minCustom2["angulo"]).round(2)
						@teta_aux_v1Custom3 = (@teta_minCustom3["angulo"]).round(2)

						/Variaveis começando em 0 para pegar os valores da curva
						como angulo minimo, assimetria e largura da curva/
						@v1 = 0
						@v2 = 0
						@v1Custom2 = 0
						@v2Custom2 = 0
						@v1Custom3 = 0
						@v2Custom3 = 0
						@aux = 0
						@auxCustom2 = 0
						@auxCustom3 = 0
						@vetor = []
						@vetorCustom2 = []
						@vetorCustom3 = []
						@vetor1 = []
						@vetor1Custom2 = []
						@vetor1Custom3 = []

						#For do Hash aonde tem o meus valores de R e o angulo
						#A partir dai vou pegar o meu @valor medio e procurar o 
						# minino do meus angulos
						@assimetria.each do |a|
						  if a["r"] <= @teta_medio then
							if @aux == 0 then
								@v1 = a["angulo"]
							    @aux += 1
							 else
								@v2 = a["angulo"]
							end
						   end
						end

						#For do Hash aonde tem o meus valores de R e o angulo
						#A partir dai vou pegar o meu @valor medio e procurar o 
						# minino do meus angulos dos valores da assimetriaCustom2
						@assimetriaCustom2.each do |custom2|
							if custom2["r"] <= @teta_medioCustom2 then
								if @auxCustom2 == 0 then
									@v1Custom2 = custom2["angulo"]
									@auxCustom2 += 1
								else
									@v2Custom2 = custom2["angulo"]
								 end
							end
						end

						#For do Hash aonde tem o meus valores de R e o angulo
						#A partir dai vou pegar o meu @valor medio e procurar o 
						# minino do meus angulos dos valores da assimetriaCustom3
						@assimetriaCustom3.each do |custom3|
							if custom3["r"] <= @teta_medioCustom3 then
								if @auxCustom3 == 0 then
									@v1Custom3 = custom3["angulo"]
									@auxCustom3 += 1
								else
									@v2Custom3 = custom3["angulo"]
								 end
							end
						end

						#@assimetria.each do |valorR|
						    

						#end

						# if @tempo_InicioAux != nil then
					 	  #while @tempo_InicioAux <= @tempo_InjecaoAux do
					   	  #	@valorSensograma << {"r" => @valor_r, "tempo" => @tempo_InjecaoAux}
					   	  #	@tempo_InjecaoAux += @tempo_PassoAux
					   	 # end
					# end

						#puts "O valor do V1Custom3 = #{@v1Custom3}"
						#puts @v2Custom3
						/Calculo da assimetria e largura
						a variavel @vetor ele está fazendo o calculo da largura
						e @vetor1 está fazendo o calculo da assimetria/
						@vetor = ((@v1 - @v2).abs).round(4)
						@vetor1 = ((@v1/@v2).abs).round(4)

						@vetorCustom2 = ((@v1Custom2 - @v2Custom2).abs).round(4)
						@vetor1Custom2 = ((@v1Custom2/@v2Custom2).abs).round(4)

						@vetorCustom3 = ((@v1Custom3 - @v2Custom3).abs).round(4)
						@vetor1Custom3 = ((@v1Custom3/@v2Custom3).abs).round(4)

						#puts @vetor1Custom3  
					
						@valor_aux = Complex((@n1) * (Math.sin(@angulo_th)))
						@valor_aux1 = Complex((@n1) * (Math.sin(@angulo_th)))
						@aux = @valor_aux * @valor_aux1
						@indice_aux = (@refracao2 * @refracao2).real
						#puts @aux   

						@n3 = Math.sqrt(((@indice_aux*@aux)/(@indice_aux-@aux))).round(4)
						#puts @n3 
					   end
					 
					result = Hash.new
					#result["chartAIM"] = @valor
					result["IRCustom1"] = @refracao4_Custom1
					result["IRCustom2"] = @refracao4_Custom2
					result["IRCustom3"] = @refracao4_Custom3
					result["chartSensograma"] = @valorSensograma
					result["valor_medio"] = @teta_medio
					result["valor_medioCustom2"] = @teta_medioCustom2
					result["valor_medioCustom3"] = @teta_medioCustom3
					result["angulo_minimo"] = "#{@teta_aux_v1} º"
					result["angulo_minimoCustom2"] = "#{@teta_aux_v1Custom2} º"
					result["angulo_minimoCustom3"] = "#{@teta_aux_v1Custom3} º"
					result["largura_certo"] = "#{@vetor} º"
					result["largura_certoCustom2"] = "#{@vetorCustom2} º"
					result["largura_certoCustom3"] = "#{@vetorCustom2} º"
					result["assimetria_certo"] = @vetor1
					result["assimetria_certoCustom2"] = @vetor1Custom2
					result["assimetria_certoCustom3"] = @vetor1Custom3
					result["indiceRefracaoCerto"] = @n3

					respond_to do |format|
					 # format.html { grafico }
					  format.html { graficoSensograma }
					  format.js { render :json => result }
					end
		 
	end

	def simulaCamada5

			@angulo_entrada = params[:anguloEntrada].to_f
			@angulo_saida = params[:anguloSaida].to_f
			@angulo_soma = params[:passo2].to_f
			
			@comprimento_onda = params['onda'].to_f

			@espessura1aux = params['espessuras'] || []
			#puts @espessura1aux

			@espessura1 = 0
			@espessura2 = 0
			@espessura3 = 0
			@espessura4 = 0
			@espessura5_Custom1 = 0
			@espessura5_Custom2 = 0
			@espessura5_Custom3 = 0

			

			if @espessura1aux.length == 4 then
			   @espessura1 = @espessura1aux[0].to_f
			   @espessura2 = @espessura1aux[1].to_f
			   @espessura3 = @espessura1aux[2].to_f
			   @espessura4 = @espessura1aux[3].to_f
			   puts 'entrei l'
			end

				@espessura5_Custom1 = params['EspessuraCustom1'].to_f
			    @espessura5_Custom2 = params['EspessuraCustom2'].to_f		 
			 	@espessura5_Custom3 = params['EspessuraCustom3'].to_f
				

				logger.info("SPR Criado".yellow)

				@refracao1 = calculoRefracao( params['elementos'] ? params['elementos'][0] : 0, @comprimento_onda)
				@refracao2 = calculoRefracao( params['elementos'] ? params['elementos'][1] : 0, @comprimento_onda)
				@refracao3 = calculoRefracao( params['elementos'] ? params['elementos'][2] : 0, @comprimento_onda)
				@refracao4 = calculoRefracao( params['elementos'] ? params['elementos'][3] : 0, @comprimento_onda)
			
				@refracao5_Custom1 = params['RefracaoCustom1'].to_f
			    @refracao5_Custom2 = params['RefracaoCustom2'].to_f
			    @refracao5_Custom3 = params['RefracaoCustom3'].to_f

				@tempo_InicioAux = params['tempoInicio'].to_f
				@tempo_TotalAux = params['tempoTotal'].to_f
				@tempo_PassoAux = params['tempoPasso'].to_f

				@tempo_InjecaoAux = params['tempoInjecao'].to_f
			    @tempo_InjecaoAux2 = params['tempoInjecao2'].to_f
			    @tempo_InjecaoAux3 = params['tempoInjecao3'].to_f
		
			@espessura1 = (0.000000001 * @espessura1)
			@espessura2 = (0.000000001 * @espessura2)
			@espessura3 = (0.000000001 * @espessura3)
			@espessura4 = (0.000000001 * @espessura4)
			@espessura5_Custom1 = (0.000000001 * @espessura5_Custom1)
			@espessura5_Custom2 = (0.000000001 * @espessura5_Custom2)
			@espessura5_Custom3 = (0.000000001 * @espessura5_Custom3)

			@valor = []
			@valor_Custom2 = []
			@valor_Custom3 = []
			@valor1 = []
			@valorSensograma = []
			@assimetria = []
			@assimetriaCustom2 = []
			@assimetriaCustom3 = []

				while @angulo_entrada < @angulo_saida do
					@angulo_th = ((@angulo_entrada * (Math::PI))/(180))
					@k0 = ((2 * (Math::PI))/(0.000000001 * @comprimento_onda))
					@n1 = @refracao1

					#Inicio do qj1
					#qj1 está correto
					@valor1_qj1 = Complex((@refracao1 * @refracao1))
					@valor2aux_qj1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj1 = (@valor2aux_qj1 * @valor3aux_qj1)
					@valor2_qj1 = Math.sqrt(@valor1_qj1 - @valor4aux_qj1)
					@qj1 = Complex(((@valor2_qj1)/(@valor1_qj1)))
					#puts  "Valor do Qj1 = #{@qj1}"
					#Final do qj1

					#Inicio do qj2
					#A variavel @valor6aux_qj2 está pegando a parte real de um numero Complexo do valor @valor5aux_qj2
					#A variavel @valor7aux_qj2 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj2
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj2 = @refracao2 * @refracao2
					@valor2aux_qj2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj2 = (@valor2aux_qj2 * @valor3aux_qj2)
					@valor5aux_qj2 = @valor1_qj2 - @valor4aux_qj2
					@valor6aux_qj2 = @valor5aux_qj2.real
					@valor7aux_qj2 = @valor5aux_qj2.imaginary
					@valor2_qj2 = sqrt2(@valor6aux_qj2,@valor7aux_qj2)
					@qj2 = ((@valor2_qj2)/(@valor1_qj2))
					#Final do qj2

					#Inicio do qj3
					#A variavel @valor6aux_qj3 está pegando a parte real de um numero Complexo do valor @valor5aux_qj3
					#A variavel @valor7aux_qj3 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj3
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj3 = @refracao3 * @refracao3
					@valor2aux_qj3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj3 = (@valor2aux_qj3 * @valor3aux_qj3)
					@valor5aux_qj3 = @valor1_qj3 - @valor4aux_qj3
					@valor6aux_qj3 = @valor5aux_qj3.real
					@valor7aux_qj3 = @valor5aux_qj3.imaginary
					@valor2_qj3 = sqrt2(@valor6aux_qj3,@valor7aux_qj3)
					@qj3 = ((@valor2_qj3)/(@valor1_qj3))
					#Final do qj3

					#Inicio do qj4
					#A variavel @valor6aux_qj4 está pegando a parte real de um numero Complexo do valor @valor5aux_qj4
					#A variavel @valor7aux_qj4 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj4
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj4 = @refracao4 * @refracao4
					@valor2aux_qj4 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj4 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj4 = (@valor2aux_qj4 * @valor3aux_qj4)
					@valor5aux_qj4 = @valor1_qj4 - @valor4aux_qj4
					@valor6aux_qj4 = @valor5aux_qj4.real
					@valor7aux_qj4 = @valor5aux_qj4.imaginary
					@valor2_qj4 = sqrt2(@valor6aux_qj4,@valor7aux_qj4)
					@qj4 = ((@valor2_qj4)/(@valor1_qj4))
					#Final do qj4

					#Inicio do qj5 para a espessura 3 Custom 1 e refração 3 Custom 1
					#Essa parte da diminuição não foi preciso fazer a função
					@valor1_qj5_Custom1 = @refracao5_Custom1 * @refracao5_Custom1
					@valor2aux_qj5_Custom1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj5_Custom1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj5_Custom1 = ((@valor2aux_qj5_Custom1) * (@valor3aux_qj5_Custom1))
					@valor5aux_qj5_Custom1 = (@valor1_qj5_Custom1 - @valor4aux_qj5_Custom1)
					@valor6aux_qj5_Custom1 = @valor5aux_qj5_Custom1.real
					@valor7aux_qj5_Custom1 = @valor5aux_qj5_Custom1.imaginary
					@valor2_qj5_Custom1 = sqrt2(@valor6aux_qj5_Custom1,@valor7aux_qj5_Custom1)
					@qj5_Custom1 = ((@valor2_qj5_Custom1)/(@valor1_qj5_Custom1))
					#Fim do qj5 para a espessura 3 Custom 1 e refração 3 Custom 1

					#Inicio do qj5 para a espessura 3 Custom 2 e refração 3 Custom 2
					#Essa parte da diminuição não foi preciso fazer a função
					@valor1_qj5_Custom2 = @refracao5_Custom2 * @refracao5_Custom2
					@valor2aux_qj5_Custom2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj5_Custom2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj5_Custom2 = ((@valor2aux_qj5_Custom2) * (@valor3aux_qj5_Custom2))
					@valor5aux_qj5_Custom2 = (@valor1_qj5_Custom2 - @valor4aux_qj5_Custom2)
					@valor6aux_qj5_Custom2 = @valor5aux_qj5_Custom2.real
					@valor7aux_qj5_Custom2 = @valor5aux_qj5_Custom2.imaginary
					@valor2_qj5_Custom2 = sqrt2(@valor6aux_qj5_Custom2,@valor7aux_qj5_Custom2)
					@qj5_Custom2 = ((@valor2_qj5_Custom2)/(@valor1_qj5_Custom2))
					#puts @qj5
					#Fim do qj5 para a espessura 3 Custom 2 e refração 3 Custom 2

					#Inicio do qj5 para a espessura 3 Custom 3 e refração 3 Custom 3
					#Essa parte da diminuição não foi preciso fazer a função
					@valor1_qj5_Custom3 = @refracao5_Custom3 * @refracao5_Custom3
					@valor2aux_qj5_Custom3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj5_Custom3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj5_Custom3 = ((@valor2aux_qj5_Custom3) * (@valor3aux_qj5_Custom3))
					@valor5aux_qj5_Custom3 = (@valor1_qj5_Custom3 - @valor4aux_qj5_Custom3)
					@valor6aux_qj5_Custom3 = @valor5aux_qj5_Custom3.real
					@valor7aux_qj5_Custom3 = @valor5aux_qj5_Custom3.imaginary
					@valor2_qj5_Custom3 = sqrt2(@valor6aux_qj5_Custom3,@valor7aux_qj5_Custom3)
					@qj5_Custom3 = ((@valor2_qj5_Custom3)/(@valor1_qj5_Custom3))
					#Fim do qj5 para a espessura 3 Custom 3 e refração 3 Custom 3

					#beta2 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta2 = Complex((@k0 * @espessura2))
					@valor1aux_beta2 = Complex(@refracao2 * @refracao2)
					@valor2aux_beta2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta2 = ((@valor2aux_beta2) * (@valor3aux_beta2))
					@valor5aux_beta2 = ((@valor1aux_beta2) - (@valor4aux_beta2))
					@valor6aux_beta2 = @valor5aux_beta2.real
					@valor7aux_beta2 = @valor5aux_beta2.imaginary
					@valor2beta2 = sqrt2(@valor6aux_beta2,@valor7aux_beta2)
					@beta2 = ((@valor1beta2) * (@valor2beta2))

					#beta3 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta3 = Complex((@k0 * @espessura3))
					@valor1aux_beta3 = Complex(@refracao3 * @refracao3)
					@valor2aux_beta3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta3 = ((@valor2aux_beta3) * (@valor3aux_beta3))
					@valor5aux_beta3 = ((@valor1aux_beta3) - (@valor4aux_beta3))
					@valor6aux_beta3 = @valor5aux_beta3.real
					@valor7aux_beta3 = @valor5aux_beta3.imaginary
					@valor2beta3 = sqrt2(@valor6aux_beta3,@valor7aux_beta3)
					@beta3 = ((@valor1beta3) * (@valor2beta3))

					#beta4 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta4 = Complex((@k0 * @espessura4))
					@valor1aux_beta4 = Complex(@refracao4 * @refracao4)
					@valor2aux_beta4 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta4 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta4 = ((@valor2aux_beta4) * (@valor3aux_beta4))
					@valor5aux_beta4 = ((@valor1aux_beta4) - (@valor4aux_beta4))
					@valor6aux_beta4 = @valor5aux_beta4.real
					@valor7aux_beta4 = @valor5aux_beta4.imaginary
					@valor2beta4 = sqrt2(@valor6aux_beta4,@valor7aux_beta4)
					@beta4 = ((@valor1beta4) * (@valor2beta4))


					@i = (Complex(0,-1))
					
					@M2 = [[],[]]
					@M2[0][0] = Complex((Math.cos(@beta2)))
					@M2[0][1] = Complex(((@i * (Math.sin(@beta2)))/(@qj2)))
					@M2[1][0] = Complex(((@i * (Math.sin(@beta2)))*(@qj2)))
					@M2[1][1] = Complex((Math.cos(@beta2)))

					@M3 = [[],[]]
					@M3[0][0] = Complex((Math.cos(@beta3)))
					@M3[0][1] = Complex(((@i * (Math.sin(@beta3)))/(@qj3)))
					@M3[1][0] = Complex(((@i * (Math.sin(@beta3)))*(@qj3)))
					@M3[1][1] = Complex((Math.cos(@beta3)))

					@M4 = [[],[]]
					@M4[0][0] = Complex((Math.cos(@beta4)))
					@M4[0][1] = Complex(((@i * (Math.sin(@beta4)))/(@qj4)))
					@M4[1][0] = Complex(((@i * (Math.sin(@beta4)))*(@qj4)))
					@M4[1][1] = Complex((Math.cos(@beta4)))

					@M5 = [[],[]]
					@M5[0][0] = Complex(((@M2[0][0] * @M3[0][0]) + (@M2[0][1] * @M3[1][0])))
					@M5[0][1] = Complex(((@M2[0][0] * @M3[0][1]) + (@M2[0][1] * @M3[1][1])))
					@M5[1][0] = Complex(((@M2[1][0] * @M3[0][0]) + (@M2[1][1] * @M3[1][0])))
					@M5[1][1] = Complex(((@M2[1][0] * @M3[0][1]) + (@M2[1][1] * @M3[1][1])))

					@M6 = [[],[]]
					@M6[0][0] = Complex(((@M5[0][0] * @M4[0][0]) + (@M5[0][1] * @M4[1][0])))
					@M6[0][1] = Complex(((@M5[0][0] * @M4[0][1]) + (@M5[0][1] * @M4[1][1])))
					@M6[1][0] = Complex(((@M5[1][0] * @M4[0][0]) + (@M5[1][1] * @M4[1][0])))
					@M6[1][1] = Complex(((@M5[1][0] * @M4[0][1]) + (@M5[1][1] * @M4[1][1])))

					#Primeiro calculo da soma de matriz do coeficiente de fresnel para a espessura3_Custom1
					#Que é o qj3_Custom1
					@r_n = ((((@M6[0][1] * @qj5_Custom1) + @M6[0][0]) * @qj1) - ((@M6[1][1] * @qj5_Custom1) + (@M6[1][0])))
					@r_d = ((((@M6[0][1] * @qj5_Custom1) + @M6[0][0]) * @qj1) + ((@M6[1][1] * @qj5_Custom1) + (@M6[1][0])))
					#Segundo calculo da soma de matriz do coeficiente de fresnel para a espessura3_Custom2
					#Que é o qj3_Custom2
					@r_n_custom2 = ((((@M6[0][1] * @qj5_Custom2) + @M6[0][0]) * @qj1) - ((@M6[1][1] * @qj5_Custom2) + (@M6[1][0])))
					@r_d_custom2 = ((((@M6[0][1] * @qj5_Custom2) + @M6[0][0]) * @qj1) + ((@M6[1][1] * @qj5_Custom2) + (@M6[1][0])))
					#Terceiro calculo da soma de matriz do coeficiente de fresnel para a espessura3_Custom3
					#Que é o qj3_Custom3
					@r_n_custom3 = ((((@M6[0][1] * @qj5_Custom3) + @M6[0][0]) * @qj1) - ((@M6[1][1] * @qj5_Custom3) + (@M6[1][0])))
					@r_d_custom3 = ((((@M6[0][1] * @qj5_Custom3) + @M6[0][0]) * @qj1) + ((@M6[1][1] * @qj5_Custom3) + (@M6[1][0])))

					#Divisão do resultado @r_n/@r_d
					@r = Complex(((@r_n)/(@r_d)))

					#puts @r

					#Divisão do resultado @r_n_custom2/@r_d_custom2
					@r_Custom2 = Complex(((@r_n_custom2)/(@r_d_custom2)))

					#puts @r_Custom2
					#Divisão do resultado @r_n_custom3/@r_d_custom3
					@r_Custom3 = Complex(((@r_n_custom3)/(@r_d_custom3)))

					#puts @r_Custom3

					#valor de r da espessura3_custom1
					@valor_r = Complex(((@r * (@r.conjugate))).real)

					#valor de r da espessura3_custom2
					@valor_r_custom2 = Complex(((@r_Custom2 * (@r_Custom2.conjugate))).real)

					#valor de r da espessura3_custom3
					@valor_r_custom3 = Complex(((@r_Custom3 * (@r_Custom3.conjugate))).real)

					@valor << { :id => 'io', :x => @angulo_entrada, :y => @valor_r}

					@valor_Custom2 << { :id => 'io', :x => @angulo_entrada, :y => @valor_r_custom2}

					@valor_Custom3 << { :id => 'io', :x => @angulo_entrada, :y => @valor_r_custom3}

					@angulo_entrada += @angulo_soma
					
					@assimetria <<  {"r" => @valor_r, "angulo" => @angulo_entrada}        
				   	@assimetriaCustom2 << {"r" => @valor_r_custom2, "angulo" => @angulo_entrada}
				   	@assimetriaCustom3 << {"r" => @valor_r_custom3, "angulo" => @angulo_entrada}
				   
				end

					#Angulo Minimo
				  @angulo = @assimetria.min_by { |x| x["r"] }
				  	@anguloCustom2 = @assimetriaCustom2.min_by {|angCustom2| angCustom2["r"] }
						@anguloCustom3 = @assimetriaCustom3.min_by {|angCustom3| angCustom3["r"] }
					   if @angulo != nil || @anguloCustmo2 != nil || @anguloCustom3 != nil then 
					   	
					   	@angulo_min = @angulo["angulo"]
					   	@angleCustom2 = @anguloCustom2["angulo"]
				   	    @angleCustom3 = @anguloCustom3["angulo"]
				   	   
				   	    end
				   	    /
				   	    	Aqui começa o inicio do sensograma com o tempo inicial
				   	    		ele faz uma verificação se os tempos eles são diferentes de 0.0, se o tempo
				   	    		for diferente de 0 ele começa a incrementação da primeira substancia
				   	    		com a variavel @EspessuraCustom1 e RefracaoCustom1. Começa com o tempo de inicio e vai 
				   	    		até o tempo_InjecaoAux
				   	    /
						   	if @tempo_InicioAux != 0.0 && @tempo_PassoAux != 0.0 && @tempo_InjecaoAux != 0.0 then
									while @tempo_InicioAux <= @tempo_InjecaoAux do
												@valorSensograma << {:id => 'io', :x => @tempo_InicioAux, :y => @angulo_min}	
										@tempo_InicioAux += @tempo_PassoAux
										#puts @valorSensograma
									end

									
									/
									Essas duas variaveis são para receber os dois angulos minimos que tem,
									O primeiro angulo minimo é o resultado da @EspessuraCustom1 e @RefracaoCustom1
									
									O Segundo Angulo Minimo denominado @anguloAuxMenor ele recebe os valores
									da @EspessuraCustom2 e @RefracaoCustom2
									/
										@anguloAuxMaior = @angulo["angulo"]
										@anguloAuxMenor = @anguloCustom2["angulo"]
									#Essa variavel @tempo_Injecao2 recebe o valor do Tempo_InjecaoAux
									@tempo_Injecao2 = @tempo_InjecaoAux
									#Essa variavel @tempoTotal2 recebe o Tempo total menos o tempo de injeção 
									#3 da terceira substancia
									@tempoTotal2 = @tempo_TotalAux - @tempo_InjecaoAux3	
										#puts @tempoTotal2
								
								    /
									 Aqui começa uma decrementação do angulo ou incrementação de acordo com
									 angulo minimo que é o resultado das variaveis @anguloAuxMaior e @anguloAuxMenor
									 Faz a verificação do Angulo se ele é maior ou menor 	
									/	
									@auxInjecao2 = @tempo_Injecao2
								 if @anguloAuxMaior >= @anguloAuxMenor then
									while @tempo_Injecao2 <= @auxInjecao2 do	
										while @anguloAuxMaior >= @anguloAuxMenor do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao2, :y => @anguloAuxMaior}
											#puts @valorSensograma
											 @anguloAuxMaior -= 0.1
											 @auxInjecao2 = @tempo_Injecao2
											@tempo_Injecao2 += @tempo_PassoAux
										end
									end
								elsif 
									while @tempo_Injecao2 <= @auxInjecao2 do	
										while @anguloAuxMaior <= @anguloAuxMenor do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao2, :y => @anguloAuxMaior}
											#puts @valorSensograma
											 @anguloAuxMaior += 0.1
											 @auxInjecao2 = @tempo_Injecao2
											@tempo_Injecao2 += @tempo_PassoAux
										end
									end
								 end
										
									/Faz um while do tempo aonde ele parou e vai até o tempoTotal2 = @tempo_TotalAux - @tempo_InjecaoAux3
										fazendo a inserção no sensograma.
									/
									while @tempo_Injecao2 <= @tempoTotal2 do
												@valorSensograma << {:id => 'io', :x => @tempo_Injecao2, :y => @angleCustom2}	
										@tempo_Injecao2 += @tempo_PassoAux
										#puts @valorSensograma
									end


									/Essa variavel @tempo_Injecao3 ela recebe o tempo @tempo_Injecao2 aonde parou
									/
									@tempo_Injecao3 = @tempo_Injecao2



									/Essas duas variaveis ela recebe os dois angulos minimos/
									@anguloAuxMaior2 = @anguloCustom2["angulo"]
									@anguloAuxMenor2 = @anguloCustom3["angulo"]
									/Fazendo a verficação para saber se é maior ou menor
									A partir dai, faz uma incrementação do angulo ou decrementação
									/
									@auxInjecao3 = @tempo_Injecao3
								 if @anguloAuxMaior2 >= @anguloAuxMenor2 then
									while @tempo_Injecao3 <= @auxInjecao3 do	
										while @anguloAuxMaior2 >= @anguloAuxMenor2 do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao3, :y => @anguloAuxMaior2}
											#puts @valorSensograma
											 @anguloAuxMaior2 -= 0.1
											 @auxInjecao3 = @tempo_Injecao3
											@tempo_Injecao3 += @tempo_PassoAux
										end
									end
								elsif 
									while @tempo_Injecao3 <= @auxInjecao3 do	
										while @anguloAuxMaior2 <= @anguloAuxMenor2 do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao3, :y => @anguloAuxMaior2}
											#puts @valorSensograma
											 @anguloAuxMaior2 += 0.1
											 @auxInjecao3 = @tempo_Injecao3
											@tempo_Injecao3 += @tempo_PassoAux
										end
									end
								 end

									#puts @tempo_Injecao3
									/Essa parte faz a inserção no sensograma até o tempo Total/
									while @tempo_Injecao3 <= @tempo_TotalAux do
												@valorSensograma << {:id => 'io', :x => @tempo_Injecao3, :y => @angleCustom3}	
											@tempo_Injecao3 += @tempo_PassoAux

										#puts @valorSensograma
									end
		                     end

		                     
					   

					
					 @teta_max = @assimetria.max_by { |z| z["r"] }
					 @teta_min = @assimetria.min_by { |y| y["r"] }
					 @teta_maxCustom2 = @assimetriaCustom2.max_by { |custom2max| custom2max["r"] }
					 @teta_minCustom2 = @assimetriaCustom2.min_by { |custom2min| custom2min["r"] }
					 @teta_maxCustom3 = @assimetriaCustom3.max_by { |custom3max| custom3max["r"] }
					 @teta_minCustom3 = @assimetriaCustom3.min_by { |custom3min| custom3min["r"] }
					  if (@teta_max != nil && @teta_min != nil) || (@teta_maxCustom2 != nil && @teta_minCustom2 != nil) || (@teta_maxCustom3 != nil && @teta_minCustom3 != nil) then 
						@teta_m1 = @teta_max["r"]
						@teta_m2 = @teta_min["r"]

						@tetaCustom2M1 = @teta_maxCustom2["r"]
						@tetaCustom2M2 = @teta_minCustom2["r"]

						@tetaCustom3M1 = @teta_maxCustom3["r"]
						@tetaCustom3M2 = @teta_minCustom3["r"]

						@teta_medio = ((@teta_m1 + @teta_m2)/2).round(2)
						@teta_medioCustom2 = ((@tetaCustom2M1 + @tetaCustom2M2)/2).round(2)
						@teta_medioCustom3 = ((@tetaCustom3M1 + @tetaCustom3M2)/2).round(2)
						
						@teta_aux_v1 = (@teta_min["angulo"]).round(2)
						@teta_aux_v1Custom2 = (@teta_minCustom2["angulo"]).round(2)
						@teta_aux_v1Custom3 = (@teta_minCustom3["angulo"]).round(2)

						/Variaveis começando em 0 para pegar os valores da curva
						como angulo minimo, assimetria e largura da curva/
						@v1 = 0
						@v2 = 0
						@v1Custom2 = 0
						@v2Custom2 = 0
						@v1Custom3 = 0
						@v2Custom3 = 0
						@aux = 0
						@auxCustom2 = 0
						@auxCustom3 = 0
						@vetor = []
						@vetorCustom2 = []
						@vetorCustom3 = []
						@vetor1 = []
						@vetor1Custom2 = []
						@vetor1Custom3 = []

						#For do Hash aonde tem o meus valores de R e o angulo
						#A partir dai vou pegar o meu @valor medio e procurar o 
						# minino do meus angulos
						@assimetria.each do |a|
						  if a["r"] <= @teta_medio then
							if @aux == 0 then
								@v1 = a["angulo"]
							    @aux += 1
							 else
								@v2 = a["angulo"]
							end
						   end
						end

						#For do Hash aonde tem o meus valores de R e o angulo
						#A partir dai vou pegar o meu @valor medio e procurar o 
						# minino do meus angulos dos valores da assimetriaCustom2
						@assimetriaCustom2.each do |custom2|
							if custom2["r"] <= @teta_medioCustom2 then
								if @auxCustom2 == 0 then
									@v1Custom2 = custom2["angulo"]
									@auxCustom2 += 1
								else
									@v2Custom2 = custom2["angulo"]
								 end
							end
						end

						#For do Hash aonde tem o meus valores de R e o angulo
						#A partir dai vou pegar o meu @valor medio e procurar o 
						# minino do meus angulos dos valores da assimetriaCustom3
						@assimetriaCustom3.each do |custom3|
							if custom3["r"] <= @teta_medioCustom3 then
								if @auxCustom3 == 0 then
									@v1Custom3 = custom3["angulo"]
									@auxCustom3 += 1
								else
									@v2Custom3 = custom3["angulo"]
								 end
							end
						end

						#@assimetria.each do |valorR|
						    

						#end

						# if @tempo_InicioAux != nil then
					 	  #while @tempo_InicioAux <= @tempo_InjecaoAux do
					   	  #	@valorSensograma << {"r" => @valor_r, "tempo" => @tempo_InjecaoAux}
					   	  #	@tempo_InjecaoAux += @tempo_PassoAux
					   	 # end
					# end

						#puts "O valor do V1Custom3 = #{@v1Custom3}"
						#puts @v2Custom3
						/Calculo da assimetria e largura
						a variavel @vetor ele está fazendo o calculo da largura
						e @vetor1 está fazendo o calculo da assimetria/
						@vetor = ((@v1 - @v2).abs).round(4)
						@vetor1 = ((@v1/@v2).abs).round(4)

						@vetorCustom2 = ((@v1Custom2 - @v2Custom2).abs).round(4)
						@vetor1Custom2 = ((@v1Custom2/@v2Custom2).abs).round(4)

						@vetorCustom3 = ((@v1Custom3 - @v2Custom3).abs).round(4)
						@vetor1Custom3 = ((@v1Custom3/@v2Custom3).abs).round(4)

						#puts @vetor1Custom3  
					
						@valor_aux = Complex((@n1) * (Math.sin(@angulo_th)))
						@valor_aux1 = Complex((@n1) * (Math.sin(@angulo_th)))
						@aux = @valor_aux * @valor_aux1
						@indice_aux = (@refracao2 * @refracao2).real
						#puts @aux   

						@n3 = Math.sqrt(((@indice_aux*@aux)/(@indice_aux-@aux))).round(4)
						#puts @n3 
					   end
					 
					result = Hash.new
					#result["chartAIM"] = @valor
					result["IRCustom1"] = @refracao5_Custom1
					result["IRCustom2"] = @refracao5_Custom2
					result["IRCustom3"] = @refracao5_Custom3
					result["chartSensograma"] = @valorSensograma
					result["valor_medio"] = @teta_medio
					result["valor_medioCustom2"] = @teta_medioCustom2
					result["valor_medioCustom3"] = @teta_medioCustom3
					result["angulo_minimo"] = "#{@teta_aux_v1} º"
					result["angulo_minimoCustom2"] = "#{@teta_aux_v1Custom2} º"
					result["angulo_minimoCustom3"] = "#{@teta_aux_v1Custom3} º"
					result["largura_certo"] = "#{@vetor} º"
					result["largura_certoCustom2"] = "#{@vetorCustom2} º"
					result["largura_certoCustom3"] = "#{@vetorCustom2} º"
					result["assimetria_certo"] = @vetor1
					result["assimetria_certoCustom2"] = @vetor1Custom2
					result["assimetria_certoCustom3"] = @vetor1Custom3
					result["indiceRefracaoCerto"] = @n3

					respond_to do |format|
					 # format.html { grafico }
					  format.html { graficoSensograma }
					  format.js { render :json => result }
					end
		 
		end

	def simulaCamada6

			@angulo_entrada = params[:anguloEntrada].to_f
			@angulo_saida = params[:anguloSaida].to_f
			@angulo_soma = params[:passo2].to_f
			
			@comprimento_onda = params['onda'].to_f

			@espessura1aux = params['espessuras'] || []
			#puts @espessura1aux

			@espessura1 = 0
			@espessura2 = 0
			@espessura3 = 0
			@espessura4 = 0
			@espessura5 = 0
			@espessura6_Custom1 = 0
			@espessura6_Custom2 = 0
			@espessura6_Custom3 = 0

			

			if @espessura1aux.length == 5 then
			   @espessura1 = @espessura1aux[0].to_f
			   @espessura2 = @espessura1aux[1].to_f
			   @espessura3 = @espessura1aux[2].to_f
			   @espessura4 = @espessura1aux[3].to_f
			   @espessura5 = @espessura1aux[4].to_f
			   puts 'entrei l'
			end

				@espessura6_Custom1 = params['EspessuraCustom1'].to_f
			    @espessura6_Custom2 = params['EspessuraCustom2'].to_f		 
			 	@espessura6_Custom3 = params['EspessuraCustom3'].to_f
				

				logger.info("SPR Criado".yellow)

				@refracao1 = calculoRefracao( params['elementos'] ? params['elementos'][0] : 0, @comprimento_onda)
				@refracao2 = calculoRefracao( params['elementos'] ? params['elementos'][1] : 0, @comprimento_onda)
				@refracao3 = calculoRefracao( params['elementos'] ? params['elementos'][2] : 0, @comprimento_onda)
				@refracao4 = calculoRefracao( params['elementos'] ? params['elementos'][3] : 0, @comprimento_onda)
				@refracao5 = calculoRefracao( params['elementos'] ? params['elementos'][4] : 0, @comprimento_onda)
			
				@refracao6_Custom1 = params['RefracaoCustom1'].to_f
			    @refracao6_Custom2 = params['RefracaoCustom2'].to_f
			    @refracao6_Custom3 = params['RefracaoCustom3'].to_f

				@tempo_InicioAux = params['tempoInicio'].to_f
				@tempo_TotalAux = params['tempoTotal'].to_f
				@tempo_PassoAux = params['tempoPasso'].to_f

				@tempo_InjecaoAux = params['tempoInjecao'].to_f
			    @tempo_InjecaoAux2 = params['tempoInjecao2'].to_f
			    @tempo_InjecaoAux3 = params['tempoInjecao3'].to_f
		
			@espessura1 = (0.000000001 * @espessura1)
			@espessura2 = (0.000000001 * @espessura2)
			@espessura3 = (0.000000001 * @espessura3)
			@espessura4 = (0.000000001 * @espessura4)
			@espessura5 = (0.000000001 * @espessura5)
			@espessura6_Custom1 = (0.000000001 * @espessura6_Custom1)
			@espessura6_Custom2 = (0.000000001 * @espessura6_Custom2)
			@espessura6_Custom3 = (0.000000001 * @espessura6_Custom3)

			@valor = []
			@valor_Custom2 = []
			@valor_Custom3 = []
			@valor1 = []
			@valorSensograma = []
			@assimetria = []
			@assimetriaCustom2 = []
			@assimetriaCustom3 = []

				while @angulo_entrada < @angulo_saida do
					@angulo_th = ((@angulo_entrada * (Math::PI))/(180))
					@k0 = ((2 * (Math::PI))/(0.000000001 * @comprimento_onda))
					@n1 = @refracao1

					#Inicio do qj1
					#qj1 está correto
					@valor1_qj1 = Complex((@refracao1 * @refracao1))
					@valor2aux_qj1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj1 = (@valor2aux_qj1 * @valor3aux_qj1)
					@valor2_qj1 = Math.sqrt(@valor1_qj1 - @valor4aux_qj1)
					@qj1 = Complex(((@valor2_qj1)/(@valor1_qj1)))
					#puts  "Valor do Qj1 = #{@qj1}"
					#Final do qj1

					#Inicio do qj2
					#A variavel @valor6aux_qj2 está pegando a parte real de um numero Complexo do valor @valor5aux_qj2
					#A variavel @valor7aux_qj2 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj2
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj2 = @refracao2 * @refracao2
					@valor2aux_qj2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj2 = (@valor2aux_qj2 * @valor3aux_qj2)
					@valor5aux_qj2 = @valor1_qj2 - @valor4aux_qj2
					@valor6aux_qj2 = @valor5aux_qj2.real
					@valor7aux_qj2 = @valor5aux_qj2.imaginary
					@valor2_qj2 = sqrt2(@valor6aux_qj2,@valor7aux_qj2)
					@qj2 = ((@valor2_qj2)/(@valor1_qj2))
					#Final do qj2

					#Inicio do qj3
					#A variavel @valor6aux_qj3 está pegando a parte real de um numero Complexo do valor @valor5aux_qj3
					#A variavel @valor7aux_qj3 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj3
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj3 = @refracao3 * @refracao3
					@valor2aux_qj3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj3 = (@valor2aux_qj3 * @valor3aux_qj3)
					@valor5aux_qj3 = @valor1_qj3 - @valor4aux_qj3
					@valor6aux_qj3 = @valor5aux_qj3.real
					@valor7aux_qj3 = @valor5aux_qj3.imaginary
					@valor2_qj3 = sqrt2(@valor6aux_qj3,@valor7aux_qj3)
					@qj3 = ((@valor2_qj3)/(@valor1_qj3))
					#Final do qj3

					#Inicio do qj4
					#A variavel @valor6aux_qj4 está pegando a parte real de um numero Complexo do valor @valor5aux_qj4
					#A variavel @valor7aux_qj4 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj4
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj4 = @refracao4 * @refracao4
					@valor2aux_qj4 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj4 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj4 = (@valor2aux_qj4 * @valor3aux_qj4)
					@valor5aux_qj4 = @valor1_qj4 - @valor4aux_qj4
					@valor6aux_qj4 = @valor5aux_qj4.real
					@valor7aux_qj4 = @valor5aux_qj4.imaginary
					@valor2_qj4 = sqrt2(@valor6aux_qj4,@valor7aux_qj4)
					@qj4 = ((@valor2_qj4)/(@valor1_qj4))
					#Final do qj4

					#Inicio do qj5
					#A variavel @valor6aux_qj5 está pegando a parte real de um numero Complexo do valor @valor5aux_qj5
					#A variavel @valor7aux_qj5 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj5
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj5 = @refracao5 * @refracao5
					@valor2aux_qj5 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj5 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj5 = (@valor2aux_qj5 * @valor3aux_qj5)
					@valor5aux_qj5 = @valor1_qj5 - @valor4aux_qj5
					@valor6aux_qj5 = @valor5aux_qj5.real
					@valor7aux_qj5 = @valor5aux_qj5.imaginary
					@valor2_qj5 = sqrt2(@valor6aux_qj5,@valor7aux_qj5)
					@qj5 = ((@valor2_qj5)/(@valor1_qj5))
					#Final do qj4

					#Inicio do qj6 para a espessura 3 Custom 1 e refração 3 Custom 1
					#Essa parte da diminuição não foi preciso fazer a função
					@valor1_qj6_Custom1 = @refracao6_Custom1 * @refracao6_Custom1
					@valor2aux_qj6_Custom1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj6_Custom1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj6_Custom1 = ((@valor2aux_qj6_Custom1) * (@valor3aux_qj6_Custom1))
					@valor5aux_qj6_Custom1 = (@valor1_qj6_Custom1 - @valor4aux_qj6_Custom1)
					@valor6aux_qj6_Custom1 = @valor5aux_qj6_Custom1.real
					@valor7aux_qj6_Custom1 = @valor5aux_qj6_Custom1.imaginary
					@valor2_qj6_Custom1 = sqrt2(@valor6aux_qj6_Custom1,@valor7aux_qj6_Custom1)
					@qj6_Custom1 = ((@valor2_qj6_Custom1)/(@valor1_qj6_Custom1))
					#Fim do qj6 para a espessura 3 Custom 1 e refração 3 Custom 1

					#Inicio do qj6 para a espessura 3 Custom 2 e refração 3 Custom 2
					#Essa parte da diminuição não foi preciso fazer a função
					@valor1_qj6_Custom2 = @refracao6_Custom2 * @refracao6_Custom2
					@valor2aux_qj6_Custom2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj6_Custom2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj6_Custom2 = ((@valor2aux_qj6_Custom2) * (@valor3aux_qj6_Custom2))
					@valor5aux_qj6_Custom2 = (@valor1_qj6_Custom2 - @valor4aux_qj6_Custom2)
					@valor6aux_qj6_Custom2 = @valor5aux_qj6_Custom2.real
					@valor7aux_qj6_Custom2 = @valor5aux_qj6_Custom2.imaginary
					@valor2_qj6_Custom2 = sqrt2(@valor6aux_qj6_Custom2,@valor7aux_qj6_Custom2)
					@qj6_Custom2 = ((@valor2_qj6_Custom2)/(@valor1_qj6_Custom2))
					#Fim do qj6 para a espessura 3 Custom 2 e refração 3 Custom 2

					#Inicio do qj6 para a espessura 3 Custom 3 e refração 3 Custom 3
					#Essa parte da diminuição não foi preciso fazer a função
					@valor1_qj6_Custom3 = @refracao6_Custom3 * @refracao6_Custom3
					@valor2aux_qj6_Custom3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj6_Custom3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj6_Custom3 = ((@valor2aux_qj6_Custom3) * (@valor3aux_qj6_Custom3))
					@valor5aux_qj6_Custom3 = (@valor1_qj6_Custom3 - @valor4aux_qj6_Custom3)
					@valor6aux_qj6_Custom3 = @valor5aux_qj6_Custom3.real
					@valor7aux_qj6_Custom3 = @valor5aux_qj6_Custom3.imaginary
					@valor2_qj6_Custom3 = sqrt2(@valor6aux_qj6_Custom3,@valor7aux_qj6_Custom3)
					@qj6_Custom3 = ((@valor2_qj6_Custom3)/(@valor1_qj6_Custom3))
					#Fim do qj6 para a espessura 3 Custom 3 e refração 3 Custom 3

					#beta2 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta2 = Complex((@k0 * @espessura2))
					@valor1aux_beta2 = Complex(@refracao2 * @refracao2)
					@valor2aux_beta2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta2 = ((@valor2aux_beta2) * (@valor3aux_beta2))
					@valor5aux_beta2 = ((@valor1aux_beta2) - (@valor4aux_beta2))
					@valor6aux_beta2 = @valor5aux_beta2.real
					@valor7aux_beta2 = @valor5aux_beta2.imaginary
					@valor2beta2 = sqrt2(@valor6aux_beta2,@valor7aux_beta2)
					@beta2 = ((@valor1beta2) * (@valor2beta2))

					#beta3 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta3 = Complex((@k0 * @espessura3))
					@valor1aux_beta3 = Complex(@refracao3 * @refracao3)
					@valor2aux_beta3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta3 = ((@valor2aux_beta3) * (@valor3aux_beta3))
					@valor5aux_beta3 = ((@valor1aux_beta3) - (@valor4aux_beta3))
					@valor6aux_beta3 = @valor5aux_beta3.real
					@valor7aux_beta3 = @valor5aux_beta3.imaginary
					@valor2beta3 = sqrt2(@valor6aux_beta3,@valor7aux_beta3)
					@beta3 = ((@valor1beta3) * (@valor2beta3))

					#beta4 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta4 = Complex((@k0 * @espessura4))
					@valor1aux_beta4 = Complex(@refracao4 * @refracao4)
					@valor2aux_beta4 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta4 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta4 = ((@valor2aux_beta4) * (@valor3aux_beta4))
					@valor5aux_beta4 = ((@valor1aux_beta4) - (@valor4aux_beta4))
					@valor6aux_beta4 = @valor5aux_beta4.real
					@valor7aux_beta4 = @valor5aux_beta4.imaginary
					@valor2beta4 = sqrt2(@valor6aux_beta4,@valor7aux_beta4)
					@beta4 = ((@valor1beta4) * (@valor2beta4))

					#beta5 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta5 = Complex((@k0 * @espessura5))
					@valor1aux_beta5 = Complex(@refracao5 * @refracao5)
					@valor2aux_beta5 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta5 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta5 = ((@valor2aux_beta5) * (@valor3aux_beta5))
					@valor5aux_beta5 = ((@valor1aux_beta5) - (@valor4aux_beta5))
					@valor6aux_beta5 = @valor5aux_beta5.real
					@valor7aux_beta5 = @valor5aux_beta5.imaginary
					@valor2beta5 = sqrt2(@valor6aux_beta5,@valor7aux_beta5)
					@beta5 = ((@valor1beta5) * (@valor2beta5))


					@i = (Complex(0,-1))
					
					@M2 = [[],[]]
					@M2[0][0] = Complex((Math.cos(@beta2)))
					@M2[0][1] = Complex(((@i * (Math.sin(@beta2)))/(@qj2)))
					@M2[1][0] = Complex(((@i * (Math.sin(@beta2)))*(@qj2)))
					@M2[1][1] = Complex((Math.cos(@beta2)))

					@M3 = [[],[]]
					@M3[0][0] = Complex((Math.cos(@beta3)))
					@M3[0][1] = Complex(((@i * (Math.sin(@beta3)))/(@qj3)))
					@M3[1][0] = Complex(((@i * (Math.sin(@beta3)))*(@qj3)))
					@M3[1][1] = Complex((Math.cos(@beta3)))

					@M4 = [[],[]]
					@M4[0][0] = Complex((Math.cos(@beta4)))
					@M4[0][1] = Complex(((@i * (Math.sin(@beta4)))/(@qj4)))
					@M4[1][0] = Complex(((@i * (Math.sin(@beta4)))*(@qj4)))
					@M4[1][1] = Complex((Math.cos(@beta4)))

					@M5 = [[],[]]
					@M5[0][0] = Complex((Math.cos(@beta5)))
					@M5[0][1] = Complex(((@i * (Math.sin(@beta5)))/(@qj5)))
					@M5[1][0] = Complex(((@i * (Math.sin(@beta5)))*(@qj5)))
					@M5[1][1] = Complex((Math.cos(@beta5)))

					@M6 = [[],[]]
					@M6[0][0] = Complex(((@M2[0][0] * @M3[0][0]) + (@M2[0][1] * @M3[1][0])))
					@M6[0][1] = Complex(((@M2[0][0] * @M3[0][1]) + (@M2[0][1] * @M3[1][1])))
					@M6[1][0] = Complex(((@M2[1][0] * @M3[0][0]) + (@M2[1][1] * @M3[1][0])))
					@M6[1][1] = Complex(((@M2[1][0] * @M3[0][1]) + (@M2[1][1] * @M3[1][1])))

					@M7 = [[],[]]
					@M7[0][0] = Complex(((@M6[0][0] * @M4[0][0]) + (@M6[0][1] * @M4[1][0])))
					@M7[0][1] = Complex(((@M6[0][0] * @M4[0][1]) + (@M6[0][1] * @M4[1][1])))
					@M7[1][0] = Complex(((@M6[1][0] * @M4[0][0]) + (@M6[1][1] * @M4[1][0])))
					@M7[1][1] = Complex(((@M6[1][0] * @M4[0][1]) + (@M6[1][1] * @M4[1][1])))

					@M8 = [[],[]]
					@M8[0][0] = Complex(((@M7[0][0] * @M5[0][0]) + (@M7[0][1] * @M5[1][0])))
					@M8[0][1] = Complex(((@M7[0][0] * @M5[0][1]) + (@M7[0][1] * @M5[1][1])))
					@M8[1][0] = Complex(((@M7[1][0] * @M5[0][0]) + (@M7[1][1] * @M5[1][0])))
					@M8[1][1] = Complex(((@M7[1][0] * @M5[0][1]) + (@M7[1][1] * @M5[1][1])))

					#Primeiro calculo da soma de matriz do coeficiente de fresnel para a espessura3_Custom1
					#Que é o qj3_Custom1
					@r_n = ((((@M8[0][1] * @qj6_Custom1) + @M8[0][0]) * @qj1) - ((@M8[1][1] * @qj6_Custom1) + (@M8[1][0])))
					@r_d = ((((@M8[0][1] * @qj6_Custom1) + @M8[0][0]) * @qj1) + ((@M8[1][1] * @qj6_Custom1) + (@M8[1][0])))
					#Segundo calculo da soma de matriz do coeficiente de fresnel para a espessura3_Custom2
					#Que é o qj3_Custom2
					@r_n_custom2 = ((((@M8[0][1] * @qj6_Custom2) + @M8[0][0]) * @qj1) - ((@M8[1][1] * @qj6_Custom2) + (@M8[1][0])))
					@r_d_custom2 = ((((@M8[0][1] * @qj6_Custom2) + @M8[0][0]) * @qj1) + ((@M8[1][1] * @qj6_Custom2) + (@M8[1][0])))
					#Terceiro calculo da soma de matriz do coeficiente de fresnel para a espessura3_Custom3
					#Que é o qj3_Custom3
					@r_n_custom3 = ((((@M8[0][1] * @qj6_Custom3) + @M8[0][0]) * @qj1) - ((@M8[1][1] * @qj6_Custom3) + (@M8[1][0])))
					@r_d_custom3 = ((((@M8[0][1] * @qj6_Custom3) + @M8[0][0]) * @qj1) + ((@M8[1][1] * @qj6_Custom3) + (@M8[1][0])))

					#Divisão do resultado @r_n/@r_d
					@r = Complex(((@r_n)/(@r_d)))

					#puts @r

					#Divisão do resultado @r_n_custom2/@r_d_custom2
					@r_Custom2 = Complex(((@r_n_custom2)/(@r_d_custom2)))

					#puts @r_Custom2
					#Divisão do resultado @r_n_custom3/@r_d_custom3
					@r_Custom3 = Complex(((@r_n_custom3)/(@r_d_custom3)))

					#puts @r_Custom3

					#valor de r da espessura3_custom1
					@valor_r = Complex(((@r * (@r.conjugate))).real)

					#valor de r da espessura3_custom2
					@valor_r_custom2 = Complex(((@r_Custom2 * (@r_Custom2.conjugate))).real)

					#valor de r da espessura3_custom3
					@valor_r_custom3 = Complex(((@r_Custom3 * (@r_Custom3.conjugate))).real)

					@valor << { :id => 'io', :x => @angulo_entrada, :y => @valor_r}

					@valor_Custom2 << { :id => 'io', :x => @angulo_entrada, :y => @valor_r_custom2}

					@valor_Custom3 << { :id => 'io', :x => @angulo_entrada, :y => @valor_r_custom3}

					@angulo_entrada += @angulo_soma
					
					@assimetria <<  {"r" => @valor_r, "angulo" => @angulo_entrada}        
				   	@assimetriaCustom2 << {"r" => @valor_r_custom2, "angulo" => @angulo_entrada}
				   	@assimetriaCustom3 << {"r" => @valor_r_custom3, "angulo" => @angulo_entrada}
				   
				end

					#Angulo Minimo
				  @angulo = @assimetria.min_by { |x| x["r"] }
				  	@anguloCustom2 = @assimetriaCustom2.min_by {|angCustom2| angCustom2["r"] }
						@anguloCustom3 = @assimetriaCustom3.min_by {|angCustom3| angCustom3["r"] }
					   if @angulo != nil || @anguloCustmo2 != nil || @anguloCustom3 != nil then 
					   	
					   	@angulo_min = @angulo["angulo"]
					   	@angleCustom2 = @anguloCustom2["angulo"]
				   	    @angleCustom3 = @anguloCustom3["angulo"]
				   	   
				   	    end
				   	    /
				   	    	Aqui começa o inicio do sensograma com o tempo inicial
				   	    		ele faz uma verificação se os tempos eles são diferentes de 0.0, se o tempo
				   	    		for diferente de 0 ele começa a incrementação da primeira substancia
				   	    		com a variavel @EspessuraCustom1 e RefracaoCustom1. Começa com o tempo de inicio e vai 
				   	    		até o tempo_InjecaoAux
				   	    /
						   	if @tempo_InicioAux != 0.0 && @tempo_PassoAux != 0.0 && @tempo_InjecaoAux != 0.0 then
									while @tempo_InicioAux <= @tempo_InjecaoAux do
												@valorSensograma << {:id => 'io', :x => @tempo_InicioAux, :y => @angulo_min}	
										@tempo_InicioAux += @tempo_PassoAux
										#puts @valorSensograma
									end

									
									/
									Essas duas variaveis são para receber os dois angulos minimos que tem,
									O primeiro angulo minimo é o resultado da @EspessuraCustom1 e @RefracaoCustom1
									
									O Segundo Angulo Minimo denominado @anguloAuxMenor ele recebe os valores
									da @EspessuraCustom2 e @RefracaoCustom2
									/
										@anguloAuxMaior = @angulo["angulo"]
										@anguloAuxMenor = @anguloCustom2["angulo"]
									#Essa variavel @tempo_Injecao2 recebe o valor do Tempo_InjecaoAux
									@tempo_Injecao2 = @tempo_InjecaoAux
									#Essa variavel @tempoTotal2 recebe o Tempo total menos o tempo de injeção 
									#3 da terceira substancia
									@tempoTotal2 = @tempo_TotalAux - @tempo_InjecaoAux3	
										#puts @tempoTotal2
								
								    /
									 Aqui começa uma decrementação do angulo ou incrementação de acordo com
									 angulo minimo que é o resultado das variaveis @anguloAuxMaior e @anguloAuxMenor
									 Faz a verificação do Angulo se ele é maior ou menor 	
									/	
									@auxInjecao2 = @tempo_Injecao2
								 if @anguloAuxMaior >= @anguloAuxMenor then
									while @tempo_Injecao2 <= @auxInjecao2 do	
										while @anguloAuxMaior >= @anguloAuxMenor do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao2, :y => @anguloAuxMaior}
											#puts @valorSensograma
											 @anguloAuxMaior -= 0.1
											 @auxInjecao2 = @tempo_Injecao2
											@tempo_Injecao2 += @tempo_PassoAux
										end
									end
								elsif 
									while @tempo_Injecao2 <= @auxInjecao2 do	
										while @anguloAuxMaior <= @anguloAuxMenor do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao2, :y => @anguloAuxMaior}
											#puts @valorSensograma
											 @anguloAuxMaior += 0.1
											 @auxInjecao2 = @tempo_Injecao2
											@tempo_Injecao2 += @tempo_PassoAux
										end
									end
								 end
										
									/Faz um while do tempo aonde ele parou e vai até o tempoTotal2 = @tempo_TotalAux - @tempo_InjecaoAux3
										fazendo a inserção no sensograma.
									/
									while @tempo_Injecao2 <= @tempoTotal2 do
												@valorSensograma << {:id => 'io', :x => @tempo_Injecao2, :y => @angleCustom2}	
										@tempo_Injecao2 += @tempo_PassoAux
										#puts @valorSensograma
									end


									/Essa variavel @tempo_Injecao3 ela recebe o tempo @tempo_Injecao2 aonde parou
									/
									@tempo_Injecao3 = @tempo_Injecao2



									/Essas duas variaveis ela recebe os dois angulos minimos/
									@anguloAuxMaior2 = @anguloCustom2["angulo"]
									@anguloAuxMenor2 = @anguloCustom3["angulo"]
									/Fazendo a verficação para saber se é maior ou menor
									A partir dai, faz uma incrementação do angulo ou decrementação
									/
									@auxInjecao3 = @tempo_Injecao3
								 if @anguloAuxMaior2 >= @anguloAuxMenor2 then
									while @tempo_Injecao3 <= @auxInjecao3 do	
										while @anguloAuxMaior2 >= @anguloAuxMenor2 do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao3, :y => @anguloAuxMaior2}
											#puts @valorSensograma
											 @anguloAuxMaior2 -= 0.1
											 @auxInjecao3 = @tempo_Injecao3
											@tempo_Injecao3 += @tempo_PassoAux
										end
									end
								elsif 
									while @tempo_Injecao3 <= @auxInjecao3 do	
										while @anguloAuxMaior2 <= @anguloAuxMenor2 do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao3, :y => @anguloAuxMaior2}
											#puts @valorSensograma
											 @anguloAuxMaior2 += 0.1
											 @auxInjecao3 = @tempo_Injecao3
											@tempo_Injecao3 += @tempo_PassoAux
										end
									end
								 end

									#puts @tempo_Injecao3
									/Essa parte faz a inserção no sensograma até o tempo Total/
									while @tempo_Injecao3 <= @tempo_TotalAux do
												@valorSensograma << {:id => 'io', :x => @tempo_Injecao3, :y => @angleCustom3}	
											@tempo_Injecao3 += @tempo_PassoAux

										#puts @valorSensograma
									end
		                     end

		                     
					   

					
					 @teta_max = @assimetria.max_by { |z| z["r"] }
					 @teta_min = @assimetria.min_by { |y| y["r"] }
					 @teta_maxCustom2 = @assimetriaCustom2.max_by { |custom2max| custom2max["r"] }
					 @teta_minCustom2 = @assimetriaCustom2.min_by { |custom2min| custom2min["r"] }
					 @teta_maxCustom3 = @assimetriaCustom3.max_by { |custom3max| custom3max["r"] }
					 @teta_minCustom3 = @assimetriaCustom3.min_by { |custom3min| custom3min["r"] }
					  if (@teta_max != nil && @teta_min != nil) || (@teta_maxCustom2 != nil && @teta_minCustom2 != nil) || (@teta_maxCustom3 != nil && @teta_minCustom3 != nil) then 
						@teta_m1 = @teta_max["r"]
						@teta_m2 = @teta_min["r"]

						@tetaCustom2M1 = @teta_maxCustom2["r"]
						@tetaCustom2M2 = @teta_minCustom2["r"]

						@tetaCustom3M1 = @teta_maxCustom3["r"]
						@tetaCustom3M2 = @teta_minCustom3["r"]

						@teta_medio = ((@teta_m1 + @teta_m2)/2).round(2)
						@teta_medioCustom2 = ((@tetaCustom2M1 + @tetaCustom2M2)/2).round(2)
						@teta_medioCustom3 = ((@tetaCustom3M1 + @tetaCustom3M2)/2).round(2)
						
						@teta_aux_v1 = (@teta_min["angulo"]).round(2)
						@teta_aux_v1Custom2 = (@teta_minCustom2["angulo"]).round(2)
						@teta_aux_v1Custom3 = (@teta_minCustom3["angulo"]).round(2)

						/Variaveis começando em 0 para pegar os valores da curva
						como angulo minimo, assimetria e largura da curva/
						@v1 = 0
						@v2 = 0
						@v1Custom2 = 0
						@v2Custom2 = 0
						@v1Custom3 = 0
						@v2Custom3 = 0
						@aux = 0
						@auxCustom2 = 0
						@auxCustom3 = 0
						@vetor = []
						@vetorCustom2 = []
						@vetorCustom3 = []
						@vetor1 = []
						@vetor1Custom2 = []
						@vetor1Custom3 = []

						#For do Hash aonde tem o meus valores de R e o angulo
						#A partir dai vou pegar o meu @valor medio e procurar o 
						# minino do meus angulos
						@assimetria.each do |a|
						  if a["r"] <= @teta_medio then
							if @aux == 0 then
								@v1 = a["angulo"]
							    @aux += 1
							 else
								@v2 = a["angulo"]
							end
						   end
						end

						#For do Hash aonde tem o meus valores de R e o angulo
						#A partir dai vou pegar o meu @valor medio e procurar o 
						# minino do meus angulos dos valores da assimetriaCustom2
						@assimetriaCustom2.each do |custom2|
							if custom2["r"] <= @teta_medioCustom2 then
								if @auxCustom2 == 0 then
									@v1Custom2 = custom2["angulo"]
									@auxCustom2 += 1
								else
									@v2Custom2 = custom2["angulo"]
								 end
							end
						end

						#For do Hash aonde tem o meus valores de R e o angulo
						#A partir dai vou pegar o meu @valor medio e procurar o 
						# minino do meus angulos dos valores da assimetriaCustom3
						@assimetriaCustom3.each do |custom3|
							if custom3["r"] <= @teta_medioCustom3 then
								if @auxCustom3 == 0 then
									@v1Custom3 = custom3["angulo"]
									@auxCustom3 += 1
								else
									@v2Custom3 = custom3["angulo"]
								 end
							end
						end

						#@assimetria.each do |valorR|
						    

						#end

						# if @tempo_InicioAux != nil then
					 	  #while @tempo_InicioAux <= @tempo_InjecaoAux do
					   	  #	@valorSensograma << {"r" => @valor_r, "tempo" => @tempo_InjecaoAux}
					   	  #	@tempo_InjecaoAux += @tempo_PassoAux
					   	 # end
					# end

						#puts "O valor do V1Custom3 = #{@v1Custom3}"
						#puts @v2Custom3
						/Calculo da assimetria e largura
						a variavel @vetor ele está fazendo o calculo da largura
						e @vetor1 está fazendo o calculo da assimetria/
						@vetor = ((@v1 - @v2).abs).round(4)
						@vetor1 = ((@v1/@v2).abs).round(4)

						@vetorCustom2 = ((@v1Custom2 - @v2Custom2).abs).round(4)
						@vetor1Custom2 = ((@v1Custom2/@v2Custom2).abs).round(4)

						@vetorCustom3 = ((@v1Custom3 - @v2Custom3).abs).round(4)
						@vetor1Custom3 = ((@v1Custom3/@v2Custom3).abs).round(4)

						#puts @vetor1Custom3  
					
						@valor_aux = Complex((@n1) * (Math.sin(@angulo_th)))
						@valor_aux1 = Complex((@n1) * (Math.sin(@angulo_th)))
						@aux = @valor_aux * @valor_aux1
						@indice_aux = (@refracao2 * @refracao2).real
						#puts @aux   

						@n3 = Math.sqrt(((@indice_aux*@aux)/(@indice_aux-@aux))).round(4)
						#puts @n3 
					   end
					 
					result = Hash.new
					#result["chartAIM"] = @valor
					result["IRCustom1"] = @refracao6_Custom1
					result["IRCustom2"] = @refracao6_Custom2
					result["IRCustom3"] = @refracao6_Custom3
					result["chartSensograma"] = @valorSensograma
					result["valor_medio"] = @teta_medio
					result["valor_medioCustom2"] = @teta_medioCustom2
					result["valor_medioCustom3"] = @teta_medioCustom3
					result["angulo_minimo"] = "#{@teta_aux_v1} º"
					result["angulo_minimoCustom2"] = "#{@teta_aux_v1Custom2} º"
					result["angulo_minimoCustom3"] = "#{@teta_aux_v1Custom3} º"
					result["largura_certo"] = "#{@vetor} º"
					result["largura_certoCustom2"] = "#{@vetorCustom2} º"
					result["largura_certoCustom3"] = "#{@vetorCustom2} º"
					result["assimetria_certo"] = @vetor1
					result["assimetria_certoCustom2"] = @vetor1Custom2
					result["assimetria_certoCustom3"] = @vetor1Custom3
					result["indiceRefracaoCerto"] = @n3

					respond_to do |format|
					 # format.html { grafico }
					  format.html { graficoSensograma }
					  format.js { render :json => result }
					end
		 
	end

	def simulaCamada7

			@angulo_entrada = params[:anguloEntrada].to_f
			@angulo_saida = params[:anguloSaida].to_f
			@angulo_soma = params[:passo2].to_f
			
			@comprimento_onda = params['onda'].to_f

			@espessura1aux = params['espessuras'] || []
			#puts @espessura1aux

			@espessura1 = 0
			@espessura2 = 0
			@espessura3 = 0
			@espessura4 = 0
			@espessura5 = 0
			@espessura6 = 0
			@espessura7_Custom1 = 0
			@espessura7_Custom2 = 0
			@espessura7_Custom3 = 0

			

			if @espessura1aux.length == 6 then
			   @espessura1 = @espessura1aux[0].to_f
			   @espessura2 = @espessura1aux[1].to_f
			   @espessura3 = @espessura1aux[2].to_f
			   @espessura4 = @espessura1aux[3].to_f
			   @espessura5 = @espessura1aux[4].to_f
			   @espessura6 = @espessura1aux[5].to_f
			   puts 'entrei l'
			end

				@espessura7_Custom1 = params['EspessuraCustom1'].to_f
			    @espessura7_Custom2 = params['EspessuraCustom2'].to_f		 
			 	@espessura7_Custom3 = params['EspessuraCustom3'].to_f
				

				logger.info("SPR Criado".yellow)

				@refracao1 = calculoRefracao( params['elementos'] ? params['elementos'][0] : 0, @comprimento_onda)
				@refracao2 = calculoRefracao( params['elementos'] ? params['elementos'][1] : 0, @comprimento_onda)
				@refracao3 = calculoRefracao( params['elementos'] ? params['elementos'][2] : 0, @comprimento_onda)
				@refracao4 = calculoRefracao( params['elementos'] ? params['elementos'][3] : 0, @comprimento_onda)
				@refracao5 = calculoRefracao( params['elementos'] ? params['elementos'][4] : 0, @comprimento_onda)
				@refracao6 = calculoRefracao( params['elementos'] ? params['elementos'][5] : 0, @comprimento_onda)
			
				@refracao7_Custom1 = params['RefracaoCustom1'].to_f
			    @refracao7_Custom2 = params['RefracaoCustom2'].to_f
			    @refracao7_Custom3 = params['RefracaoCustom3'].to_f

				@tempo_InicioAux = params['tempoInicio'].to_f
				@tempo_TotalAux = params['tempoTotal'].to_f
				@tempo_PassoAux = params['tempoPasso'].to_f

				@tempo_InjecaoAux = params['tempoInjecao'].to_f
			    @tempo_InjecaoAux2 = params['tempoInjecao2'].to_f
			    @tempo_InjecaoAux3 = params['tempoInjecao3'].to_f
		
			@espessura1 = (0.000000001 * @espessura1)
			@espessura2 = (0.000000001 * @espessura2)
			@espessura3 = (0.000000001 * @espessura3)
			@espessura4 = (0.000000001 * @espessura4)
			@espessura5 = (0.000000001 * @espessura5)
			@espessura6 = (0.000000001 * @espessura6)
			@espessura7_Custom1 = (0.000000001 * @espessura7_Custom1)
			@espessura7_Custom2 = (0.000000001 * @espessura7_Custom2)
			@espessura7_Custom3 = (0.000000001 * @espessura7_Custom3)

			@valor = []
			@valor_Custom2 = []
			@valor_Custom3 = []
			@valor1 = []
			@valorSensograma = []
			@assimetria = []
			@assimetriaCustom2 = []
			@assimetriaCustom3 = []

				while @angulo_entrada < @angulo_saida do
					@angulo_th = ((@angulo_entrada * (Math::PI))/(180))
					@k0 = ((2 * (Math::PI))/(0.000000001 * @comprimento_onda))
					@n1 = @refracao1

					#Inicio do qj1
					#qj1 está correto
					@valor1_qj1 = Complex((@refracao1 * @refracao1))
					@valor2aux_qj1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj1 = (@valor2aux_qj1 * @valor3aux_qj1)
					@valor2_qj1 = Math.sqrt(@valor1_qj1 - @valor4aux_qj1)
					@qj1 = Complex(((@valor2_qj1)/(@valor1_qj1)))
					#puts  "Valor do Qj1 = #{@qj1}"
					#Final do qj1

					#Inicio do qj2
					#A variavel @valor6aux_qj2 está pegando a parte real de um numero Complexo do valor @valor5aux_qj2
					#A variavel @valor7aux_qj2 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj2
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj2 = @refracao2 * @refracao2
					@valor2aux_qj2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj2 = (@valor2aux_qj2 * @valor3aux_qj2)
					@valor5aux_qj2 = @valor1_qj2 - @valor4aux_qj2
					@valor6aux_qj2 = @valor5aux_qj2.real
					@valor7aux_qj2 = @valor5aux_qj2.imaginary
					@valor2_qj2 = sqrt2(@valor6aux_qj2,@valor7aux_qj2)
					@qj2 = ((@valor2_qj2)/(@valor1_qj2))
					#Final do qj2

					#Inicio do qj3
					#A variavel @valor6aux_qj3 está pegando a parte real de um numero Complexo do valor @valor5aux_qj3
					#A variavel @valor7aux_qj3 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj3
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj3 = @refracao3 * @refracao3
					@valor2aux_qj3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj3 = (@valor2aux_qj3 * @valor3aux_qj3)
					@valor5aux_qj3 = @valor1_qj3 - @valor4aux_qj3
					@valor6aux_qj3 = @valor5aux_qj3.real
					@valor7aux_qj3 = @valor5aux_qj3.imaginary
					@valor2_qj3 = sqrt2(@valor6aux_qj3,@valor7aux_qj3)
					@qj3 = ((@valor2_qj3)/(@valor1_qj3))
					#Final do qj3

					#Inicio do qj4
					#A variavel @valor6aux_qj4 está pegando a parte real de um numero Complexo do valor @valor5aux_qj4
					#A variavel @valor7aux_qj4 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj4
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj4 = @refracao4 * @refracao4
					@valor2aux_qj4 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj4 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj4 = (@valor2aux_qj4 * @valor3aux_qj4)
					@valor5aux_qj4 = @valor1_qj4 - @valor4aux_qj4
					@valor6aux_qj4 = @valor5aux_qj4.real
					@valor7aux_qj4 = @valor5aux_qj4.imaginary
					@valor2_qj4 = sqrt2(@valor6aux_qj4,@valor7aux_qj4)
					@qj4 = ((@valor2_qj4)/(@valor1_qj4))
					#Final do qj4

					#Inicio do qj5
					#A variavel @valor6aux_qj5 está pegando a parte real de um numero Complexo do valor @valor5aux_qj5
					#A variavel @valor7aux_qj5 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj5
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj5 = @refracao5 * @refracao5
					@valor2aux_qj5 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj5 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj5 = (@valor2aux_qj5 * @valor3aux_qj5)
					@valor5aux_qj5 = @valor1_qj5 - @valor4aux_qj5
					@valor6aux_qj5 = @valor5aux_qj5.real
					@valor7aux_qj5 = @valor5aux_qj5.imaginary
					@valor2_qj5 = sqrt2(@valor6aux_qj5,@valor7aux_qj5)
					@qj5 = ((@valor2_qj5)/(@valor1_qj5))
					#Final do qj5

					#Inicio do qj6
					#A variavel @valor6aux_qj6 está pegando a parte real de um numero Complexo do valor @valor5aux_qj6
					#A variavel @valor7aux_qj6 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj6
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj6 = @refracao6 * @refracao6
					@valor2aux_qj6 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj6 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj6 = (@valor2aux_qj6 * @valor3aux_qj6)
					@valor5aux_qj6 = @valor1_qj6 - @valor4aux_qj6
					@valor6aux_qj6 = @valor5aux_qj6.real
					@valor7aux_qj6 = @valor5aux_qj6.imaginary
					@valor2_qj6 = sqrt2(@valor6aux_qj6,@valor7aux_qj6)
					@qj6 = ((@valor2_qj6)/(@valor1_qj6))
					#Final do qj6

					#Inicio do qj7 para a espessura 3 Custom 1 e refração 3 Custom 1
					#Essa parte da diminuição não foi preciso fazer a função
					@valor1_qj7_Custom1 = @refracao7_Custom1 * @refracao7_Custom1
					@valor2aux_qj7_Custom1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj7_Custom1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj7_Custom1 = ((@valor2aux_qj7_Custom1) * (@valor3aux_qj7_Custom1))
					@valor5aux_qj7_Custom1 = (@valor1_qj7_Custom1 - @valor4aux_qj7_Custom1)
					@valor6aux_qj7_Custom1 = @valor5aux_qj7_Custom1.real
					@valor7aux_qj7_Custom1 = @valor5aux_qj7_Custom1.imaginary
					@valor2_qj7_Custom1 = sqrt2(@valor6aux_qj7_Custom1,@valor7aux_qj7_Custom1)
					@qj7_Custom1 = ((@valor2_qj7_Custom1)/(@valor1_qj7_Custom1))
					#Fim do qj7 para a espessura 3 Custom 1 e refração 3 Custom 1

					#Inicio do qj7 para a espessura 3 Custom 2 e refração 3 Custom 2
					#Essa parte da diminuição não foi preciso fazer a função
					@valor1_qj7_Custom2 = @refracao7_Custom2 * @refracao7_Custom2
					@valor2aux_qj7_Custom2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj7_Custom2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj7_Custom2 = ((@valor2aux_qj7_Custom2) * (@valor3aux_qj7_Custom2))
					@valor5aux_qj7_Custom2 = (@valor1_qj7_Custom2 - @valor4aux_qj7_Custom2)
					@valor6aux_qj7_Custom2 = @valor5aux_qj7_Custom2.real
					@valor7aux_qj7_Custom2 = @valor5aux_qj7_Custom2.imaginary
					@valor2_qj7_Custom2 = sqrt2(@valor6aux_qj7_Custom2,@valor7aux_qj7_Custom2)
					@qj7_Custom2 = ((@valor2_qj7_Custom2)/(@valor1_qj7_Custom2))
					#Fim do qj7 para a espessura 3 Custom 2 e refração 3 Custom 2

					#Inicio do qj7 para a espessura 3 Custom 3 e refração 3 Custom 3
					#Essa parte da diminuição não foi preciso fazer a função
					@valor1_qj7_Custom3 = @refracao7_Custom3 * @refracao7_Custom3
					@valor2aux_qj7_Custom3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj7_Custom3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj7_Custom3 = ((@valor2aux_qj7_Custom3) * (@valor3aux_qj7_Custom3))
					@valor5aux_qj7_Custom3 = (@valor1_qj7_Custom3 - @valor4aux_qj7_Custom3)
					@valor6aux_qj7_Custom3 = @valor5aux_qj7_Custom3.real
					@valor7aux_qj7_Custom3 = @valor5aux_qj7_Custom3.imaginary
					@valor2_qj7_Custom3 = sqrt2(@valor6aux_qj7_Custom3,@valor7aux_qj7_Custom3)
					@qj7_Custom3 = ((@valor2_qj7_Custom3)/(@valor1_qj7_Custom3))
					#Fim do qj7 para a espessura 3 Custom 3 e refração 3 Custom 3

					#beta2 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta2 = Complex((@k0 * @espessura2))
					@valor1aux_beta2 = Complex(@refracao2 * @refracao2)
					@valor2aux_beta2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta2 = ((@valor2aux_beta2) * (@valor3aux_beta2))
					@valor5aux_beta2 = ((@valor1aux_beta2) - (@valor4aux_beta2))
					@valor6aux_beta2 = @valor5aux_beta2.real
					@valor7aux_beta2 = @valor5aux_beta2.imaginary
					@valor2beta2 = sqrt2(@valor6aux_beta2,@valor7aux_beta2)
					@beta2 = ((@valor1beta2) * (@valor2beta2))

					#beta3 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta3 = Complex((@k0 * @espessura3))
					@valor1aux_beta3 = Complex(@refracao3 * @refracao3)
					@valor2aux_beta3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta3 = ((@valor2aux_beta3) * (@valor3aux_beta3))
					@valor5aux_beta3 = ((@valor1aux_beta3) - (@valor4aux_beta3))
					@valor6aux_beta3 = @valor5aux_beta3.real
					@valor7aux_beta3 = @valor5aux_beta3.imaginary
					@valor2beta3 = sqrt2(@valor6aux_beta3,@valor7aux_beta3)
					@beta3 = ((@valor1beta3) * (@valor2beta3))

					#beta4 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta4 = Complex((@k0 * @espessura4))
					@valor1aux_beta4 = Complex(@refracao4 * @refracao4)
					@valor2aux_beta4 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta4 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta4 = ((@valor2aux_beta4) * (@valor3aux_beta4))
					@valor5aux_beta4 = ((@valor1aux_beta4) - (@valor4aux_beta4))
					@valor6aux_beta4 = @valor5aux_beta4.real
					@valor7aux_beta4 = @valor5aux_beta4.imaginary
					@valor2beta4 = sqrt2(@valor6aux_beta4,@valor7aux_beta4)
					@beta4 = ((@valor1beta4) * (@valor2beta4))

					#beta5 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta5 = Complex((@k0 * @espessura5))
					@valor1aux_beta5 = Complex(@refracao5 * @refracao5)
					@valor2aux_beta5 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta5 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta5 = ((@valor2aux_beta5) * (@valor3aux_beta5))
					@valor5aux_beta5 = ((@valor1aux_beta5) - (@valor4aux_beta5))
					@valor6aux_beta5 = @valor5aux_beta5.real
					@valor7aux_beta5 = @valor5aux_beta5.imaginary
					@valor2beta5 = sqrt2(@valor6aux_beta5,@valor7aux_beta5)
					@beta5 = ((@valor1beta5) * (@valor2beta5))

					#beta6 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta6 = Complex((@k0 * @espessura6))
					@valor1aux_beta6 = Complex(@refracao6 * @refracao6)
					@valor2aux_beta6 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta6 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta6 = ((@valor2aux_beta6) * (@valor3aux_beta6))
					@valor5aux_beta6 = ((@valor1aux_beta6) - (@valor4aux_beta6))
					@valor6aux_beta6 = @valor5aux_beta6.real
					@valor7aux_beta6 = @valor5aux_beta6.imaginary
					@valor2beta6 = sqrt2(@valor6aux_beta6,@valor7aux_beta6)
					@beta6 = ((@valor1beta6) * (@valor2beta6))


					@i = (Complex(0,-1))
					
					@M2 = [[],[]]
					@M2[0][0] = Complex((Math.cos(@beta2)))
					@M2[0][1] = Complex(((@i * (Math.sin(@beta2)))/(@qj2)))
					@M2[1][0] = Complex(((@i * (Math.sin(@beta2)))*(@qj2)))
					@M2[1][1] = Complex((Math.cos(@beta2)))

					@M3 = [[],[]]
					@M3[0][0] = Complex((Math.cos(@beta3)))
					@M3[0][1] = Complex(((@i * (Math.sin(@beta3)))/(@qj3)))
					@M3[1][0] = Complex(((@i * (Math.sin(@beta3)))*(@qj3)))
					@M3[1][1] = Complex((Math.cos(@beta3)))

					@M4 = [[],[]]
					@M4[0][0] = Complex((Math.cos(@beta4)))
					@M4[0][1] = Complex(((@i * (Math.sin(@beta4)))/(@qj4)))
					@M4[1][0] = Complex(((@i * (Math.sin(@beta4)))*(@qj4)))
					@M4[1][1] = Complex((Math.cos(@beta4)))

					@M5 = [[],[]]
					@M5[0][0] = Complex((Math.cos(@beta5)))
					@M5[0][1] = Complex(((@i * (Math.sin(@beta5)))/(@qj5)))
					@M5[1][0] = Complex(((@i * (Math.sin(@beta5)))*(@qj5)))
					@M5[1][1] = Complex((Math.cos(@beta5)))

					@M6 = [[],[]]
					@M6[0][0] = Complex((Math.cos(@beta6)))
					@M6[0][1] = Complex(((@i * (Math.sin(@beta6)))/(@qj6)))
					@M6[1][0] = Complex(((@i * (Math.sin(@beta6)))*(@qj6)))
					@M6[1][1] = Complex((Math.cos(@beta6)))

					@M7 = [[],[]]
					@M7[0][0] = Complex(((@M2[0][0] * @M3[0][0]) + (@M2[0][1] * @M3[1][0])))
					@M7[0][1] = Complex(((@M2[0][0] * @M3[0][1]) + (@M2[0][1] * @M3[1][1])))
					@M7[1][0] = Complex(((@M2[1][0] * @M3[0][0]) + (@M2[1][1] * @M3[1][0])))
					@M7[1][1] = Complex(((@M2[1][0] * @M3[0][1]) + (@M2[1][1] * @M3[1][1])))

					@M8 = [[],[]]
					@M8[0][0] = Complex(((@M7[0][0] * @M4[0][0]) + (@M7[0][1] * @M4[1][0])))
					@M8[0][1] = Complex(((@M7[0][0] * @M4[0][1]) + (@M7[0][1] * @M4[1][1])))
					@M8[1][0] = Complex(((@M7[1][0] * @M4[0][0]) + (@M7[1][1] * @M4[1][0])))
					@M8[1][1] = Complex(((@M7[1][0] * @M4[0][1]) + (@M7[1][1] * @M4[1][1])))

					@M9 = [[],[]]
					@M9[0][0] = Complex(((@M8[0][0] * @M5[0][0]) + (@M8[0][1] * @M5[1][0])))
					@M9[0][1] = Complex(((@M8[0][0] * @M5[0][1]) + (@M8[0][1] * @M5[1][1])))
					@M9[1][0] = Complex(((@M8[1][0] * @M5[0][0]) + (@M8[1][1] * @M5[1][0])))
					@M9[1][1] = Complex(((@M8[1][0] * @M5[0][1]) + (@M8[1][1] * @M5[1][1])))

					@M10 = [[],[]]
					@M10[0][0] = Complex(((@M9[0][0] * @M6[0][0]) + (@M9[0][1] * @M6[1][0])))
					@M10[0][1] = Complex(((@M9[0][0] * @M6[0][1]) + (@M9[0][1] * @M6[1][1])))
					@M10[1][0] = Complex(((@M9[1][0] * @M6[0][0]) + (@M9[1][1] * @M6[1][0])))
					@M10[1][1] = Complex(((@M9[1][0] * @M6[0][1]) + (@M9[1][1] * @M6[1][1])))

					#Primeiro calculo da soma de matriz do coeficiente de fresnel para a espessura3_Custom1
					#Que é o qj3_Custom1
					@r_n = ((((@M10[0][1] * @qj7_Custom1) + @M10[0][0]) * @qj1) - ((@M10[1][1] * @qj7_Custom1) + (@M10[1][0])))
					@r_d = ((((@M10[0][1] * @qj7_Custom1) + @M10[0][0]) * @qj1) + ((@M10[1][1] * @qj7_Custom1) + (@M10[1][0])))
					#Segundo calculo da soma de matriz do coeficiente de fresnel para a espessura3_Custom2
					#Que é o qj3_Custom2
					@r_n_custom2 = ((((@M10[0][1] * @qj7_Custom2) + @M10[0][0]) * @qj1) - ((@M10[1][1] * @qj7_Custom2) + (@M10[1][0])))
					@r_d_custom2 = ((((@M10[0][1] * @qj7_Custom2) + @M10[0][0]) * @qj1) + ((@M10[1][1] * @qj7_Custom2) + (@M10[1][0])))
					#Terceiro calculo da soma de matriz do coeficiente de fresnel para a espessura3_Custom3
					#Que é o qj3_Custom3
					@r_n_custom3 = ((((@M10[0][1] * @qj7_Custom3) + @M10[0][0]) * @qj1) - ((@M10[1][1] * @qj7_Custom3) + (@M10[1][0])))
					@r_d_custom3 = ((((@M10[0][1] * @qj7_Custom3) + @M10[0][0]) * @qj1) + ((@M10[1][1] * @qj7_Custom3) + (@M10[1][0])))

					#Divisão do resultado @r_n/@r_d
					@r = Complex(((@r_n)/(@r_d)))

					#puts @r

					#Divisão do resultado @r_n_custom2/@r_d_custom2
					@r_Custom2 = Complex(((@r_n_custom2)/(@r_d_custom2)))

					#puts @r_Custom2
					#Divisão do resultado @r_n_custom3/@r_d_custom3
					@r_Custom3 = Complex(((@r_n_custom3)/(@r_d_custom3)))

					#puts @r_Custom3

					#valor de r da espessura3_custom1
					@valor_r = Complex(((@r * (@r.conjugate))).real)

					#valor de r da espessura3_custom2
					@valor_r_custom2 = Complex(((@r_Custom2 * (@r_Custom2.conjugate))).real)

					#valor de r da espessura3_custom3
					@valor_r_custom3 = Complex(((@r_Custom3 * (@r_Custom3.conjugate))).real)

					@valor << { :id => 'io', :x => @angulo_entrada, :y => @valor_r}

					@valor_Custom2 << { :id => 'io', :x => @angulo_entrada, :y => @valor_r_custom2}

					@valor_Custom3 << { :id => 'io', :x => @angulo_entrada, :y => @valor_r_custom3}

					@angulo_entrada += @angulo_soma
					
					@assimetria <<  {"r" => @valor_r, "angulo" => @angulo_entrada}        
				   	@assimetriaCustom2 << {"r" => @valor_r_custom2, "angulo" => @angulo_entrada}
				   	@assimetriaCustom3 << {"r" => @valor_r_custom3, "angulo" => @angulo_entrada}
				   
				end

					#Angulo Minimo
				  @angulo = @assimetria.min_by { |x| x["r"] }
				  	@anguloCustom2 = @assimetriaCustom2.min_by {|angCustom2| angCustom2["r"] }
						@anguloCustom3 = @assimetriaCustom3.min_by {|angCustom3| angCustom3["r"] }
					   if @angulo != nil || @anguloCustmo2 != nil || @anguloCustom3 != nil then 
					   	
					   	@angulo_min = @angulo["angulo"]
					   	@angleCustom2 = @anguloCustom2["angulo"]
				   	    @angleCustom3 = @anguloCustom3["angulo"]
				   	   
				   	    end
				   	    /
				   	    	Aqui começa o inicio do sensograma com o tempo inicial
				   	    		ele faz uma verificação se os tempos eles são diferentes de 0.0, se o tempo
				   	    		for diferente de 0 ele começa a incrementação da primeira substancia
				   	    		com a variavel @EspessuraCustom1 e RefracaoCustom1. Começa com o tempo de inicio e vai 
				   	    		até o tempo_InjecaoAux
				   	    /
						   	if @tempo_InicioAux != 0.0 && @tempo_PassoAux != 0.0 && @tempo_InjecaoAux != 0.0 then
									while @tempo_InicioAux <= @tempo_InjecaoAux do
												@valorSensograma << {:id => 'io', :x => @tempo_InicioAux, :y => @angulo_min}	
										@tempo_InicioAux += @tempo_PassoAux
										#puts @valorSensograma
									end

									
									/
									Essas duas variaveis são para receber os dois angulos minimos que tem,
									O primeiro angulo minimo é o resultado da @EspessuraCustom1 e @RefracaoCustom1
									
									O Segundo Angulo Minimo denominado @anguloAuxMenor ele recebe os valores
									da @EspessuraCustom2 e @RefracaoCustom2
									/
										@anguloAuxMaior = @angulo["angulo"]
										@anguloAuxMenor = @anguloCustom2["angulo"]
									#Essa variavel @tempo_Injecao2 recebe o valor do Tempo_InjecaoAux
									@tempo_Injecao2 = @tempo_InjecaoAux
									#Essa variavel @tempoTotal2 recebe o Tempo total menos o tempo de injeção 
									#3 da terceira substancia
									@tempoTotal2 = @tempo_TotalAux - @tempo_InjecaoAux3	
										#puts @tempoTotal2
								
								    /
									 Aqui começa uma decrementação do angulo ou incrementação de acordo com
									 angulo minimo que é o resultado das variaveis @anguloAuxMaior e @anguloAuxMenor
									 Faz a verificação do Angulo se ele é maior ou menor 	
									/	
									@auxInjecao2 = @tempo_Injecao2
								 if @anguloAuxMaior >= @anguloAuxMenor then
									while @tempo_Injecao2 <= @auxInjecao2 do	
										while @anguloAuxMaior >= @anguloAuxMenor do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao2, :y => @anguloAuxMaior}
											#puts @valorSensograma
											 @anguloAuxMaior -= 0.1
											 @auxInjecao2 = @tempo_Injecao2
											@tempo_Injecao2 += @tempo_PassoAux
										end
									end
								elsif 
									while @tempo_Injecao2 <= @auxInjecao2 do	
										while @anguloAuxMaior <= @anguloAuxMenor do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao2, :y => @anguloAuxMaior}
											#puts @valorSensograma
											 @anguloAuxMaior += 0.1
											 @auxInjecao2 = @tempo_Injecao2
											@tempo_Injecao2 += @tempo_PassoAux
										end
									end
								 end
										
									/Faz um while do tempo aonde ele parou e vai até o tempoTotal2 = @tempo_TotalAux - @tempo_InjecaoAux3
										fazendo a inserção no sensograma.
									/
									while @tempo_Injecao2 <= @tempoTotal2 do
												@valorSensograma << {:id => 'io', :x => @tempo_Injecao2, :y => @angleCustom2}	
										@tempo_Injecao2 += @tempo_PassoAux
										#puts @valorSensograma
									end


									/Essa variavel @tempo_Injecao3 ela recebe o tempo @tempo_Injecao2 aonde parou
									/
									@tempo_Injecao3 = @tempo_Injecao2



									/Essas duas variaveis ela recebe os dois angulos minimos/
									@anguloAuxMaior2 = @anguloCustom2["angulo"]
									@anguloAuxMenor2 = @anguloCustom3["angulo"]
									/Fazendo a verficação para saber se é maior ou menor
									A partir dai, faz uma incrementação do angulo ou decrementação
									/
									@auxInjecao3 = @tempo_Injecao3
								 if @anguloAuxMaior2 >= @anguloAuxMenor2 then
									while @tempo_Injecao3 <= @auxInjecao3 do	
										while @anguloAuxMaior2 >= @anguloAuxMenor2 do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao3, :y => @anguloAuxMaior2}
											#puts @valorSensograma
											 @anguloAuxMaior2 -= 0.1
											 @auxInjecao3 = @tempo_Injecao3
											@tempo_Injecao3 += @tempo_PassoAux
										end
									end
								elsif 
									while @tempo_Injecao3 <= @auxInjecao3 do	
										while @anguloAuxMaior2 <= @anguloAuxMenor2 do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao3, :y => @anguloAuxMaior2}
											#puts @valorSensograma
											 @anguloAuxMaior2 += 0.1
											 @auxInjecao3 = @tempo_Injecao3
											@tempo_Injecao3 += @tempo_PassoAux
										end
									end
								 end

									#puts @tempo_Injecao3
									/Essa parte faz a inserção no sensograma até o tempo Total/
									while @tempo_Injecao3 <= @tempo_TotalAux do
												@valorSensograma << {:id => 'io', :x => @tempo_Injecao3, :y => @angleCustom3}	
											@tempo_Injecao3 += @tempo_PassoAux

										#puts @valorSensograma
									end
		                     end

		                     
					   

					
					 @teta_max = @assimetria.max_by { |z| z["r"] }
					 @teta_min = @assimetria.min_by { |y| y["r"] }
					 @teta_maxCustom2 = @assimetriaCustom2.max_by { |custom2max| custom2max["r"] }
					 @teta_minCustom2 = @assimetriaCustom2.min_by { |custom2min| custom2min["r"] }
					 @teta_maxCustom3 = @assimetriaCustom3.max_by { |custom3max| custom3max["r"] }
					 @teta_minCustom3 = @assimetriaCustom3.min_by { |custom3min| custom3min["r"] }
					  if (@teta_max != nil && @teta_min != nil) || (@teta_maxCustom2 != nil && @teta_minCustom2 != nil) || (@teta_maxCustom3 != nil && @teta_minCustom3 != nil) then 
						@teta_m1 = @teta_max["r"]
						@teta_m2 = @teta_min["r"]

						@tetaCustom2M1 = @teta_maxCustom2["r"]
						@tetaCustom2M2 = @teta_minCustom2["r"]

						@tetaCustom3M1 = @teta_maxCustom3["r"]
						@tetaCustom3M2 = @teta_minCustom3["r"]

						@teta_medio = ((@teta_m1 + @teta_m2)/2).round(2)
						@teta_medioCustom2 = ((@tetaCustom2M1 + @tetaCustom2M2)/2).round(2)
						@teta_medioCustom3 = ((@tetaCustom3M1 + @tetaCustom3M2)/2).round(2)
						
						@teta_aux_v1 = (@teta_min["angulo"]).round(2)
						@teta_aux_v1Custom2 = (@teta_minCustom2["angulo"]).round(2)
						@teta_aux_v1Custom3 = (@teta_minCustom3["angulo"]).round(2)

						/Variaveis começando em 0 para pegar os valores da curva
						como angulo minimo, assimetria e largura da curva/
						@v1 = 0
						@v2 = 0
						@v1Custom2 = 0
						@v2Custom2 = 0
						@v1Custom3 = 0
						@v2Custom3 = 0
						@aux = 0
						@auxCustom2 = 0
						@auxCustom3 = 0
						@vetor = []
						@vetorCustom2 = []
						@vetorCustom3 = []
						@vetor1 = []
						@vetor1Custom2 = []
						@vetor1Custom3 = []

						#For do Hash aonde tem o meus valores de R e o angulo
						#A partir dai vou pegar o meu @valor medio e procurar o 
						# minino do meus angulos
						@assimetria.each do |a|
						  if a["r"] <= @teta_medio then
							if @aux == 0 then
								@v1 = a["angulo"]
							    @aux += 1
							 else
								@v2 = a["angulo"]
							end
						   end
						end

						#For do Hash aonde tem o meus valores de R e o angulo
						#A partir dai vou pegar o meu @valor medio e procurar o 
						# minino do meus angulos dos valores da assimetriaCustom2
						@assimetriaCustom2.each do |custom2|
							if custom2["r"] <= @teta_medioCustom2 then
								if @auxCustom2 == 0 then
									@v1Custom2 = custom2["angulo"]
									@auxCustom2 += 1
								else
									@v2Custom2 = custom2["angulo"]
								 end
							end
						end

						#For do Hash aonde tem o meus valores de R e o angulo
						#A partir dai vou pegar o meu @valor medio e procurar o 
						# minino do meus angulos dos valores da assimetriaCustom3
						@assimetriaCustom3.each do |custom3|
							if custom3["r"] <= @teta_medioCustom3 then
								if @auxCustom3 == 0 then
									@v1Custom3 = custom3["angulo"]
									@auxCustom3 += 1
								else
									@v2Custom3 = custom3["angulo"]
								 end
							end
						end

						#@assimetria.each do |valorR|
						    

						#end

						# if @tempo_InicioAux != nil then
					 	  #while @tempo_InicioAux <= @tempo_InjecaoAux do
					   	  #	@valorSensograma << {"r" => @valor_r, "tempo" => @tempo_InjecaoAux}
					   	  #	@tempo_InjecaoAux += @tempo_PassoAux
					   	 # end
					# end

						#puts "O valor do V1Custom3 = #{@v1Custom3}"
						#puts @v2Custom3
						/Calculo da assimetria e largura
						a variavel @vetor ele está fazendo o calculo da largura
						e @vetor1 está fazendo o calculo da assimetria/
						@vetor = ((@v1 - @v2).abs).round(4)
						@vetor1 = ((@v1/@v2).abs).round(4)

						@vetorCustom2 = ((@v1Custom2 - @v2Custom2).abs).round(4)
						@vetor1Custom2 = ((@v1Custom2/@v2Custom2).abs).round(4)

						@vetorCustom3 = ((@v1Custom3 - @v2Custom3).abs).round(4)
						@vetor1Custom3 = ((@v1Custom3/@v2Custom3).abs).round(4)

						#puts @vetor1Custom3  
					
						@valor_aux = Complex((@n1) * (Math.sin(@angulo_th)))
						@valor_aux1 = Complex((@n1) * (Math.sin(@angulo_th)))
						@aux = @valor_aux * @valor_aux1
						@indice_aux = (@refracao2 * @refracao2).real
						#puts @aux   

						@n3 = Math.sqrt(((@indice_aux*@aux)/(@indice_aux-@aux))).round(4)
						#puts @n3 
					   end
					 
					result = Hash.new
					#result["chartAIM"] = @valor
					result["IRCustom1"] = @refracao7_Custom1
					result["IRCustom2"] = @refracao7_Custom2
					result["IRCustom3"] = @refracao7_Custom3
					result["chartSensograma"] = @valorSensograma
					result["valor_medio"] = @teta_medio
					result["valor_medioCustom2"] = @teta_medioCustom2
					result["valor_medioCustom3"] = @teta_medioCustom3
					result["angulo_minimo"] = "#{@teta_aux_v1} º"
					result["angulo_minimoCustom2"] = "#{@teta_aux_v1Custom2} º"
					result["angulo_minimoCustom3"] = "#{@teta_aux_v1Custom3} º"
					result["largura_certo"] = "#{@vetor} º"
					result["largura_certoCustom2"] = "#{@vetorCustom2} º"
					result["largura_certoCustom3"] = "#{@vetorCustom2} º"
					result["assimetria_certo"] = @vetor1
					result["assimetria_certoCustom2"] = @vetor1Custom2
					result["assimetria_certoCustom3"] = @vetor1Custom3
					result["indiceRefracaoCerto"] = @n3

					respond_to do |format|
					 # format.html { grafico }
					  format.html { graficoSensograma }
					  format.js { render :json => result }
					end
		 
		end

		def simulaCamada8

			@angulo_entrada = params[:anguloEntrada].to_f
			@angulo_saida = params[:anguloSaida].to_f
			@angulo_soma = params[:passo2].to_f
			
			@comprimento_onda = params['onda'].to_f

			@espessura1aux = params['espessuras'] || []
			#puts @espessura1aux

			@espessura1 = 0
			@espessura2 = 0
			@espessura3 = 0
			@espessura4 = 0
			@espessura5 = 0
			@espessura6 = 0
			@espessura7 = 0
			@espessura8_Custom1 = 0
			@espessura8_Custom2 = 0
			@espessura8_Custom3 = 0

			

			if @espessura1aux.length == 7 then
			   @espessura1 = @espessura1aux[0].to_f
			   @espessura2 = @espessura1aux[1].to_f
			   @espessura3 = @espessura1aux[2].to_f
			   @espessura4 = @espessura1aux[3].to_f
			   @espessura5 = @espessura1aux[4].to_f
			   @espessura6 = @espessura1aux[5].to_f
			   @espessura7 = @espessura1aux[6].to_f
			   puts 'entrei l'
			end

				@espessura8_Custom1 = params['EspessuraCustom1'].to_f
			    @espessura8_Custom2 = params['EspessuraCustom2'].to_f		 
			 	@espessura8_Custom3 = params['EspessuraCustom3'].to_f
				

				logger.info("SPR Criado".yellow)

				@refracao1 = calculoRefracao( params['elementos'] ? params['elementos'][0] : 0, @comprimento_onda)
				@refracao2 = calculoRefracao( params['elementos'] ? params['elementos'][1] : 0, @comprimento_onda)
				@refracao3 = calculoRefracao( params['elementos'] ? params['elementos'][2] : 0, @comprimento_onda)
				@refracao4 = calculoRefracao( params['elementos'] ? params['elementos'][3] : 0, @comprimento_onda)
				@refracao5 = calculoRefracao( params['elementos'] ? params['elementos'][4] : 0, @comprimento_onda)
				@refracao6 = calculoRefracao( params['elementos'] ? params['elementos'][5] : 0, @comprimento_onda)
				@refracao7 = calculoRefracao( params['elementos'] ? params['elementos'][6] : 0, @comprimento_onda)
			
				@refracao8_Custom1 = params['RefracaoCustom1'].to_f
			    @refracao8_Custom2 = params['RefracaoCustom2'].to_f
			    @refracao8_Custom3 = params['RefracaoCustom3'].to_f

				@tempo_InicioAux = params['tempoInicio'].to_f
				@tempo_TotalAux = params['tempoTotal'].to_f
				@tempo_PassoAux = params['tempoPasso'].to_f

				@tempo_InjecaoAux = params['tempoInjecao'].to_f
			    @tempo_InjecaoAux2 = params['tempoInjecao2'].to_f
			    @tempo_InjecaoAux3 = params['tempoInjecao3'].to_f
		
			@espessura1 = (0.000000001 * @espessura1)
			@espessura2 = (0.000000001 * @espessura2)
			@espessura3 = (0.000000001 * @espessura3)
			@espessura4 = (0.000000001 * @espessura4)
			@espessura5 = (0.000000001 * @espessura5)
			@espessura6 = (0.000000001 * @espessura6)
			@espessura7 = (0.000000001 * @espessura7)
			@espessura8_Custom1 = (0.000000001 * @espessura8_Custom1)
			@espessura8_Custom2 = (0.000000001 * @espessura8_Custom2)
			@espessura8_Custom3 = (0.000000001 * @espessura8_Custom3)

			@valor = []
			@valor_Custom2 = []
			@valor_Custom3 = []
			@valor1 = []
			@valorSensograma = []
			@assimetria = []
			@assimetriaCustom2 = []
			@assimetriaCustom3 = []

				while @angulo_entrada < @angulo_saida do
					@angulo_th = ((@angulo_entrada * (Math::PI))/(180))
					@k0 = ((2 * (Math::PI))/(0.000000001 * @comprimento_onda))
					@n1 = @refracao1

					#Inicio do qj1
					#qj1 está correto
					@valor1_qj1 = Complex((@refracao1 * @refracao1))
					@valor2aux_qj1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj1 = (@valor2aux_qj1 * @valor3aux_qj1)
					@valor2_qj1 = Math.sqrt(@valor1_qj1 - @valor4aux_qj1)
					@qj1 = Complex(((@valor2_qj1)/(@valor1_qj1)))
					#puts  "Valor do Qj1 = #{@qj1}"
					#Final do qj1

					#Inicio do qj2
					#A variavel @valor6aux_qj2 está pegando a parte real de um numero Complexo do valor @valor5aux_qj2
					#A variavel @valor7aux_qj2 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj2
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj2 = @refracao2 * @refracao2
					@valor2aux_qj2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj2 = (@valor2aux_qj2 * @valor3aux_qj2)
					@valor5aux_qj2 = @valor1_qj2 - @valor4aux_qj2
					@valor6aux_qj2 = @valor5aux_qj2.real
					@valor7aux_qj2 = @valor5aux_qj2.imaginary
					@valor2_qj2 = sqrt2(@valor6aux_qj2,@valor7aux_qj2)
					@qj2 = ((@valor2_qj2)/(@valor1_qj2))
					#Final do qj2

					#Inicio do qj3
					#A variavel @valor6aux_qj3 está pegando a parte real de um numero Complexo do valor @valor5aux_qj3
					#A variavel @valor7aux_qj3 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj3
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj3 = @refracao3 * @refracao3
					@valor2aux_qj3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj3 = (@valor2aux_qj3 * @valor3aux_qj3)
					@valor5aux_qj3 = @valor1_qj3 - @valor4aux_qj3
					@valor6aux_qj3 = @valor5aux_qj3.real
					@valor7aux_qj3 = @valor5aux_qj3.imaginary
					@valor2_qj3 = sqrt2(@valor6aux_qj3,@valor7aux_qj3)
					@qj3 = ((@valor2_qj3)/(@valor1_qj3))
					#Final do qj3

					#Inicio do qj4
					#A variavel @valor6aux_qj4 está pegando a parte real de um numero Complexo do valor @valor5aux_qj4
					#A variavel @valor7aux_qj4 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj4
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj4 = @refracao4 * @refracao4
					@valor2aux_qj4 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj4 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj4 = (@valor2aux_qj4 * @valor3aux_qj4)
					@valor5aux_qj4 = @valor1_qj4 - @valor4aux_qj4
					@valor6aux_qj4 = @valor5aux_qj4.real
					@valor7aux_qj4 = @valor5aux_qj4.imaginary
					@valor2_qj4 = sqrt2(@valor6aux_qj4,@valor7aux_qj4)
					@qj4 = ((@valor2_qj4)/(@valor1_qj4))
					#Final do qj4

					#Inicio do qj5
					#A variavel @valor6aux_qj5 está pegando a parte real de um numero Complexo do valor @valor5aux_qj5
					#A variavel @valor7aux_qj5 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj5
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj5 = @refracao5 * @refracao5
					@valor2aux_qj5 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj5 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj5 = (@valor2aux_qj5 * @valor3aux_qj5)
					@valor5aux_qj5 = @valor1_qj5 - @valor4aux_qj5
					@valor6aux_qj5 = @valor5aux_qj5.real
					@valor7aux_qj5 = @valor5aux_qj5.imaginary
					@valor2_qj5 = sqrt2(@valor6aux_qj5,@valor7aux_qj5)
					@qj5 = ((@valor2_qj5)/(@valor1_qj5))
					#Final do qj5

					#Inicio do qj6
					#A variavel @valor6aux_qj6 está pegando a parte real de um numero Complexo do valor @valor5aux_qj6
					#A variavel @valor7aux_qj6 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj6
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj6 = @refracao6 * @refracao6
					@valor2aux_qj6 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj6 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj6 = (@valor2aux_qj6 * @valor3aux_qj6)
					@valor5aux_qj6 = @valor1_qj6 - @valor4aux_qj6
					@valor6aux_qj6 = @valor5aux_qj6.real
					@valor7aux_qj6 = @valor5aux_qj6.imaginary
					@valor2_qj6 = sqrt2(@valor6aux_qj6,@valor7aux_qj6)
					@qj6 = ((@valor2_qj6)/(@valor1_qj6))
					#Final do qj6

					#Inicio do qj7
					#A variavel @valor6aux_qj7 está pegando a parte real de um numero Complexo do valor @valor5aux_qj7
					#A variavel @valor7aux_qj7 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj7
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj7 = @refracao7 * @refracao7
					@valor2aux_qj7 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj7 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj7 = (@valor2aux_qj7 * @valor3aux_qj7)
					@valor5aux_qj7 = @valor1_qj7 - @valor4aux_qj7
					@valor6aux_qj7 = @valor5aux_qj7.real
					@valor7aux_qj7 = @valor5aux_qj7.imaginary
					@valor2_qj7 = sqrt2(@valor6aux_qj7,@valor7aux_qj7)
					@qj7 = ((@valor2_qj7)/(@valor1_qj7))
					#Final do qj7

					#Inicio do qj8 para a espessura 3 Custom 1 e refração 3 Custom 1
					#Essa parte da diminuição não foi preciso fazer a função
					@valor1_qj8_Custom1 = @refracao8_Custom1 * @refracao8_Custom1
					@valor2aux_qj8_Custom1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj8_Custom1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj8_Custom1 = ((@valor2aux_qj8_Custom1) * (@valor3aux_qj8_Custom1))
					@valor5aux_qj8_Custom1 = (@valor1_qj8_Custom1 - @valor4aux_qj8_Custom1)
					@valor6aux_qj8_Custom1 = @valor5aux_qj8_Custom1.real
					@valor7aux_qj8_Custom1 = @valor5aux_qj8_Custom1.imaginary
					@valor2_qj8_Custom1 = sqrt2(@valor6aux_qj8_Custom1,@valor7aux_qj8_Custom1)
					@qj8_Custom1 = ((@valor2_qj8_Custom1)/(@valor1_qj8_Custom1))
					#Fim do qj8 para a espessura 3 Custom 1 e refração 3 Custom 1

					#Inicio do qj8 para a espessura 3 Custom 2 e refração 3 Custom 2
					#Essa parte da diminuição não foi preciso fazer a função
					@valor1_qj8_Custom2 = @refracao8_Custom2 * @refracao8_Custom2
					@valor2aux_qj8_Custom2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj8_Custom2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj8_Custom2 = ((@valor2aux_qj8_Custom2) * (@valor3aux_qj8_Custom2))
					@valor5aux_qj8_Custom2 = (@valor1_qj8_Custom2 - @valor4aux_qj8_Custom2)
					@valor6aux_qj8_Custom2 = @valor5aux_qj8_Custom2.real
					@valor7aux_qj8_Custom2 = @valor5aux_qj8_Custom2.imaginary
					@valor2_qj8_Custom2 = sqrt2(@valor6aux_qj8_Custom2,@valor7aux_qj8_Custom2)
					@qj8_Custom2 = ((@valor2_qj8_Custom2)/(@valor1_qj8_Custom2))
					#Fim do qj8 para a espessura 3 Custom 2 e refração 3 Custom 2

					#Inicio do qj8 para a espessura 3 Custom 3 e refração 3 Custom 3
					#Essa parte da diminuição não foi preciso fazer a função
					@valor1_qj8_Custom3 = @refracao8_Custom3 * @refracao8_Custom3
					@valor2aux_qj8_Custom3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj8_Custom3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj8_Custom3 = ((@valor2aux_qj8_Custom3) * (@valor3aux_qj8_Custom3))
					@valor5aux_qj8_Custom3 = (@valor1_qj8_Custom3 - @valor4aux_qj8_Custom3)
					@valor6aux_qj8_Custom3 = @valor5aux_qj8_Custom3.real
					@valor7aux_qj8_Custom3 = @valor5aux_qj8_Custom3.imaginary
					@valor2_qj8_Custom3 = sqrt2(@valor6aux_qj8_Custom3,@valor7aux_qj8_Custom3)
					@qj8_Custom3 = ((@valor2_qj8_Custom3)/(@valor1_qj8_Custom3))
					#Fim do qj8 para a espessura 3 Custom 3 e refração 3 Custom 3

					#beta2 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta2 = Complex((@k0 * @espessura2))
					@valor1aux_beta2 = Complex(@refracao2 * @refracao2)
					@valor2aux_beta2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta2 = ((@valor2aux_beta2) * (@valor3aux_beta2))
					@valor5aux_beta2 = ((@valor1aux_beta2) - (@valor4aux_beta2))
					@valor6aux_beta2 = @valor5aux_beta2.real
					@valor7aux_beta2 = @valor5aux_beta2.imaginary
					@valor2beta2 = sqrt2(@valor6aux_beta2,@valor7aux_beta2)
					@beta2 = ((@valor1beta2) * (@valor2beta2))

					#beta3 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta3 = Complex((@k0 * @espessura3))
					@valor1aux_beta3 = Complex(@refracao3 * @refracao3)
					@valor2aux_beta3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta3 = ((@valor2aux_beta3) * (@valor3aux_beta3))
					@valor5aux_beta3 = ((@valor1aux_beta3) - (@valor4aux_beta3))
					@valor6aux_beta3 = @valor5aux_beta3.real
					@valor7aux_beta3 = @valor5aux_beta3.imaginary
					@valor2beta3 = sqrt2(@valor6aux_beta3,@valor7aux_beta3)
					@beta3 = ((@valor1beta3) * (@valor2beta3))

					#beta4 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta4 = Complex((@k0 * @espessura4))
					@valor1aux_beta4 = Complex(@refracao4 * @refracao4)
					@valor2aux_beta4 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta4 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta4 = ((@valor2aux_beta4) * (@valor3aux_beta4))
					@valor5aux_beta4 = ((@valor1aux_beta4) - (@valor4aux_beta4))
					@valor6aux_beta4 = @valor5aux_beta4.real
					@valor7aux_beta4 = @valor5aux_beta4.imaginary
					@valor2beta4 = sqrt2(@valor6aux_beta4,@valor7aux_beta4)
					@beta4 = ((@valor1beta4) * (@valor2beta4))

					#beta5 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta5 = Complex((@k0 * @espessura5))
					@valor1aux_beta5 = Complex(@refracao5 * @refracao5)
					@valor2aux_beta5 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta5 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta5 = ((@valor2aux_beta5) * (@valor3aux_beta5))
					@valor5aux_beta5 = ((@valor1aux_beta5) - (@valor4aux_beta5))
					@valor6aux_beta5 = @valor5aux_beta5.real
					@valor7aux_beta5 = @valor5aux_beta5.imaginary
					@valor2beta5 = sqrt2(@valor6aux_beta5,@valor7aux_beta5)
					@beta5 = ((@valor1beta5) * (@valor2beta5))

					#beta6 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta6 = Complex((@k0 * @espessura6))
					@valor1aux_beta6 = Complex(@refracao6 * @refracao6)
					@valor2aux_beta6 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta6 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta6 = ((@valor2aux_beta6) * (@valor3aux_beta6))
					@valor5aux_beta6 = ((@valor1aux_beta6) - (@valor4aux_beta6))
					@valor6aux_beta6 = @valor5aux_beta6.real
					@valor7aux_beta6 = @valor5aux_beta6.imaginary
					@valor2beta6 = sqrt2(@valor6aux_beta6,@valor7aux_beta6)
					@beta6 = ((@valor1beta6) * (@valor2beta6))

					#beta7 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta7 = Complex((@k0 * @espessura7))
					@valor1aux_beta7 = Complex(@refracao7 * @refracao7)
					@valor2aux_beta7 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta7 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta7 = ((@valor2aux_beta7) * (@valor3aux_beta7))
					@valor5aux_beta7 = ((@valor1aux_beta7) - (@valor4aux_beta7))
					@valor6aux_beta7 = @valor5aux_beta7.real
					@valor7aux_beta7 = @valor5aux_beta7.imaginary
					@valor2beta7 = sqrt2(@valor6aux_beta7,@valor7aux_beta7)
					@beta7 = ((@valor1beta7) * (@valor2beta7))


					@i = (Complex(0,-1))
					
					@M2 = [[],[]]
					@M2[0][0] = Complex((Math.cos(@beta2)))
					@M2[0][1] = Complex(((@i * (Math.sin(@beta2)))/(@qj2)))
					@M2[1][0] = Complex(((@i * (Math.sin(@beta2)))*(@qj2)))
					@M2[1][1] = Complex((Math.cos(@beta2)))

					@M3 = [[],[]]
					@M3[0][0] = Complex((Math.cos(@beta3)))
					@M3[0][1] = Complex(((@i * (Math.sin(@beta3)))/(@qj3)))
					@M3[1][0] = Complex(((@i * (Math.sin(@beta3)))*(@qj3)))
					@M3[1][1] = Complex((Math.cos(@beta3)))

					@M4 = [[],[]]
					@M4[0][0] = Complex((Math.cos(@beta4)))
					@M4[0][1] = Complex(((@i * (Math.sin(@beta4)))/(@qj4)))
					@M4[1][0] = Complex(((@i * (Math.sin(@beta4)))*(@qj4)))
					@M4[1][1] = Complex((Math.cos(@beta4)))

					@M5 = [[],[]]
					@M5[0][0] = Complex((Math.cos(@beta5)))
					@M5[0][1] = Complex(((@i * (Math.sin(@beta5)))/(@qj5)))
					@M5[1][0] = Complex(((@i * (Math.sin(@beta5)))*(@qj5)))
					@M5[1][1] = Complex((Math.cos(@beta5)))

					@M6 = [[],[]]
					@M6[0][0] = Complex((Math.cos(@beta6)))
					@M6[0][1] = Complex(((@i * (Math.sin(@beta6)))/(@qj6)))
					@M6[1][0] = Complex(((@i * (Math.sin(@beta6)))*(@qj6)))
					@M6[1][1] = Complex((Math.cos(@beta6)))

					@M7 = [[],[]]
					@M7[0][0] = Complex((Math.cos(@beta7)))
					@M7[0][1] = Complex(((@i * (Math.sin(@beta7)))/(@qj7)))
					@M7[1][0] = Complex(((@i * (Math.sin(@beta7)))*(@qj7)))
					@M7[1][1] = Complex((Math.cos(@beta7)))

					@M8 = [[],[]]
					@M8[0][0] = Complex(((@M2[0][0] * @M3[0][0]) + (@M2[0][1] * @M3[1][0])))
					@M8[0][1] = Complex(((@M2[0][0] * @M3[0][1]) + (@M2[0][1] * @M3[1][1])))
					@M8[1][0] = Complex(((@M2[1][0] * @M3[0][0]) + (@M2[1][1] * @M3[1][0])))
					@M8[1][1] = Complex(((@M2[1][0] * @M3[0][1]) + (@M2[1][1] * @M3[1][1])))

					@M9 = [[],[]]
					@M9[0][0] = Complex(((@M8[0][0] * @M4[0][0]) + (@M8[0][1] * @M4[1][0])))
					@M9[0][1] = Complex(((@M8[0][0] * @M4[0][1]) + (@M8[0][1] * @M4[1][1])))
					@M9[1][0] = Complex(((@M8[1][0] * @M4[0][0]) + (@M8[1][1] * @M4[1][0])))
					@M9[1][1] = Complex(((@M8[1][0] * @M4[0][1]) + (@M8[1][1] * @M4[1][1])))

					@M10 = [[],[]]
					@M10[0][0] = Complex(((@M9[0][0] * @M5[0][0]) + (@M9[0][1] * @M5[1][0])))
					@M10[0][1] = Complex(((@M9[0][0] * @M5[0][1]) + (@M9[0][1] * @M5[1][1])))
					@M10[1][0] = Complex(((@M9[1][0] * @M5[0][0]) + (@M9[1][1] * @M5[1][0])))
					@M10[1][1] = Complex(((@M9[1][0] * @M5[0][1]) + (@M9[1][1] * @M5[1][1])))

					@M11 = [[],[]]
					@M11[0][0] = Complex(((@M10[0][0] * @M6[0][0]) + (@M10[0][1] * @M6[1][0])))
					@M11[0][1] = Complex(((@M10[0][0] * @M6[0][1]) + (@M10[0][1] * @M6[1][1])))
					@M11[1][0] = Complex(((@M10[1][0] * @M6[0][0]) + (@M10[1][1] * @M6[1][0])))
					@M11[1][1] = Complex(((@M10[1][0] * @M6[0][1]) + (@M10[1][1] * @M6[1][1])))

					@M12 = [[],[]]
					@M12[0][0] = Complex(((@M11[0][0] * @M7[0][0]) + (@M11[0][1] * @M7[1][0])))
					@M12[0][1] = Complex(((@M11[0][0] * @M7[0][1]) + (@M11[0][1] * @M7[1][1])))
					@M12[1][0] = Complex(((@M11[1][0] * @M7[0][0]) + (@M11[1][1] * @M7[1][0])))
					@M12[1][1] = Complex(((@M11[1][0] * @M7[0][1]) + (@M11[1][1] * @M7[1][1])))

					#Primeiro calculo da soma de matriz do coeficiente de fresnel para a espessura3_Custom1
					#Que é o qj8_Custom1
					@r_n = ((((@M12[0][1] * @qj8_Custom1) + @M12[0][0]) * @qj1) - ((@M12[1][1] * @qj8_Custom1) + (@M12[1][0])))
					@r_d = ((((@M12[0][1] * @qj8_Custom1) + @M12[0][0]) * @qj1) + ((@M12[1][1] * @qj8_Custom1) + (@M12[1][0])))
					#Segundo calculo da soma de matriz do coeficiente de fresnel para a espessura3_Custom2
					#Que é o qj8_Custom2
					@r_n_custom2 = ((((@M12[0][1] * @qj8_Custom2) + @M12[0][0]) * @qj1) - ((@M12[1][1] * @qj8_Custom2) + (@M12[1][0])))
					@r_d_custom2 = ((((@M12[0][1] * @qj8_Custom2) + @M12[0][0]) * @qj1) + ((@M12[1][1] * @qj8_Custom2) + (@M12[1][0])))
					#Terceiro calculo da soma de matriz do coeficiente de fresnel para a espessura3_Custom3
					#Que é o qj8_Custom3
					@r_n_custom3 = ((((@M12[0][1] * @qj8_Custom3) + @M12[0][0]) * @qj1) - ((@M12[1][1] * @qj8_Custom3) + (@M12[1][0])))
					@r_d_custom3 = ((((@M12[0][1] * @qj8_Custom3) + @M12[0][0]) * @qj1) + ((@M12[1][1] * @qj8_Custom3) + (@M12[1][0])))

					#Divisão do resultado @r_n/@r_d
					@r = Complex(((@r_n)/(@r_d)))

					#puts @r

					#Divisão do resultado @r_n_custom2/@r_d_custom2
					@r_Custom2 = Complex(((@r_n_custom2)/(@r_d_custom2)))

					#puts @r_Custom2
					#Divisão do resultado @r_n_custom3/@r_d_custom3
					@r_Custom3 = Complex(((@r_n_custom3)/(@r_d_custom3)))

					#puts @r_Custom3

					#valor de r da espessura3_custom1
					@valor_r = Complex(((@r * (@r.conjugate))).real)

					#valor de r da espessura3_custom2
					@valor_r_custom2 = Complex(((@r_Custom2 * (@r_Custom2.conjugate))).real)

					#valor de r da espessura3_custom3
					@valor_r_custom3 = Complex(((@r_Custom3 * (@r_Custom3.conjugate))).real)

					@valor << { :id => 'io', :x => @angulo_entrada, :y => @valor_r}

					@valor_Custom2 << { :id => 'io', :x => @angulo_entrada, :y => @valor_r_custom2}

					@valor_Custom3 << { :id => 'io', :x => @angulo_entrada, :y => @valor_r_custom3}

					@angulo_entrada += @angulo_soma
					
					@assimetria <<  {"r" => @valor_r, "angulo" => @angulo_entrada}        
				   	@assimetriaCustom2 << {"r" => @valor_r_custom2, "angulo" => @angulo_entrada}
				   	@assimetriaCustom3 << {"r" => @valor_r_custom3, "angulo" => @angulo_entrada}
				   
				end

					#Angulo Minimo
				  @angulo = @assimetria.min_by { |x| x["r"] }
				  	@anguloCustom2 = @assimetriaCustom2.min_by {|angCustom2| angCustom2["r"] }
						@anguloCustom3 = @assimetriaCustom3.min_by {|angCustom3| angCustom3["r"] }
					   if @angulo != nil || @anguloCustmo2 != nil || @anguloCustom3 != nil then 
					   	
					   	@angulo_min = @angulo["angulo"]
					   	@angleCustom2 = @anguloCustom2["angulo"]
				   	    @angleCustom3 = @anguloCustom3["angulo"]
				   	   
				   	    end
				   	    /
				   	    	Aqui começa o inicio do sensograma com o tempo inicial
				   	    		ele faz uma verificação se os tempos eles são diferentes de 0.0, se o tempo
				   	    		for diferente de 0 ele começa a incrementação da primeira substancia
				   	    		com a variavel @EspessuraCustom1 e RefracaoCustom1. Começa com o tempo de inicio e vai 
				   	    		até o tempo_InjecaoAux
				   	    /
						   	if @tempo_InicioAux != 0.0 && @tempo_PassoAux != 0.0 && @tempo_InjecaoAux != 0.0 then
									while @tempo_InicioAux <= @tempo_InjecaoAux do
												@valorSensograma << {:id => 'io', :x => @tempo_InicioAux, :y => @angulo_min}	
										@tempo_InicioAux += @tempo_PassoAux
										#puts @valorSensograma
									end

									
									/
									Essas duas variaveis são para receber os dois angulos minimos que tem,
									O primeiro angulo minimo é o resultado da @EspessuraCustom1 e @RefracaoCustom1
									
									O Segundo Angulo Minimo denominado @anguloAuxMenor ele recebe os valores
									da @EspessuraCustom2 e @RefracaoCustom2
									/
										@anguloAuxMaior = @angulo["angulo"]
										@anguloAuxMenor = @anguloCustom2["angulo"]
									#Essa variavel @tempo_Injecao2 recebe o valor do Tempo_InjecaoAux
									@tempo_Injecao2 = @tempo_InjecaoAux
									#Essa variavel @tempoTotal2 recebe o Tempo total menos o tempo de injeção 
									#3 da terceira substancia
									@tempoTotal2 = @tempo_TotalAux - @tempo_InjecaoAux3	
										#puts @tempoTotal2
								
								    /
									 Aqui começa uma decrementação do angulo ou incrementação de acordo com
									 angulo minimo que é o resultado das variaveis @anguloAuxMaior e @anguloAuxMenor
									 Faz a verificação do Angulo se ele é maior ou menor 	
									/	
									@auxInjecao2 = @tempo_Injecao2
								 if @anguloAuxMaior >= @anguloAuxMenor then
									while @tempo_Injecao2 <= @auxInjecao2 do	
										while @anguloAuxMaior >= @anguloAuxMenor do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao2, :y => @anguloAuxMaior}
											#puts @valorSensograma
											 @anguloAuxMaior -= 0.1
											 @auxInjecao2 = @tempo_Injecao2
											@tempo_Injecao2 += @tempo_PassoAux
										end
									end
								elsif 
									while @tempo_Injecao2 <= @auxInjecao2 do	
										while @anguloAuxMaior <= @anguloAuxMenor do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao2, :y => @anguloAuxMaior}
											#puts @valorSensograma
											 @anguloAuxMaior += 0.1
											 @auxInjecao2 = @tempo_Injecao2
											@tempo_Injecao2 += @tempo_PassoAux
										end
									end
								 end
										
									/Faz um while do tempo aonde ele parou e vai até o tempoTotal2 = @tempo_TotalAux - @tempo_InjecaoAux3
										fazendo a inserção no sensograma.
									/
									while @tempo_Injecao2 <= @tempoTotal2 do
												@valorSensograma << {:id => 'io', :x => @tempo_Injecao2, :y => @angleCustom2}	
										@tempo_Injecao2 += @tempo_PassoAux
										#puts @valorSensograma
									end


									/Essa variavel @tempo_Injecao3 ela recebe o tempo @tempo_Injecao2 aonde parou
									/
									@tempo_Injecao3 = @tempo_Injecao2



									/Essas duas variaveis ela recebe os dois angulos minimos/
									@anguloAuxMaior2 = @anguloCustom2["angulo"]
									@anguloAuxMenor2 = @anguloCustom3["angulo"]
									/Fazendo a verficação para saber se é maior ou menor
									A partir dai, faz uma incrementação do angulo ou decrementação
									/
									@auxInjecao3 = @tempo_Injecao3
								 if @anguloAuxMaior2 >= @anguloAuxMenor2 then
									while @tempo_Injecao3 <= @auxInjecao3 do	
										while @anguloAuxMaior2 >= @anguloAuxMenor2 do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao3, :y => @anguloAuxMaior2}
											#puts @valorSensograma
											 @anguloAuxMaior2 -= 0.1
											 @auxInjecao3 = @tempo_Injecao3
											@tempo_Injecao3 += @tempo_PassoAux
										end
									end
								elsif 
									while @tempo_Injecao3 <= @auxInjecao3 do	
										while @anguloAuxMaior2 <= @anguloAuxMenor2 do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao3, :y => @anguloAuxMaior2}
											#puts @valorSensograma
											 @anguloAuxMaior2 += 0.1
											 @auxInjecao3 = @tempo_Injecao3
											@tempo_Injecao3 += @tempo_PassoAux
										end
									end
								 end

									#puts @tempo_Injecao3
									/Essa parte faz a inserção no sensograma até o tempo Total/
									while @tempo_Injecao3 <= @tempo_TotalAux do
												@valorSensograma << {:id => 'io', :x => @tempo_Injecao3, :y => @angleCustom3}	
											@tempo_Injecao3 += @tempo_PassoAux

										#puts @valorSensograma
									end
		                     end

		                     
					   

					
					 @teta_max = @assimetria.max_by { |z| z["r"] }
					 @teta_min = @assimetria.min_by { |y| y["r"] }
					 @teta_maxCustom2 = @assimetriaCustom2.max_by { |custom2max| custom2max["r"] }
					 @teta_minCustom2 = @assimetriaCustom2.min_by { |custom2min| custom2min["r"] }
					 @teta_maxCustom3 = @assimetriaCustom3.max_by { |custom3max| custom3max["r"] }
					 @teta_minCustom3 = @assimetriaCustom3.min_by { |custom3min| custom3min["r"] }
					  if (@teta_max != nil && @teta_min != nil) || (@teta_maxCustom2 != nil && @teta_minCustom2 != nil) || (@teta_maxCustom3 != nil && @teta_minCustom3 != nil) then 
						@teta_m1 = @teta_max["r"]
						@teta_m2 = @teta_min["r"]

						@tetaCustom2M1 = @teta_maxCustom2["r"]
						@tetaCustom2M2 = @teta_minCustom2["r"]

						@tetaCustom3M1 = @teta_maxCustom3["r"]
						@tetaCustom3M2 = @teta_minCustom3["r"]

						@teta_medio = ((@teta_m1 + @teta_m2)/2).round(2)
						@teta_medioCustom2 = ((@tetaCustom2M1 + @tetaCustom2M2)/2).round(2)
						@teta_medioCustom3 = ((@tetaCustom3M1 + @tetaCustom3M2)/2).round(2)
						
						@teta_aux_v1 = (@teta_min["angulo"]).round(2)
						@teta_aux_v1Custom2 = (@teta_minCustom2["angulo"]).round(2)
						@teta_aux_v1Custom3 = (@teta_minCustom3["angulo"]).round(2)

						/Variaveis começando em 0 para pegar os valores da curva
						como angulo minimo, assimetria e largura da curva/
						@v1 = 0
						@v2 = 0
						@v1Custom2 = 0
						@v2Custom2 = 0
						@v1Custom3 = 0
						@v2Custom3 = 0
						@aux = 0
						@auxCustom2 = 0
						@auxCustom3 = 0
						@vetor = []
						@vetorCustom2 = []
						@vetorCustom3 = []
						@vetor1 = []
						@vetor1Custom2 = []
						@vetor1Custom3 = []

						#For do Hash aonde tem o meus valores de R e o angulo
						#A partir dai vou pegar o meu @valor medio e procurar o 
						# minino do meus angulos
						@assimetria.each do |a|
						  if a["r"] <= @teta_medio then
							if @aux == 0 then
								@v1 = a["angulo"]
							    @aux += 1
							 else
								@v2 = a["angulo"]
							end
						   end
						end

						#For do Hash aonde tem o meus valores de R e o angulo
						#A partir dai vou pegar o meu @valor medio e procurar o 
						# minino do meus angulos dos valores da assimetriaCustom2
						@assimetriaCustom2.each do |custom2|
							if custom2["r"] <= @teta_medioCustom2 then
								if @auxCustom2 == 0 then
									@v1Custom2 = custom2["angulo"]
									@auxCustom2 += 1
								else
									@v2Custom2 = custom2["angulo"]
								 end
							end
						end

						#For do Hash aonde tem o meus valores de R e o angulo
						#A partir dai vou pegar o meu @valor medio e procurar o 
						# minino do meus angulos dos valores da assimetriaCustom3
						@assimetriaCustom3.each do |custom3|
							if custom3["r"] <= @teta_medioCustom3 then
								if @auxCustom3 == 0 then
									@v1Custom3 = custom3["angulo"]
									@auxCustom3 += 1
								else
									@v2Custom3 = custom3["angulo"]
								 end
							end
						end

						#@assimetria.each do |valorR|
						    

						#end

						# if @tempo_InicioAux != nil then
					 	  #while @tempo_InicioAux <= @tempo_InjecaoAux do
					   	  #	@valorSensograma << {"r" => @valor_r, "tempo" => @tempo_InjecaoAux}
					   	  #	@tempo_InjecaoAux += @tempo_PassoAux
					   	 # end
					# end

						#puts "O valor do V1Custom3 = #{@v1Custom3}"
						#puts @v2Custom3
						/Calculo da assimetria e largura
						a variavel @vetor ele está fazendo o calculo da largura
						e @vetor1 está fazendo o calculo da assimetria/
						@vetor = ((@v1 - @v2).abs).round(4)
						@vetor1 = ((@v1/@v2).abs).round(4)

						@vetorCustom2 = ((@v1Custom2 - @v2Custom2).abs).round(4)
						@vetor1Custom2 = ((@v1Custom2/@v2Custom2).abs).round(4)

						@vetorCustom3 = ((@v1Custom3 - @v2Custom3).abs).round(4)
						@vetor1Custom3 = ((@v1Custom3/@v2Custom3).abs).round(4)

						#puts @vetor1Custom3  
					
						@valor_aux = Complex((@n1) * (Math.sin(@angulo_th)))
						@valor_aux1 = Complex((@n1) * (Math.sin(@angulo_th)))
						@aux = @valor_aux * @valor_aux1
						@indice_aux = (@refracao2 * @refracao2).real
						#puts @aux   

						@n3 = Math.sqrt(((@indice_aux*@aux)/(@indice_aux-@aux))).round(4)
						#puts @n3 
					   end
					 
					result = Hash.new
					#result["chartAIM"] = @valor
					result["IRCustom1"] = @refracao8_Custom1
					result["IRCustom2"] = @refracao8_Custom2
					result["IRCustom3"] = @refracao8_Custom3
					result["chartSensograma"] = @valorSensograma
					result["valor_medio"] = @teta_medio
					result["valor_medioCustom2"] = @teta_medioCustom2
					result["valor_medioCustom3"] = @teta_medioCustom3
					result["angulo_minimo"] = "#{@teta_aux_v1} º"
					result["angulo_minimoCustom2"] = "#{@teta_aux_v1Custom2} º"
					result["angulo_minimoCustom3"] = "#{@teta_aux_v1Custom3} º"
					result["largura_certo"] = "#{@vetor} º"
					result["largura_certoCustom2"] = "#{@vetorCustom2} º"
					result["largura_certoCustom3"] = "#{@vetorCustom2} º"
					result["assimetria_certo"] = @vetor1
					result["assimetria_certoCustom2"] = @vetor1Custom2
					result["assimetria_certoCustom3"] = @vetor1Custom3
					result["indiceRefracaoCerto"] = @n3

					respond_to do |format|
					 # format.html { grafico }
					  format.html { graficoSensograma }
					  format.js { render :json => result }
					end
		 
	end

	def simulaCamada9

			@angulo_entrada = params[:anguloEntrada].to_f
			@angulo_saida = params[:anguloSaida].to_f
			@angulo_soma = params[:passo2].to_f
			
			@comprimento_onda = params['onda'].to_f

			@espessura1aux = params['espessuras'] || []
			#puts @espessura1aux

			@espessura1 = 0
			@espessura2 = 0
			@espessura3 = 0
			@espessura4 = 0
			@espessura5 = 0
			@espessura6 = 0
			@espessura7 = 0
			@espessura8 = 0
			@espessura9_Custom1 = 0
			@espessura9_Custom2 = 0
			@espessura9_Custom3 = 0

			

			if @espessura1aux.length == 8 then
			   @espessura1 = @espessura1aux[0].to_f
			   @espessura2 = @espessura1aux[1].to_f
			   @espessura3 = @espessura1aux[2].to_f
			   @espessura4 = @espessura1aux[3].to_f
			   @espessura5 = @espessura1aux[4].to_f
			   @espessura6 = @espessura1aux[5].to_f
			   @espessura7 = @espessura1aux[6].to_f
			   @espessura8 = @espessura1aux[7].to_f
			   puts 'entrei l'
			end

				@espessura9_Custom1 = params['EspessuraCustom1'].to_f
			    @espessura9_Custom2 = params['EspessuraCustom2'].to_f		 
			 	@espessura9_Custom3 = params['EspessuraCustom3'].to_f
				

				logger.info("SPR Criado".yellow)

				@refracao1 = calculoRefracao( params['elementos'] ? params['elementos'][0] : 0, @comprimento_onda)
				@refracao2 = calculoRefracao( params['elementos'] ? params['elementos'][1] : 0, @comprimento_onda)
				@refracao3 = calculoRefracao( params['elementos'] ? params['elementos'][2] : 0, @comprimento_onda)
				@refracao4 = calculoRefracao( params['elementos'] ? params['elementos'][3] : 0, @comprimento_onda)
				@refracao5 = calculoRefracao( params['elementos'] ? params['elementos'][4] : 0, @comprimento_onda)
				@refracao6 = calculoRefracao( params['elementos'] ? params['elementos'][5] : 0, @comprimento_onda)
				@refracao7 = calculoRefracao( params['elementos'] ? params['elementos'][6] : 0, @comprimento_onda)
				@refracao8 = calculoRefracao( params['elementos'] ? params['elementos'][7] : 0, @comprimento_onda)
			
				@refracao9_Custom1 = params['RefracaoCustom1'].to_f
			    @refracao9_Custom2 = params['RefracaoCustom2'].to_f
			    @refracao9_Custom3 = params['RefracaoCustom3'].to_f

				@tempo_InicioAux = params['tempoInicio'].to_f
				@tempo_TotalAux = params['tempoTotal'].to_f
				@tempo_PassoAux = params['tempoPasso'].to_f

				@tempo_InjecaoAux = params['tempoInjecao'].to_f
			    @tempo_InjecaoAux2 = params['tempoInjecao2'].to_f
			    @tempo_InjecaoAux3 = params['tempoInjecao3'].to_f
		
			@espessura1 = (0.000000001 * @espessura1)
			@espessura2 = (0.000000001 * @espessura2)
			@espessura3 = (0.000000001 * @espessura3)
			@espessura4 = (0.000000001 * @espessura4)
			@espessura5 = (0.000000001 * @espessura5)
			@espessura6 = (0.000000001 * @espessura6)
			@espessura7 = (0.000000001 * @espessura7)
			@espessura8 = (0.000000001 * @espessura8)
			@espessura9_Custom1 = (0.000000001 * @espessura9_Custom1)
			@espessura9_Custom2 = (0.000000001 * @espessura9_Custom2)
			@espessura9_Custom3 = (0.000000001 * @espessura9_Custom3)

			@valor = []
			@valor_Custom2 = []
			@valor_Custom3 = []
			@valor1 = []
			@valorSensograma = []
			@assimetria = []
			@assimetriaCustom2 = []
			@assimetriaCustom3 = []

				while @angulo_entrada < @angulo_saida do
					@angulo_th = ((@angulo_entrada * (Math::PI))/(180))
					@k0 = ((2 * (Math::PI))/(0.000000001 * @comprimento_onda))
					@n1 = @refracao1

					#Inicio do qj1
					#qj1 está correto
					@valor1_qj1 = Complex((@refracao1 * @refracao1))
					@valor2aux_qj1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj1 = (@valor2aux_qj1 * @valor3aux_qj1)
					@valor2_qj1 = Math.sqrt(@valor1_qj1 - @valor4aux_qj1)
					@qj1 = Complex(((@valor2_qj1)/(@valor1_qj1)))
					#puts  "Valor do Qj1 = #{@qj1}"
					#Final do qj1

					#Inicio do qj2
					#A variavel @valor6aux_qj2 está pegando a parte real de um numero Complexo do valor @valor5aux_qj2
					#A variavel @valor7aux_qj2 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj2
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj2 = @refracao2 * @refracao2
					@valor2aux_qj2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj2 = (@valor2aux_qj2 * @valor3aux_qj2)
					@valor5aux_qj2 = @valor1_qj2 - @valor4aux_qj2
					@valor6aux_qj2 = @valor5aux_qj2.real
					@valor7aux_qj2 = @valor5aux_qj2.imaginary
					@valor2_qj2 = sqrt2(@valor6aux_qj2,@valor7aux_qj2)
					@qj2 = ((@valor2_qj2)/(@valor1_qj2))
					#Final do qj2

					#Inicio do qj3
					#A variavel @valor6aux_qj3 está pegando a parte real de um numero Complexo do valor @valor5aux_qj3
					#A variavel @valor7aux_qj3 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj3
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj3 = @refracao3 * @refracao3
					@valor2aux_qj3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj3 = (@valor2aux_qj3 * @valor3aux_qj3)
					@valor5aux_qj3 = @valor1_qj3 - @valor4aux_qj3
					@valor6aux_qj3 = @valor5aux_qj3.real
					@valor7aux_qj3 = @valor5aux_qj3.imaginary
					@valor2_qj3 = sqrt2(@valor6aux_qj3,@valor7aux_qj3)
					@qj3 = ((@valor2_qj3)/(@valor1_qj3))
					#Final do qj3

					#Inicio do qj4
					#A variavel @valor6aux_qj4 está pegando a parte real de um numero Complexo do valor @valor5aux_qj4
					#A variavel @valor7aux_qj4 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj4
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj4 = @refracao4 * @refracao4
					@valor2aux_qj4 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj4 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj4 = (@valor2aux_qj4 * @valor3aux_qj4)
					@valor5aux_qj4 = @valor1_qj4 - @valor4aux_qj4
					@valor6aux_qj4 = @valor5aux_qj4.real
					@valor7aux_qj4 = @valor5aux_qj4.imaginary
					@valor2_qj4 = sqrt2(@valor6aux_qj4,@valor7aux_qj4)
					@qj4 = ((@valor2_qj4)/(@valor1_qj4))
					#Final do qj4

					#Inicio do qj5
					#A variavel @valor6aux_qj5 está pegando a parte real de um numero Complexo do valor @valor5aux_qj5
					#A variavel @valor7aux_qj5 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj5
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj5 = @refracao5 * @refracao5
					@valor2aux_qj5 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj5 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj5 = (@valor2aux_qj5 * @valor3aux_qj5)
					@valor5aux_qj5 = @valor1_qj5 - @valor4aux_qj5
					@valor6aux_qj5 = @valor5aux_qj5.real
					@valor7aux_qj5 = @valor5aux_qj5.imaginary
					@valor2_qj5 = sqrt2(@valor6aux_qj5,@valor7aux_qj5)
					@qj5 = ((@valor2_qj5)/(@valor1_qj5))
					#Final do qj5

					#Inicio do qj6
					#A variavel @valor6aux_qj6 está pegando a parte real de um numero Complexo do valor @valor5aux_qj6
					#A variavel @valor7aux_qj6 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj6
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj6 = @refracao6 * @refracao6
					@valor2aux_qj6 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj6 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj6 = (@valor2aux_qj6 * @valor3aux_qj6)
					@valor5aux_qj6 = @valor1_qj6 - @valor4aux_qj6
					@valor6aux_qj6 = @valor5aux_qj6.real
					@valor7aux_qj6 = @valor5aux_qj6.imaginary
					@valor2_qj6 = sqrt2(@valor6aux_qj6,@valor7aux_qj6)
					@qj6 = ((@valor2_qj6)/(@valor1_qj6))
					#Final do qj6

					#Inicio do qj7
					#A variavel @valor6aux_qj7 está pegando a parte real de um numero Complexo do valor @valor5aux_qj7
					#A variavel @valor7aux_qj7 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj7
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj7 = @refracao7 * @refracao7
					@valor2aux_qj7 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj7 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj7 = (@valor2aux_qj7 * @valor3aux_qj7)
					@valor5aux_qj7 = @valor1_qj7 - @valor4aux_qj7
					@valor6aux_qj7 = @valor5aux_qj7.real
					@valor7aux_qj7 = @valor5aux_qj7.imaginary
					@valor2_qj7 = sqrt2(@valor6aux_qj7,@valor7aux_qj7)
					@qj7 = ((@valor2_qj7)/(@valor1_qj7))
					#Final do qj7

					#Inicio do qj8
					#A variavel @valor6aux_qj8 está pegando a parte real de um numero Complexo do valor @valor5aux_qj8
					#A variavel @valor7aux_qj8 está pegando a parte imaginaria de um numero Complexo do valor @valor5aux_qj8
					#Essa função Sqrt2 é a função correta para tirar a raiz, está no final do código
					@valor1_qj8 = @refracao8 * @refracao8
					@valor2aux_qj8 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj8 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj8 = (@valor2aux_qj8 * @valor3aux_qj8)
					@valor5aux_qj8 = @valor1_qj8 - @valor4aux_qj8
					@valor6aux_qj8 = @valor5aux_qj8.real
					@valor7aux_qj8 = @valor5aux_qj8.imaginary
					@valor2_qj8 = sqrt2(@valor6aux_qj8,@valor7aux_qj8)
					@qj8 = ((@valor2_qj8)/(@valor1_qj8))
					#Final do qj8

					#Inicio do qj9 para a espessura 3 Custom 1 e refração 3 Custom 1
					#Essa parte da diminuição não foi preciso fazer a função
					@valor1_qj9_Custom1 = @refracao9_Custom1 * @refracao9_Custom1
					@valor2aux_qj9_Custom1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj9_Custom1 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj9_Custom1 = ((@valor2aux_qj9_Custom1) * (@valor3aux_qj9_Custom1))
					@valor5aux_qj9_Custom1 = (@valor1_qj9_Custom1 - @valor4aux_qj9_Custom1)
					@valor6aux_qj9_Custom1 = @valor5aux_qj9_Custom1.real
					@valor7aux_qj9_Custom1 = @valor5aux_qj9_Custom1.imaginary
					@valor2_qj9_Custom1 = sqrt2(@valor6aux_qj9_Custom1,@valor7aux_qj9_Custom1)
					@qj9_Custom1 = ((@valor2_qj9_Custom1)/(@valor1_qj9_Custom1))
					#Fim do qj9 para a espessura 3 Custom 1 e refração 3 Custom 1

					#Inicio do qj9 para a espessura 3 Custom 2 e refração 3 Custom 2
					#Essa parte da diminuição não foi preciso fazer a função
					@valor1_qj9_Custom2 = @refracao9_Custom2 * @refracao9_Custom2
					@valor2aux_qj9_Custom2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj9_Custom2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj9_Custom2 = ((@valor2aux_qj9_Custom2) * (@valor3aux_qj9_Custom2))
					@valor5aux_qj9_Custom2 = (@valor1_qj9_Custom2 - @valor4aux_qj9_Custom2)
					@valor6aux_qj9_Custom2 = @valor5aux_qj9_Custom2.real
					@valor7aux_qj9_Custom2 = @valor5aux_qj9_Custom2.imaginary
					@valor2_qj9_Custom2 = sqrt2(@valor6aux_qj9_Custom2,@valor7aux_qj9_Custom2)
					@qj9_Custom2 = ((@valor2_qj9_Custom2)/(@valor1_qj9_Custom2))
					#Fim do qj9 para a espessura 3 Custom 2 e refração 3 Custom 2

					#Inicio do qj9 para a espessura 3 Custom 3 e refração 3 Custom 3
					#Essa parte da diminuição não foi preciso fazer a função
					@valor1_qj9_Custom3 = @refracao9_Custom3 * @refracao9_Custom3
					@valor2aux_qj9_Custom3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_qj9_Custom3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_qj9_Custom3 = ((@valor2aux_qj9_Custom3) * (@valor3aux_qj9_Custom3))
					@valor5aux_qj9_Custom3 = (@valor1_qj9_Custom3 - @valor4aux_qj9_Custom3)
					@valor6aux_qj9_Custom3 = @valor5aux_qj9_Custom3.real
					@valor7aux_qj9_Custom3 = @valor5aux_qj9_Custom3.imaginary
					@valor2_qj9_Custom3 = sqrt2(@valor6aux_qj9_Custom3,@valor7aux_qj9_Custom3)
					@qj9_Custom3 = ((@valor2_qj9_Custom3)/(@valor1_qj9_Custom3))
					#Fim do qj9 para a espessura 3 Custom 3 e refração 3 Custom 3

					#beta2 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta2 = Complex((@k0 * @espessura2))
					@valor1aux_beta2 = Complex(@refracao2 * @refracao2)
					@valor2aux_beta2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta2 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta2 = ((@valor2aux_beta2) * (@valor3aux_beta2))
					@valor5aux_beta2 = ((@valor1aux_beta2) - (@valor4aux_beta2))
					@valor6aux_beta2 = @valor5aux_beta2.real
					@valor7aux_beta2 = @valor5aux_beta2.imaginary
					@valor2beta2 = sqrt2(@valor6aux_beta2,@valor7aux_beta2)
					@beta2 = ((@valor1beta2) * (@valor2beta2))

					#beta3 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta3 = Complex((@k0 * @espessura3))
					@valor1aux_beta3 = Complex(@refracao3 * @refracao3)
					@valor2aux_beta3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta3 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta3 = ((@valor2aux_beta3) * (@valor3aux_beta3))
					@valor5aux_beta3 = ((@valor1aux_beta3) - (@valor4aux_beta3))
					@valor6aux_beta3 = @valor5aux_beta3.real
					@valor7aux_beta3 = @valor5aux_beta3.imaginary
					@valor2beta3 = sqrt2(@valor6aux_beta3,@valor7aux_beta3)
					@beta3 = ((@valor1beta3) * (@valor2beta3))

					#beta4 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta4 = Complex((@k0 * @espessura4))
					@valor1aux_beta4 = Complex(@refracao4 * @refracao4)
					@valor2aux_beta4 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta4 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta4 = ((@valor2aux_beta4) * (@valor3aux_beta4))
					@valor5aux_beta4 = ((@valor1aux_beta4) - (@valor4aux_beta4))
					@valor6aux_beta4 = @valor5aux_beta4.real
					@valor7aux_beta4 = @valor5aux_beta4.imaginary
					@valor2beta4 = sqrt2(@valor6aux_beta4,@valor7aux_beta4)
					@beta4 = ((@valor1beta4) * (@valor2beta4))

					#beta5 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta5 = Complex((@k0 * @espessura5))
					@valor1aux_beta5 = Complex(@refracao5 * @refracao5)
					@valor2aux_beta5 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta5 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta5 = ((@valor2aux_beta5) * (@valor3aux_beta5))
					@valor5aux_beta5 = ((@valor1aux_beta5) - (@valor4aux_beta5))
					@valor6aux_beta5 = @valor5aux_beta5.real
					@valor7aux_beta5 = @valor5aux_beta5.imaginary
					@valor2beta5 = sqrt2(@valor6aux_beta5,@valor7aux_beta5)
					@beta5 = ((@valor1beta5) * (@valor2beta5))

					#beta6 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta6 = Complex((@k0 * @espessura6))
					@valor1aux_beta6 = Complex(@refracao6 * @refracao6)
					@valor2aux_beta6 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta6 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta6 = ((@valor2aux_beta6) * (@valor3aux_beta6))
					@valor5aux_beta6 = ((@valor1aux_beta6) - (@valor4aux_beta6))
					@valor6aux_beta6 = @valor5aux_beta6.real
					@valor7aux_beta6 = @valor5aux_beta6.imaginary
					@valor2beta6 = sqrt2(@valor6aux_beta6,@valor7aux_beta6)
					@beta6 = ((@valor1beta6) * (@valor2beta6))

					#beta7 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta7 = Complex((@k0 * @espessura7))
					@valor1aux_beta7 = Complex(@refracao7 * @refracao7)
					@valor2aux_beta7 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta7 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta7 = ((@valor2aux_beta7) * (@valor3aux_beta7))
					@valor5aux_beta7 = ((@valor1aux_beta7) - (@valor4aux_beta7))
					@valor6aux_beta7 = @valor5aux_beta7.real
					@valor7aux_beta7 = @valor5aux_beta7.imaginary
					@valor2beta7 = sqrt2(@valor6aux_beta7,@valor7aux_beta7)
					@beta7 = ((@valor1beta7) * (@valor2beta7))

					#beta8 = 2*pi*d2*sqrt(n2²-(n1*sin(theta))²)/lambda    
					@valor1beta8 = Complex((@k0 * @espessura8))
					@valor1aux_beta8 = Complex(@refracao8 * @refracao8)
					@valor2aux_beta8 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor3aux_beta8 = Complex((@n1) * (Math.sin(@angulo_th)))
					@valor4aux_beta8 = ((@valor2aux_beta8) * (@valor3aux_beta8))
					@valor5aux_beta8 = ((@valor1aux_beta8) - (@valor4aux_beta8))
					@valor6aux_beta8 = @valor5aux_beta8.real
					@valor7aux_beta8 = @valor5aux_beta8.imaginary
					@valor2beta8 = sqrt2(@valor6aux_beta8,@valor7aux_beta8)
					@beta8 = ((@valor1beta8) * (@valor2beta8))


					@i = (Complex(0,-1))
					
					@M2 = [[],[]]
					@M2[0][0] = Complex((Math.cos(@beta2)))
					@M2[0][1] = Complex(((@i * (Math.sin(@beta2)))/(@qj2)))
					@M2[1][0] = Complex(((@i * (Math.sin(@beta2)))*(@qj2)))
					@M2[1][1] = Complex((Math.cos(@beta2)))

					@M3 = [[],[]]
					@M3[0][0] = Complex((Math.cos(@beta3)))
					@M3[0][1] = Complex(((@i * (Math.sin(@beta3)))/(@qj3)))
					@M3[1][0] = Complex(((@i * (Math.sin(@beta3)))*(@qj3)))
					@M3[1][1] = Complex((Math.cos(@beta3)))

					@M4 = [[],[]]
					@M4[0][0] = Complex((Math.cos(@beta4)))
					@M4[0][1] = Complex(((@i * (Math.sin(@beta4)))/(@qj4)))
					@M4[1][0] = Complex(((@i * (Math.sin(@beta4)))*(@qj4)))
					@M4[1][1] = Complex((Math.cos(@beta4)))

					@M5 = [[],[]]
					@M5[0][0] = Complex((Math.cos(@beta5)))
					@M5[0][1] = Complex(((@i * (Math.sin(@beta5)))/(@qj5)))
					@M5[1][0] = Complex(((@i * (Math.sin(@beta5)))*(@qj5)))
					@M5[1][1] = Complex((Math.cos(@beta5)))

					@M6 = [[],[]]
					@M6[0][0] = Complex((Math.cos(@beta6)))
					@M6[0][1] = Complex(((@i * (Math.sin(@beta6)))/(@qj6)))
					@M6[1][0] = Complex(((@i * (Math.sin(@beta6)))*(@qj6)))
					@M6[1][1] = Complex((Math.cos(@beta6)))

					@M7 = [[],[]]
					@M7[0][0] = Complex((Math.cos(@beta7)))
					@M7[0][1] = Complex(((@i * (Math.sin(@beta7)))/(@qj7)))
					@M7[1][0] = Complex(((@i * (Math.sin(@beta7)))*(@qj7)))
					@M7[1][1] = Complex((Math.cos(@beta7)))

					@M8 = [[],[]]
					@M8[0][0] = Complex((Math.cos(@beta8)))
					@M8[0][1] = Complex(((@i * (Math.sin(@beta8)))/(@qj8)))
					@M8[1][0] = Complex(((@i * (Math.sin(@beta8)))*(@qj8)))
					@M8[1][1] = Complex((Math.cos(@beta8)))

					@M9 = [[],[]]
					@M9[0][0] = Complex(((@M2[0][0] * @M3[0][0]) + (@M2[0][1] * @M3[1][0])))
					@M9[0][1] = Complex(((@M2[0][0] * @M3[0][1]) + (@M2[0][1] * @M3[1][1])))
					@M9[1][0] = Complex(((@M2[1][0] * @M3[0][0]) + (@M2[1][1] * @M3[1][0])))
					@M9[1][1] = Complex(((@M2[1][0] * @M3[0][1]) + (@M2[1][1] * @M3[1][1])))

					@M10 = [[],[]]
					@M10[0][0] = Complex(((@M9[0][0] * @M4[0][0]) + (@M9[0][1] * @M4[1][0])))
					@M10[0][1] = Complex(((@M9[0][0] * @M4[0][1]) + (@M9[0][1] * @M4[1][1])))
					@M10[1][0] = Complex(((@M9[1][0] * @M4[0][0]) + (@M9[1][1] * @M4[1][0])))
					@M10[1][1] = Complex(((@M9[1][0] * @M4[0][1]) + (@M9[1][1] * @M4[1][1])))

					@M11 = [[],[]]
					@M11[0][0] = Complex(((@M10[0][0] * @M5[0][0]) + (@M10[0][1] * @M5[1][0])))
					@M11[0][1] = Complex(((@M10[0][0] * @M5[0][1]) + (@M10[0][1] * @M5[1][1])))
					@M11[1][0] = Complex(((@M10[1][0] * @M5[0][0]) + (@M10[1][1] * @M5[1][0])))
					@M11[1][1] = Complex(((@M10[1][0] * @M5[0][1]) + (@M10[1][1] * @M5[1][1])))

					@M12 = [[],[]]
					@M12[0][0] = Complex(((@M11[0][0] * @M6[0][0]) + (@M11[0][1] * @M6[1][0])))
					@M12[0][1] = Complex(((@M11[0][0] * @M6[0][1]) + (@M11[0][1] * @M6[1][1])))
					@M12[1][0] = Complex(((@M11[1][0] * @M6[0][0]) + (@M11[1][1] * @M6[1][0])))
					@M12[1][1] = Complex(((@M11[1][0] * @M6[0][1]) + (@M11[1][1] * @M6[1][1])))

					@M13 = [[],[]]
					@M13[0][0] = Complex(((@M12[0][0] * @M7[0][0]) + (@M12[0][1] * @M7[1][0])))
					@M13[0][1] = Complex(((@M12[0][0] * @M7[0][1]) + (@M12[0][1] * @M7[1][1])))
					@M13[1][0] = Complex(((@M12[1][0] * @M7[0][0]) + (@M12[1][1] * @M7[1][0])))
					@M13[1][1] = Complex(((@M12[1][0] * @M7[0][1]) + (@M12[1][1] * @M7[1][1])))

					@M14 = [[],[]]
					@M14[0][0] = Complex(((@M13[0][0] * @M8[0][0]) + (@M13[0][1] * @M8[1][0])))
					@M14[0][1] = Complex(((@M13[0][0] * @M8[0][1]) + (@M13[0][1] * @M8[1][1])))
					@M14[1][0] = Complex(((@M13[1][0] * @M8[0][0]) + (@M13[1][1] * @M8[1][0])))
					@M14[1][1] = Complex(((@M13[1][0] * @M8[0][1]) + (@M13[1][1] * @M8[1][1])))

					#Primeiro calculo da soma de matriz do coeficiente de fresnel para a espessura3_Custom1
					#Que é o qj9_Custom1
					@r_n = ((((@M14[0][1] * @qj9_Custom1) + @M14[0][0]) * @qj1) - ((@M14[1][1] * @qj9_Custom1) + (@M14[1][0])))
					@r_d = ((((@M14[0][1] * @qj9_Custom1) + @M14[0][0]) * @qj1) + ((@M14[1][1] * @qj9_Custom1) + (@M14[1][0])))
					#Segundo calculo da soma de matriz do coeficiente de fresnel para a espessura3_Custom2
					#Que é o qj9_Custom2
					@r_n_custom2 = ((((@M14[0][1] * @qj9_Custom2) + @M14[0][0]) * @qj1) - ((@M14[1][1] * @qj9_Custom2) + (@M14[1][0])))
					@r_d_custom2 = ((((@M14[0][1] * @qj9_Custom2) + @M14[0][0]) * @qj1) + ((@M14[1][1] * @qj9_Custom2) + (@M14[1][0])))
					#Terceiro calculo da soma de matriz do coeficiente de fresnel para a espessura3_Custom3
					#Que é o qj9_Custom3
					@r_n_custom3 = ((((@M14[0][1] * @qj9_Custom3) + @M14[0][0]) * @qj1) - ((@M14[1][1] * @qj9_Custom3) + (@M14[1][0])))
					@r_d_custom3 = ((((@M14[0][1] * @qj9_Custom3) + @M14[0][0]) * @qj1) + ((@M14[1][1] * @qj9_Custom3) + (@M14[1][0])))

					#Divisão do resultado @r_n/@r_d
					@r = Complex(((@r_n)/(@r_d)))

					#puts @r

					#Divisão do resultado @r_n_custom2/@r_d_custom2
					@r_Custom2 = Complex(((@r_n_custom2)/(@r_d_custom2)))

					#puts @r_Custom2
					#Divisão do resultado @r_n_custom3/@r_d_custom3
					@r_Custom3 = Complex(((@r_n_custom3)/(@r_d_custom3)))

					#puts @r_Custom3

					#valor de r da espessura3_custom1
					@valor_r = Complex(((@r * (@r.conjugate))).real)

					#valor de r da espessura3_custom2
					@valor_r_custom2 = Complex(((@r_Custom2 * (@r_Custom2.conjugate))).real)

					#valor de r da espessura3_custom3
					@valor_r_custom3 = Complex(((@r_Custom3 * (@r_Custom3.conjugate))).real)

					@valor << { :id => 'io', :x => @angulo_entrada, :y => @valor_r}

					@valor_Custom2 << { :id => 'io', :x => @angulo_entrada, :y => @valor_r_custom2}

					@valor_Custom3 << { :id => 'io', :x => @angulo_entrada, :y => @valor_r_custom3}

					@angulo_entrada += @angulo_soma
					
					@assimetria <<  {"r" => @valor_r, "angulo" => @angulo_entrada}        
				   	@assimetriaCustom2 << {"r" => @valor_r_custom2, "angulo" => @angulo_entrada}
				   	@assimetriaCustom3 << {"r" => @valor_r_custom3, "angulo" => @angulo_entrada}
				   
				end

					#Angulo Minimo
				  @angulo = @assimetria.min_by { |x| x["r"] }
				  	@anguloCustom2 = @assimetriaCustom2.min_by {|angCustom2| angCustom2["r"] }
						@anguloCustom3 = @assimetriaCustom3.min_by {|angCustom3| angCustom3["r"] }
					   if @angulo != nil || @anguloCustmo2 != nil || @anguloCustom3 != nil then 
					   	
					   	@angulo_min = @angulo["angulo"]
					   	@angleCustom2 = @anguloCustom2["angulo"]
				   	    @angleCustom3 = @anguloCustom3["angulo"]
				   	   
				   	    end
				   	    /
				   	    	Aqui começa o inicio do sensograma com o tempo inicial
				   	    		ele faz uma verificação se os tempos eles são diferentes de 0.0, se o tempo
				   	    		for diferente de 0 ele começa a incrementação da primeira substancia
				   	    		com a variavel @EspessuraCustom1 e RefracaoCustom1. Começa com o tempo de inicio e vai 
				   	    		até o tempo_InjecaoAux
				   	    /
						   	if @tempo_InicioAux != 0.0 && @tempo_PassoAux != 0.0 && @tempo_InjecaoAux != 0.0 then
									while @tempo_InicioAux <= @tempo_InjecaoAux do
												@valorSensograma << {:id => 'io', :x => @tempo_InicioAux, :y => @angulo_min}	
										@tempo_InicioAux += @tempo_PassoAux
										#puts @valorSensograma
									end

									
									/
									Essas duas variaveis são para receber os dois angulos minimos que tem,
									O primeiro angulo minimo é o resultado da @EspessuraCustom1 e @RefracaoCustom1
									
									O Segundo Angulo Minimo denominado @anguloAuxMenor ele recebe os valores
									da @EspessuraCustom2 e @RefracaoCustom2
									/
										@anguloAuxMaior = @angulo["angulo"]
										@anguloAuxMenor = @anguloCustom2["angulo"]
									#Essa variavel @tempo_Injecao2 recebe o valor do Tempo_InjecaoAux
									@tempo_Injecao2 = @tempo_InjecaoAux
									#Essa variavel @tempoTotal2 recebe o Tempo total menos o tempo de injeção 
									#3 da terceira substancia
									@tempoTotal2 = @tempo_TotalAux - @tempo_InjecaoAux3	
										#puts @tempoTotal2
								
								    /
									 Aqui começa uma decrementação do angulo ou incrementação de acordo com
									 angulo minimo que é o resultado das variaveis @anguloAuxMaior e @anguloAuxMenor
									 Faz a verificação do Angulo se ele é maior ou menor 	
									/	
									@auxInjecao2 = @tempo_Injecao2
								 if @anguloAuxMaior >= @anguloAuxMenor then
									while @tempo_Injecao2 <= @auxInjecao2 do	
										while @anguloAuxMaior >= @anguloAuxMenor do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao2, :y => @anguloAuxMaior}
											#puts @valorSensograma
											 @anguloAuxMaior -= 0.1
											 @auxInjecao2 = @tempo_Injecao2
											@tempo_Injecao2 += @tempo_PassoAux
										end
									end
								elsif 
									while @tempo_Injecao2 <= @auxInjecao2 do	
										while @anguloAuxMaior <= @anguloAuxMenor do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao2, :y => @anguloAuxMaior}
											#puts @valorSensograma
											 @anguloAuxMaior += 0.1
											 @auxInjecao2 = @tempo_Injecao2
											@tempo_Injecao2 += @tempo_PassoAux
										end
									end
								 end
										
									/Faz um while do tempo aonde ele parou e vai até o tempoTotal2 = @tempo_TotalAux - @tempo_InjecaoAux3
										fazendo a inserção no sensograma.
									/
									while @tempo_Injecao2 <= @tempoTotal2 do
												@valorSensograma << {:id => 'io', :x => @tempo_Injecao2, :y => @angleCustom2}	
										@tempo_Injecao2 += @tempo_PassoAux
										#puts @valorSensograma
									end


									/Essa variavel @tempo_Injecao3 ela recebe o tempo @tempo_Injecao2 aonde parou
									/
									@tempo_Injecao3 = @tempo_Injecao2



									/Essas duas variaveis ela recebe os dois angulos minimos/
									@anguloAuxMaior2 = @anguloCustom2["angulo"]
									@anguloAuxMenor2 = @anguloCustom3["angulo"]
									/Fazendo a verficação para saber se é maior ou menor
									A partir dai, faz uma incrementação do angulo ou decrementação
									/
									@auxInjecao3 = @tempo_Injecao3
								 if @anguloAuxMaior2 >= @anguloAuxMenor2 then
									while @tempo_Injecao3 <= @auxInjecao3 do	
										while @anguloAuxMaior2 >= @anguloAuxMenor2 do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao3, :y => @anguloAuxMaior2}
											#puts @valorSensograma
											 @anguloAuxMaior2 -= 0.1
											 @auxInjecao3 = @tempo_Injecao3
											@tempo_Injecao3 += @tempo_PassoAux
										end
									end
								elsif 
									while @tempo_Injecao3 <= @auxInjecao3 do	
										while @anguloAuxMaior2 <= @anguloAuxMenor2 do
											@valorSensograma << {:id => 'io', :x => @tempo_Injecao3, :y => @anguloAuxMaior2}
											#puts @valorSensograma
											 @anguloAuxMaior2 += 0.1
											 @auxInjecao3 = @tempo_Injecao3
											@tempo_Injecao3 += @tempo_PassoAux
										end
									end
								 end

									#puts @tempo_Injecao3
									/Essa parte faz a inserção no sensograma até o tempo Total/
									while @tempo_Injecao3 <= @tempo_TotalAux do
												@valorSensograma << {:id => 'io', :x => @tempo_Injecao3, :y => @angleCustom3}	
											@tempo_Injecao3 += @tempo_PassoAux

										#puts @valorSensograma
									end
		                     end

		                     
					   

					
					 @teta_max = @assimetria.max_by { |z| z["r"] }
					 @teta_min = @assimetria.min_by { |y| y["r"] }
					 @teta_maxCustom2 = @assimetriaCustom2.max_by { |custom2max| custom2max["r"] }
					 @teta_minCustom2 = @assimetriaCustom2.min_by { |custom2min| custom2min["r"] }
					 @teta_maxCustom3 = @assimetriaCustom3.max_by { |custom3max| custom3max["r"] }
					 @teta_minCustom3 = @assimetriaCustom3.min_by { |custom3min| custom3min["r"] }
					  if (@teta_max != nil && @teta_min != nil) || (@teta_maxCustom2 != nil && @teta_minCustom2 != nil) || (@teta_maxCustom3 != nil && @teta_minCustom3 != nil) then 
						@teta_m1 = @teta_max["r"]
						@teta_m2 = @teta_min["r"]

						@tetaCustom2M1 = @teta_maxCustom2["r"]
						@tetaCustom2M2 = @teta_minCustom2["r"]

						@tetaCustom3M1 = @teta_maxCustom3["r"]
						@tetaCustom3M2 = @teta_minCustom3["r"]

						@teta_medio = ((@teta_m1 + @teta_m2)/2).round(2)
						@teta_medioCustom2 = ((@tetaCustom2M1 + @tetaCustom2M2)/2).round(2)
						@teta_medioCustom3 = ((@tetaCustom3M1 + @tetaCustom3M2)/2).round(2)
						
						@teta_aux_v1 = (@teta_min["angulo"]).round(2)
						@teta_aux_v1Custom2 = (@teta_minCustom2["angulo"]).round(2)
						@teta_aux_v1Custom3 = (@teta_minCustom3["angulo"]).round(2)

						/Variaveis começando em 0 para pegar os valores da curva
						como angulo minimo, assimetria e largura da curva/
						@v1 = 0
						@v2 = 0
						@v1Custom2 = 0
						@v2Custom2 = 0
						@v1Custom3 = 0
						@v2Custom3 = 0
						@aux = 0
						@auxCustom2 = 0
						@auxCustom3 = 0
						@vetor = []
						@vetorCustom2 = []
						@vetorCustom3 = []
						@vetor1 = []
						@vetor1Custom2 = []
						@vetor1Custom3 = []

						#For do Hash aonde tem o meus valores de R e o angulo
						#A partir dai vou pegar o meu @valor medio e procurar o 
						# minino do meus angulos
						@assimetria.each do |a|
						  if a["r"] <= @teta_medio then
							if @aux == 0 then
								@v1 = a["angulo"]
							    @aux += 1
							 else
								@v2 = a["angulo"]
							end
						   end
						end

						#For do Hash aonde tem o meus valores de R e o angulo
						#A partir dai vou pegar o meu @valor medio e procurar o 
						# minino do meus angulos dos valores da assimetriaCustom2
						@assimetriaCustom2.each do |custom2|
							if custom2["r"] <= @teta_medioCustom2 then
								if @auxCustom2 == 0 then
									@v1Custom2 = custom2["angulo"]
									@auxCustom2 += 1
								else
									@v2Custom2 = custom2["angulo"]
								 end
							end
						end

						#For do Hash aonde tem o meus valores de R e o angulo
						#A partir dai vou pegar o meu @valor medio e procurar o 
						# minino do meus angulos dos valores da assimetriaCustom3
						@assimetriaCustom3.each do |custom3|
							if custom3["r"] <= @teta_medioCustom3 then
								if @auxCustom3 == 0 then
									@v1Custom3 = custom3["angulo"]
									@auxCustom3 += 1
								else
									@v2Custom3 = custom3["angulo"]
								 end
							end
						end

						#@assimetria.each do |valorR|
						    

						#end

						# if @tempo_InicioAux != nil then
					 	  #while @tempo_InicioAux <= @tempo_InjecaoAux do
					   	  #	@valorSensograma << {"r" => @valor_r, "tempo" => @tempo_InjecaoAux}
					   	  #	@tempo_InjecaoAux += @tempo_PassoAux
					   	 # end
					# end

						#puts "O valor do V1Custom3 = #{@v1Custom3}"
						#puts @v2Custom3
						/Calculo da assimetria e largura
						a variavel @vetor ele está fazendo o calculo da largura
						e @vetor1 está fazendo o calculo da assimetria/
						@vetor = ((@v1 - @v2).abs).round(4)
						@vetor1 = ((@v1/@v2).abs).round(4)

						@vetorCustom2 = ((@v1Custom2 - @v2Custom2).abs).round(4)
						@vetor1Custom2 = ((@v1Custom2/@v2Custom2).abs).round(4)

						@vetorCustom3 = ((@v1Custom3 - @v2Custom3).abs).round(4)
						@vetor1Custom3 = ((@v1Custom3/@v2Custom3).abs).round(4)

						#puts @vetor1Custom3  
					
						@valor_aux = Complex((@n1) * (Math.sin(@angulo_th)))
						@valor_aux1 = Complex((@n1) * (Math.sin(@angulo_th)))
						@aux = @valor_aux * @valor_aux1
						@indice_aux = (@refracao2 * @refracao2).real
						#puts @aux   

						@n3 = Math.sqrt(((@indice_aux*@aux)/(@indice_aux-@aux))).round(4)
						#puts @n3 
					   end
					 
					result = Hash.new
					#result["chartAIM"] = @valor
					result["IRCustom1"] = @refracao9_Custom1
					result["IRCustom2"] = @refracao9_Custom2
					result["IRCustom3"] = @refracao9_Custom3
					result["chartSensograma"] = @valorSensograma
					result["valor_medio"] = @teta_medio
					result["valor_medioCustom2"] = @teta_medioCustom2
					result["valor_medioCustom3"] = @teta_medioCustom3
					result["angulo_minimo"] = "#{@teta_aux_v1} º"
					result["angulo_minimoCustom2"] = "#{@teta_aux_v1Custom2} º"
					result["angulo_minimoCustom3"] = "#{@teta_aux_v1Custom3} º"
					result["largura_certo"] = "#{@vetor} º"
					result["largura_certoCustom2"] = "#{@vetorCustom2} º"
					result["largura_certoCustom3"] = "#{@vetorCustom2} º"
					result["assimetria_certo"] = @vetor1
					result["assimetria_certoCustom2"] = @vetor1Custom2
					result["assimetria_certoCustom3"] = @vetor1Custom3
					result["indiceRefracaoCerto"] = @n3

					respond_to do |format|
					 # format.html { grafico }
					  format.html { graficoSensograma }
					  format.js { render :json => result }
					end
		 
	end
	

private

	def calculoRefracao(elemento, onda)
	    case elemento 
		when 'BK7'
		   # realizando calculos do BK7
		   #Inicio do Indice de Refração BK7
		   @BK7_aux1 = 1.03961212
		   @BK7_aux2 = 0.00600069867
		   @BK7_aux3 = 0.231792344
		   @BK7_aux4 = 0.0200179144
		   @BK7_aux5 = 1.01046945
		   @BK7_aux6 = 103.560653
		   @onda1 = onda  * 0.001

		   @BK7 = Math.sqrt(1 + @BK7_aux1/(1-@BK7_aux2/(@onda1**2)) + @BK7_aux3/(1-@BK7_aux4/(@onda1**2)) + @BK7_aux5/(1-@BK7_aux6/(@onda1**2)))
		   return @BK7

		   when 'Quartz'
			@B1_q = 0.6961663
			@B2_q = 0.4079426
			@B3_q = 0.8974794
			@C1_q = 0.0684043**2
			@C2_q = 0.1162414**2
			@C3_q = 9.896161**2
			@onda1 = onda
			@n1_quartz = Math.sqrt(1 + ((@B1_q*((@onda1*0.001)**2))/(((@onda1*0.001)**2) - @C1_q)) + ((@B2_q*((@onda1*0.001)**2))/(((@onda1*0.001)**2) - @C2_q)) + ((@B3_q*((@onda1*0.001)**2))/(((@onda1*0.001)**2) - @C3_q)))
		 	#puts @n1_quartz
		 return @n1_quartz

		when 'Safira'

			@A_n_sph = 0.00000000000000000000000000000000032293841270623
			@B_n_sph = -0.00000000000000000000000000000906691731572933
			@C_n_sph = 0.000000000000000000000000109756183034243
			@D_n_sph = -0.000000000000000000000749954856294772
			@E_n_sph = 0.0000000000000000031811886927908
			@F_n_sph = -0.00000000000000868266767821678
			@G_n_sph = 0.0000000000153015736009313
			@H_n_sph = -0.0000000170286454568001
			@I_n_sph = 0.0000113591109116551
			@J_n_sph = -0.0041077194844072
			@K_n_sph = 2.38263952164331
			@onda1 = onda
			@n1_sph = @A_n_sph*@onda1**10 + @B_n_sph*@onda1**9 + @C_n_sph*@onda1**8 + @D_n_sph*@onda1**7+ @E_n_sph*@onda1**6 + @F_n_sph*@onda1**5 + @G_n_sph*@onda1**4 + @H_n_sph*@onda1**3 + @I_n_sph*@onda1**2 + @J_n_sph*@onda1 + @K_n_sph
			#puts @n1_sph
		 return @n1_sph
		
		 
		   
		   when 'Au'
		   	 @auReal_aux = onda
			 @spliner_ouro = Spliner::Spliner.new [187.9,191.6,195.3,199.3,203.3,207.3,211.9,216.4,221.4,226.2,231.3,237.1,242.6,249,255.1,261.6,268.9,276.1,284.4,292.4,300.9,310.7,320.4,331.5,342.5,354.2,367.9,381.5,397.4,413.3,430.5,450.9,471.4,495.9,520.9,548.6,582.1,616.8,659.5,704.5,756,821.1,892,984,1088,1216,1393,1610,1937], [1.28,1.32,1.34,1.33,1.33,1.3,1.3,1.3,1.3,1.31,1.3,1.32,1.32,1.33,1.33,1.35,1.38,1.43,1.47,1.49,1.53,1.53,1.54,1.48,1.48,1.5,1.48,1.46,1.47,1.46,1.45,1.38,1.31,1.04,0.62,0.43,0.29,0.21,0.14,0.13,0.14,0.16,0.17,0.22,0.27,0.35,0.43,0.56,0.92]
			 @AuReal = @spliner_ouro[@auReal_aux].to_c
			 #puts @AuReal

			 @auImg_aux = onda
			 @spliner_ouro = Spliner::Spliner.new [187.9,191.6,195.3,199.3,203.3,207.3,211.9,216.4,221.4,226.2,231.3,237.1,242.6,249,255.1,261.6,268.9,276.1,284.4,292.4,300.9,310.7,320.4,331.5,342.5,354.2,367.9,381.5,397.4,413.3,430.5,450.9,471.4,495.9,520.9,548.6,582.1,616.8,659.5,704.5,756,821.1,892,984,1088,1216,1393,1610,1937], [1.188,1.203,1.226,1.251,1.277,1.304,1.35,1.387,1.427,1.46,1.497,1.536,1.577,1.631,1.688,1.749,1.803,1.847,1.869,1.878,1.889,1.893,1.898,1.883,1.871,1.866,1.895,1.933,1.952,1.958,1.948,1.914,1.849,1.833,2.081,2.455,2.863,3.272,3.697,4.103,4.542,5.083,5.663,6.35,7.15,8.145,9.519,11.21,13.78]
			 @AuImg = @spliner_ouro[@auImg_aux].to_c
			 @AU = Complex(@AuReal, @AuImg)
		   	return @AU

		   when 'Ag'

		   	@agReal_aux = onda
		   	@agImg_aux = onda

		   	if @agReal_aux <= 340 && @agImg_aux <= 340 then

		   		@spliner_prata = Spliner::Spliner.new [187.88,189.31,190.77,192.25,193.75,195.28,196.83,198.4,200,201.63,203.28,204.96,206.67,208.4,210.17,211.97,213.79,215.65,217.54,219.47,221.43,223.42,225.45,227.52,229.63,231.78,233.96,236.19,238.46,240.78,243.14,245.54,248,250.51,253.06,255.67,258.33,261.05,263.83,266.67,269.57,272.53,275.56,278.65,281.82,285.06,288.37,291.76,295.24,298.8,302.44,306.17,310,313.92,317.95,322.08,326.32,330.67,335.14,339.73],[0.995,1.0043,1.012,1.0195,1.028,1.0375,1.048,1.0596,1.072,1.0848,1.098,1.1116,1.125,1.1372,1.149,1.1614,1.173,1.1819,1.19,1.1991,1.208,1.2157,1.223,1.2307,1.238,1.244,1.25,1.2572,1.265,1.2734,1.282,1.2897,1.298,1.3086,1.32,1.3311,1.343,1.3569,1.372,1.3875,1.404,1.4223,1.441,1.4592,1.476,1.4901,1.502,1.5136,1.519,1.5194,1.496,1.4325,1.323,1.1421,0.932,0.7191,0.526,0.3881,0.294,0.2533]
		   		@AgReal = @spliner_prata[@agReal_aux].to_c

		   		@spliner_prata_img = Spliner::Spliner.new [187.88,189.31,190.77,192.25,193.75,195.28,196.83,198.4,200,201.63,203.28,204.96,206.67,208.4,210.17,211.97,213.79,215.65,217.54,219.47,221.43,223.42,225.45,227.52,229.63,231.78,233.96,236.19,238.46,240.78,243.14,245.54,248,250.51,253.06,255.67,258.33,261.05,263.83,266.67,269.57,272.53,275.56,278.65,281.82,285.06,288.37,291.76,295.24,298.8,302.44,306.17,310,313.92,317.95,322.08,326.32,330.67,335.14,339.73], [1.13,1.1494,1.16,1.1681,1.18,1.1944,1.21,1.2256,1.24,1.2513,1.26,1.2656,1.27,1.275,1.28,1.2853,1.29,1.2928,1.295,1.2975,1.3,1.3025,1.305,1.3072,1.31,1.3147,1.32,1.325,1.33,1.335,1.34,1.3456,1.35,1.3506,1.35,1.35,1.35,1.3513,1.35,1.3413,1.33,1.3219,1.31,1.2881,1.26,1.2288,1.19,1.143,1.08,0.9888,0.882,0.7611,0.647,0.5509,0.504,0.5544,0.663,0.8186,0.986,1.1207]
		   		@AgImg = @spliner_prata_img[@agImg_aux].to_c
		   	 
		   	 elsif @agReal_aux >= 341 && @agImg_aux >= 341 then
		   	
			   	 @spliner_prata = Spliner::Spliner.new [344.44,349.3,354.29,359.42,364.71,370.15,375.76,381.54,387.5,393.65,400,406.56,413.33,420.34,427.59,435.09,442.86,450.91,459.26,467.92,476.92,486.27,496,506.12,516.67,527.66,539.13,551.11,563.64,576.74,590.48,604.88,620,635.9,652.63,670.27,688.89,708.57,729.41,751.52,775,800,826.67,855.17,885.71,918.52,953.85,992,1033.33,1078.26,1127.27,1180.95,1240,1305.26,1377.78,1458.82,1550,1653.33,1771.43,1907.69,2066.67], [0.238,0.2214,0.209,0.1948,0.186,0.1921,0.2,0.1981,0.192,0.182,0.173,0.1726,0.173,0.1667,0.16,0.1585,0.157,0.1511,0.144,0.1373,0.132,0.1303,0.13,0.1299,0.13,0.1301,0.129,0.1244,0.12,0.1193,0.121,0.1255,0.131,0.1361,0.14,0.1401,0.14,0.1443,0.148,0.1459,0.143,0.1426,0.145,0.1519,0.163,0.1799,0.198,0.2126,0.226,0.2354,0.251,0.2874,0.329,0.36,0.396,0.4386,0.514,0.6726,0.844,0.9879,1.064]
				 @AgReal = @spliner_prata[@agReal_aux].to_c

				 @spliner_prata_img = Spliner::Spliner.new [344.44,349.3,354.29,359.42,364.71,370.15,375.76,381.54,387.5,393.65,400,406.56,413.33,420.34,427.59,435.09,442.86,450.91,459.26,467.92,476.92,486.27,496,506.12,516.67,527.66,539.13,551.11,563.64,576.74,590.48,604.88,620,635.9,652.63,670.27,688.89,708.57,729.41,751.52,775,800,826.67,855.17,885.71,918.52,953.85,992,1033.33,1078.26,1127.27,1180.95,1240,1305.26,1377.78,1458.82,1550,1653.33,1771.43,1907.69,2066.67],[1.24,1.3453,1.44,1.5338,1.61,1.6419,1.67,1.735,1.81,1.8788,1.95,2.0294,2.11,2.1863,2.26,2.3294,2.4,2.4788,2.56,2.64,2.72,2.7981,2.88,2.9738,3.07,3.1594,3.25,3.3481,3.45,3.5538,3.66,3.7663,3.88,4.0106,4.15,4.2931,4.44,4.5863,4.74,4.9081,5.09,5.2888,5.5,5.7206,5.95,6.1831,6.43,6.6975,6.99,7.3138,7.67,8.0606,8.49,8.9538,9.48,10.1144,10.8,11.445,12.2,13.155,14.4]
				 @AgImg = @spliner_prata_img[@agImg_aux].to_c
		   	
		   	end
			 
			 @AG = Complex(@AgReal, @AgImg)
			 return @AG

			when 'Cu'
			 @cuReal_aux = onda
			 @spliner_cobre = Spliner::Spliner.new [187.9,191.6,195.3,199.3,203.3,207.3,211.9,216.4,221.4,226.2,231.3,237.1,242.6,249,255.1,261.6,268.9,276.1,284.4,292.4,300.9,310.7,320.4,331.5,342.5,354.2,367.9,381.5,397.4,413.3,430.5,450.9,471.4,495.9,520.9,548.6,582.1,616.8,659.5,704.5,756,821.1,892,984,1088,1216,1393,1610,1937], [0.94,0.95,0.97,0.98,0.99,1.01,1.04,1.08,1.13,1.18,1.23,1.28,1.34,1.37,1.41,1.41,1.45,1.46,1.45,1.42,1.4,1.38,1.38,1.34,1.36,1.37,1.36,1.33,1.32,1.28,1.25,1.24,1.25,1.22,1.18,1.02,0.7,0.3,0.22,0.21,0.24,0.26,0.3,0.32,0.36,0.48,0.6,0.76,1.09]
			 @CuReal = @spliner_cobre[@cuReal_aux].to_c

			 @cuImg_aux = onda
			 @spliner_cobre = Spliner::Spliner.new [187.9,191.6,195.3,199.3,203.3,207.3,211.9,216.4,221.4,226.2,231.3,237.1,242.6,249,255.1,261.6,268.9,276.1,284.4,292.4,300.9,310.7,320.4,331.5,342.5,354.2,367.9,381.5,397.4,413.3,430.5,450.9,471.4,495.9,520.9,548.6,582.1,616.8,659.5,704.5,756,821.1,892,984,1088,1216,1393,1610,1937], [1.337,1.388,1.44,1.493,1.55,1.599,1.651,1.699,1.737,1.768,1.792,1.802,1.799,1.783,1.741,1.691,1.668,1.646,1.633,1.633,1.679,1.729,1.783,1.821,1.864,1.916,1.975,2.045,2.116,2.207,2.305,2.397,2.483,2.564,2.608,2.577,2.704,3.205,3.747,4.205,4.665,5.18,5.768,6.421,7.217,8.245,9.439,11.12,13.43]
			 @CuImg = @spliner_cobre[@cuImg_aux].to_c
			 @CU = Complex(@CuReal, @CuImg)
			 return @CU
			 
			when 'Al'
			 	
			 @alReal_aux = onda
			 @alImg_aux = onda
			 if (@alReal_aux >= 1 && @alImg_aux >= 1) && (@alReal_aux <= 515 && @alImg_aux <= 515) then
			 	
			 	#Primeiro Spline
			 	@spliner_aluminio = Spliner::Spliner.new [150,155,160,165,170,175,180,185,190,195,200,205,210,215,220,225,230,235,240,245,250,255,260,265,270,275,280,285,290,295,300,305,310,315,320,325,330,335,340,345,350,355,360,365,370,375,380,385,390,395,400,405,410,415,420,425,430,435,440,445,450,455,460,465,470,475,480,485,490,495,500,505,510,515],[0.095390828,0.095510386,0.09903925,0.098692838,0.100850207,0.1069563,0.099715746,0.108316112,0.106567769,0.111513266,0.110803374,0.111587326,0.11365555,0.115928445,0.116173424,0.119771906,0.124315543,0.129276519,0.133002743,0.139688588,0.141162655,0.148765766,0.150722638,0.161465056,0.164610587,0.172365826,0.178635303,0.18587018,0.188953314,0.197315218,0.204991638,0.210097266,0.218816718,0.224454799,0.237666454,0.24464928,0.251892832,0.259391824,0.267481852,0.275201252,0.28349792,0.291774119,0.300125667,0.308578012,0.317597538,0.32692637,0.335956002,0.345714203,0.354901676,0.364968364,0.375150842,0.385211589,0.396086448,0.40706511,0.417647849,0.429543735,0.440996226,0.452837879,0.464232752,0.477070026,0.489220122,0.501228231,0.514817248,0.528042125,0.53987657,0.554932886,0.568005038,0.582771419,0.596705366,0.610784373,0.625686295,0.640306464,0.655709839,0.672565753]
			 	@AlReal = @spliner_aluminio[@alReal_aux].to_c

			 	@spliner_aluminio_img = Spliner::Spliner.new [150,155,160,165,170,175,180,185,190,195,200,205,210,215,220,225,230,235,240,245,250,255,260,265,270,275,280,285,290,295,300,305,310,315,320,325,330,335,340,345,350,355,360,365,370,375,380,385,390,395,400,405,410,415,420,425,430,435,440,445,450,455,460,465,470,475,480,485,490,495,500,505,510,515],[1.283666394,1.337393822,1.402928641,1.46616516,1.532569987,1.596539899,1.657661977,1.734368006,1.79116071,1.853411775,1.908606137,1.969936987,2.028057589,2.091850713,2.151998203,2.213039835,2.274896559,2.336773934,2.395514502,2.457358928,2.515219055,2.573777623,2.633214255,2.693431077,2.750035753,2.81099804,2.868973784,2.923276881,2.98248275,3.042340102,3.100858199,3.157347124,3.214790935,3.267473023,3.323775766,3.37932734,3.436779015,3.493749292,3.550147397,3.607178361,3.663518946,3.719829584,3.776251503,3.831608068,3.889198914,3.945995546,4.002607455,4.058265431,4.114238107,4.169676475,4.226433266,4.281283449,4.336805792,4.391435073,4.447407079,4.503499508,4.559109307,4.613682059,4.669101846,4.724044433,4.778319404,4.832828338,4.887948117,4.943260916,4.997345967,5.051331558,5.105940631,5.159226931,5.212874187,5.265933683,5.320477736,5.374848125,5.428163169,5.481545831]
			 	@AlImg = @spliner_aluminio_img[@alImg_aux].to_c

			 	elsif (@alReal_aux >= 516 && @alImg_aux >= 516) && (@alReal_aux <= 885 && @alImg_aux <= 885) then
			 		 #Segundo Spline
			 		@spliner_aluminio = Spliner::Spliner.new [520,525,530,535,540,545,550,555,560,565,570,575,580,585,590,595,600,605,610,615,620,625,630,635,640,645,650,655,660,665,670,675,680,685,690,695,700,705,710,715,720,725,730,735,740,745,750,755,760,765,770,775,780,785,790,795,800,805,810,815,820,825,830,835,840,845,850,855,860,865,870,875,880,885],[0.688336416,0.704045108,0.720793584,0.737603948,0.75446839,0.772330366,0.789405353,0.808351698,0.829205097,0.848630853,0.867376853,0.887661988,0.908569739,0.928308533,0.948955518,0.9714896,0.992465612,1.016073317,1.038145667,1.062059906,1.088160063,1.112663572,1.136328574,1.165731637,1.190265203,1.218505245,1.246364405,1.275302761,1.304382818,1.333854457,1.365410391,1.395892303,1.426024482,1.457391234,1.493230683,1.5274935,1.559751729,1.596346402,1.631621525,1.669698976,1.706780893,1.745260386,1.786398844,1.827772224,1.872189153,1.916802427,1.958355454,2.005349601,2.054224115,2.09984451,2.144528834,2.189915668,2.231500036,2.275479945,2.314643646,2.350258044,2.373653298,2.399438967,2.409296598,2.414395973,2.410049326,2.399462094,2.375277499,2.342102357,2.301774531,2.257656113,2.204898553,2.148111401,2.092718783,2.03127715,1.973568962,1.915492386,1.854996083,1.802564727]
			 		@AlReal = @spliner_aluminio[@alReal_aux].to_c

			 		@spliner_aluminio_img = Spliner::Spliner.new [520,525,530,535,540,545,550,555,560,565,570,575,580,585,590,595,600,605,610,615,620,625,630,635,640,645,650,655,660,665,670,675,680,685,690,695,700,705,710,715,720,725,730,735,740,745,750,755,760,765,770,775,780,785,790,795,800,805,810,815,820,825,830,835,840,845,850,855,860,865,870,875,880,885],[5.535955836,5.58799423,5.641767754,5.693773756,5.746496546,5.799008035,5.851936501,5.905288517,5.958236408,6.009090973,6.060226283,6.112687118,6.165184423,6.215605902,6.267417058,6.317103137,6.368986418,6.417351867,6.46681888,6.517052343,6.566589093,6.61475776,6.66312559,6.710806975,6.759417967,6.807110623,6.852329839,6.898361917,6.942363757,6.987643931,7.031951759,7.071104251,7.116287362,7.157652493,7.197481356,7.23603734,7.27391404,7.311383032,7.34575992,7.380372258,7.414249862,7.446739811,7.475374294,7.502849546,7.528352557,7.55216072,7.571403838,7.585211073,7.599462923,7.605086914,7.61216158,7.610567038,7.603853787,7.589387919,7.572223927,7.547894599,7.522581337,7.486541749,7.449477169,7.410756231,7.369351244,7.325600203,7.288411569,7.253098886,7.225729444,7.204374444,7.188085811,7.177700369,7.181328004,7.184642431,7.198887028,7.218248477,7.243411521,7.274079983]
			 		@AlImg = @spliner_aluminio_img[@alImg_aux].to_c

			 	elsif (@alReal_aux >= 886 && @alImg_aux >= 886) && (@alReal_aux <= 1255 && @alImg_aux <= 1255) then
			 		#Terceito Spline
					 @spliner_aluminio = Spliner::Spliner.new [890,895,900,905,910,915,920,925,930,935,940,945,950,955,960,965,970,975,980,985,990,995,1000,1005,1010,1015,1020,1025,1030,1035,1040,1045,1050,1055,1060,1065,1070,1075,1080,1085,1090,1095,1100,1105,1110,1115,1120,1125,1130,1135,1140,1145,1150,1155,1160,1165,1170,1175,1180,1185,1190,1195,1200,1205,1210,1215,1220,1225,1230,1235,1240,1245,1250,1255],[1.750120449,1.694671727,1.64715608,1.602399525,1.557188172,1.516837858,1.483545494,1.447230511,1.415213787,1.385433286,1.355589911,1.325884954,1.302164463,1.280288795,1.25976713,1.23821822,1.218188621,1.201067407,1.182294465,1.167631515,1.153696234,1.140905235,1.126639087,1.114680776,1.102964941,1.09358083,1.08727974,1.081445553,1.071397488,1.061537336,1.055494597,1.051806876,1.043454926,1.036482441,1.033133204,1.02747793,1.025591906,1.024418445,1.020244607,1.015304156,1.014378517,1.011890304,1.009905005,1.008451539,1.005344855,1.00382405,1.004294808,1.003882164,1.003972742,1.00500573,1.002828586,1.003941179,1.005317282,1.003754384,1.008818757,1.007932489,1.008357257,1.012688334,1.015213183,1.014759181,1.017683803,1.01813905,1.021736807,1.022859293,1.024257787,1.026450838,1.029738722,1.035047848,1.038667581,1.039978895,1.043389606,1.046657092,1.049913391,1.051297547]
				 	 @AlReal = @spliner_aluminio[@alReal_aux].to_c

				 	 @spliner_aluminio_img = Spliner::Spliner.new [890,895,900,905,910,915,920,925,930,935,940,945,950,955,960,965,970,975,980,985,990,995,1000,1005,1010,1015,1020,1025,1030,1035,1040,1045,1050,1055,1060,1065,1070,1075,1080,1085,1090,1095,1100,1105,1110,1115,1120,1125,1130,1135,1140,1145,1150,1155,1160,1165,1170,1175,1180,1185,1190,1195,1200,1205,1210,1215,1220,1225,1230,1235,1240,1245,1250,1255],[7.309782595,7.350096072,7.393045594,7.439468008,7.48978204,7.542665118,7.592621895,7.647710517,7.70342976,7.756057335,7.812465936,7.872100794,7.928721983,7.985307721,8.04593147,8.1051949,8.163760378,8.22268587,8.279240316,8.338247019,8.399524689,8.457757667,8.511598888,8.571960874,8.629920722,8.687515124,8.74843856,8.809967337,8.867857271,8.920081923,8.978255334,9.036830069,9.083765639,9.148731926,9.204040646,9.264432573,9.318521275,9.373122913,9.42862127,9.479917855,9.5368739,9.593431189,9.649917519,9.704482186,9.750882949,9.806001363,9.860152537,9.917498646,9.968398129,10.02382345,10.07500199,10.12708734,10.18335224,10.23706613,10.28628773,10.34146643,10.39215013,10.44200832,10.49336256,10.5517646,10.60545522,10.65817091,10.70579031,10.75807795,10.80782605,10.85926339,10.9110202,10.96181208,11.01266681,11.06849385,11.11569439,11.16984741,11.21616325,11.26655345]
					 @AlImg = @spliner_aluminio_img[@alImg_aux].to_c

				elsif (@alReal_aux >= 1256 && @alImg_aux >= 1256) then
					 #Quarto Spline
			 		 @spliner_aluminio = Spliner::Spliner.new [1260,1265,1270,1275,1280,1285,1290,1295,1300,1305,1310,1315,1320,1325,1330,1335,1340,1345,1350,1355,1430,1435,1440,1445,1450,1455,1460,1465,1470,1475,1480,1485,1490,1495,1500,1505,1510,1515,1520,1525,1530,1535,1540,1545,1550,1555,1560,1565,1570,1575,1580,1585,1590,1595,1600,1605,1610,1615,1620,1625,1630,1635,1640,1645,1650,1655,1660,1665,1670,1675,1680,1685,1690,1695,1700],[1.056495057,1.057188404,1.061492702,1.066383574,1.070500901,1.073080696,1.078091053,1.081263285,1.08667656,1.090905752,1.094227716,1.097106966,1.104427092,1.108138114,1.109282178,1.112492808,1.123987022,1.129233973,1.132981867,1.135566133,1.20897633,1.215774284,1.222359347,1.225637477,1.232348412,1.235053048,1.23989324,1.245674136,1.25590184,1.252421713,1.265153644,1.264792932,1.275023692,1.277693673,1.285699173,1.290149697,1.296809948,1.304059962,1.313924676,1.31627889,1.325289614,1.332565366,1.338103685,1.342317392,1.347399401,1.354287918,1.36162657,1.367953829,1.374881699,1.374949427,1.385662006,1.390425967,1.400170689,1.39786136,1.400984694,1.402928097,1.421414675,1.424742484,1.436002763,1.439358574,1.44609075,1.447904247,1.45643876,1.458873135,1.469917493,1.478177828,1.474977808,1.477843053,1.462292742,1.425778483,1.427335087,1.454908413,1.514176047,1.555668449,1.584018511]
			 		 @AlReal = @spliner_aluminio[@alReal_aux].to_c							
					 		
					@spliner_aluminio_img = Spliner::Spliner.new [1260,1265,1270,1275,1280,1285,1290,1295,1300,1305,1310,1315,1320,1325,1330,1335,1340,1345,1350,1355,1430,1435,1440,1445,1450,1455,1460,1465,1470,1475,1480,1485,1490,1495,1500,1505,1510,1515,1520,1525,1530,1535,1540,1545,1550,1555,1560,1565,1570,1575,1580,1585,1590,1595,1600,1605,1610,1615,1620,1625,1630,1635,1640,1645,1650,1655,1660,1665,1670,1675,1680,1685,1690,1695,1700],[11.31619114,11.36079431,11.41081797,11.47288865,11.51937378,11.56250415,11.61775711,11.66829595,11.71967858,11.7737027,11.82020872,11.86438552,11.91930196,11.96737106,12.01542787,12.06389822,12.1162431,12.16532652,12.21775953,12.26700903,12.99198306,13.0402495,13.08984959,13.13438949,13.19350911,13.23424936,13.2825952,13.32973008,13.37524914,13.42119816,13.46516297,13.51960433,13.56523813,13.61515704,13.66027168,13.70964938,13.75760575,13.81378197,13.8577198,13.90548777,13.94978109,13.9955611,14.0463704,14.08906725,14.13278052,14.18605286,14.2300396,14.27204602,14.33214219,14.37325593,14.41873986,14.47146449,14.51965833,14.56653756,14.60865354,14.65292487,14.70647543,14.75635087,14.82100212,14.84862799,14.8956765,14.94712102,14.99170484,15.02392461,15.08179888,15.1223348,15.17582154,15.23233469,15.2773787,15.34740513,15.38951869,15.4724516,15.49621661,15.53351552,15.55632073]
					@AlImg = @spliner_aluminio_img[@alImg_aux].to_c

			    end		 
			 
			 @AL = Complex(@AlReal, @AlImg)
			 return @AL

			  when 'Li'

			 @LiReal_aux = onda
			 @LiImg_aux = onda
			 @spliner_litio = Spliner::Spliner.new [116.4,119.8,123.4,127.2,131.2,135.5,140.1,145,150.3,156,162.1,168.7,175.9,183.7,192.2,201.6,211.9,223.4,236.2,250.5,266.6,285,306.1,330.6,359.4,393.6,435,486.2,551,635.8,751.4,918.4,1181,1653,2755,8266], [0.753,0.743,0.726,0.708,0.68,0.657,0.624,0.601,0.572,0.546,0.518,0.492,0.466,0.44,0.408,0.377,0.342,0.31,0.299,0.302,0.317,0.334,0.346,0.345,0.335,0.305,0.248,0.217,0.205,0.221,0.265,0.338,0.448,0.561,0.662,0.366]
			 @LiReal = @spliner_litio[@LiReal_aux].to_c

			 
			 @spliner_Litio_img = Spliner::Spliner.new [116.4,119.8,123.4,127.2,131.2,135.5,140.1,145,150.3,156,162.1,168.7,175.9,183.7,192.2,201.6,211.9,223.4,236.2,250.5,266.6,285,306.1,330.6,359.4,393.6,435,486.2,551,635.8,751.4,918.4,1181,1653,2755,8266], [0.081,0.102,0.108,0.119,0.13,0.144,0.164,0.189,0.213,0.246,0.282,0.32,0.364,0.407,0.46,0.522,0.594,0.688,0.795,0.906,1.01,1.11,1.21,1.32,1.45,1.6,1.82,2.11,2.48,2.94,3.54,4.36,5.58,7.68,12.6,38]
			 @LiImg = @spliner_Litio_img[@LiImg_aux].to_c
			 @Li = Complex(@LiReal, @LiImg)

			 return @Li

			when 'Fe'

			 @FeReal_aux = onda
			 @spliner_ferro = Spliner::Spliner.new [188,192,195,199,203,207,212,216,221,226,231,237,243,249,255,262,269,276,284,292,301,311,320,332,342,354,368,381,397,413,431,451,471,496,521,549,582,617,659,704,756,821,892,984,1088,1216,1393,1610,1937], [1.29,1.35,1.42,1.45,1.47,1.49,1.47,1.47,1.47,1.47,1.48,1.48,1.5,1.51,1.53,1.56,1.59,1.62,1.64,1.65,1.67,1.69,1.74,1.78,1.85,1.93,2.02,2.12,2.24,2.35,2.48,2.59,2.67,2.74,2.86,2.95,2.94,2.88,2.92,2.86,2.87,2.94,2.96,2.92,2.97,3.03,3.09,3.11,3.17]
			 @FeReal = @spliner_ferro[@FeReal_aux].to_c

			 @FeImg_aux = onda
			 @spliner_ferro_img = Spliner::Spliner.new [188,192,195,199,203,207,212,216,221,226,231,237,243,249,255,262,269,276,284,292,301,311,320,332,342,354,368,381,397,413,431,451,471,496,521,549,582,617,659,704,756,821,892,984,1088,1216,1393,1610,1937], [1.35,1.37,1.39,1.4,1.4,1.41,1.43,1.44,1.47,1.49,1.53,1.57,1.61,1.66,1.7,1.75,1.79,1.84,1.88,1.94,2,2.06,2.12,2.19,2.27,2.35,2.43,2.5,2.58,2.65,2.71,2.77,2.82,2.88,2.91,2.93,2.99,3.05,3.1,3.19,3.28,3.39,3.56,3.79,4.06,4.39,4.83,5.39,6.12]
			 @FeImg = @spliner_ferro_img[@FeImg_aux].to_c
			 @Fe = Complex(@FeReal, @FeImg)
			return @Fe

			when 'In'
			 @InReal_aux = onda
			 @spliner_indium = Spliner::Spliner.new [550,600,650,700,720,740,750,760,800,850,900,950,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2100,2200,2400,2500,2600,3000,3500,4000,5000,6000,8000,10000], [0.7,0.795,0.9,1.01,1.07,1.13,1.17,1.19,1.32,1.45,1.59,1.72,1.81,1.84,1.87,1.95,2.06,2.19,2.33,2.49,2.64,2.8,2.97,3.13,3.3,3.65,3.81,4,4.7,6,7.27,9.77,12.4,18.4,24.8]
			 @InReal = @spliner_indium[@InReal_aux].to_c

			 @InImg_aux = onda
			 @spliner_indium_img = Spliner::Spliner.new [550,600,650,700,720,740,750,760,800,850,900,950,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2100,2200,2400,2500,2600,3000,3500,4000,5000,6000,8000,10000], [4.7,5.02,5.42,5.83,6,6.18,6.26,6.31,6.6,6.78,7.18,7.44,7.77,8.38,9,9.7,10.35,11,11.8,12.5,13.1,13.8,14.5,15,15.6,16.9,17.6,18.3,20.9,23.9,26.7,32.2,37.2,45.3,51.9]
			 @InImg = @spliner_indium_img[@InImg_aux].to_c
			 @In = Complex(@InReal, @InImg)
			return @In

		when 'Pt'
			 @PtReal_aux = onda
			 @PtImg_aux = onda

			 if (@PtReal_aux >= 0 && @PtImg_aux >= 0) && (@PtReal_aux <= 3.899 && @PtImg_aux <= 3.899) then
			 		
			 @spliner_platina = Spliner::Spliner.new [0.6199,0.6351,0.6508,0.6665,0.6831,0.6997,0.7166,0.7344,0.7523,0.7705,0.7897,0.8087,0.8287,0.8492,0.8694,0.8906,0.9129,0.935,0.9581,0.9816,1.005,1.03,1.055,1.081,1.108,1.134,1.162,1.191,1.22,1.25,1.281,1.312,1.343,1.376,1.41,1.445,1.479,1.516,1.554,1.592,1.631,1.671,1.71,1.754,1.797,1.839,1.884,1.931,1.977,2.026,2.077,2.127,2.179,2.234,2.287,2.344,2.403,2.46,2.52,2.584,2.643,2.707,2.774,2.844,2.91,2.987,3.054,3.131,3.212,3.289,3.369,3.453,3.532,3.625,3.712,3.803,3.899], [0.999528,0.999476,0.999421,0.999377,0.999332,0.999288,0.999243,0.999197,0.99915,0.999102,0.999051,0.999,0.99895,0.99889,0.99883,0.99877,0.99871,0.99865,0.99858,0.99851,0.99844,0.99837,0.99829,0.99821,0.99813,0.99805,0.99796,0.99787,0.99778,0.99769,0.99759,0.99749,0.99739,0.99729,0.99718,0.99707,0.99697,0.99686,0.99675,0.99666,0.99656,0.99646,0.99634,0.9962,0.99606,0.99592,0.99579,0.99566,0.99555,0.99544,0.99532,0.99521,0.99513,0.99508,0.99498,0.99482,0.99458,0.99437,0.99419,0.994,0.99384,0.99368,0.99353,0.99338,0.99324,0.9931,0.993,0.99293,0.9929,0.99284,0.99273,0.9926,0.99246,0.99228,0.99212,0.99193,0.99174]
			 @PtReal = @spliner_platina[@PtReal_aux].to_c

			 @spliner_platina_img = Spliner::Spliner.new [0.6199,0.6351,0.6508,0.6665,0.6831,0.6997,0.7166,0.7344,0.7523,0.7705,0.7897,0.8087,0.8287,0.8492,0.8694,0.8906,0.9129,0.935,0.9581,0.9816,1.005,1.03,1.055,1.081,1.108,1.134,1.162,1.191,1.22,1.25,1.281,1.312,1.343,1.376,1.41,1.445,1.479,1.516,1.554,1.592,1.631,1.671,1.71,1.754,1.797,1.839,1.884,1.931,1.977,2.026,2.077,2.127,2.179,2.234,2.287,2.344,2.403,2.46,2.52,2.584,2.643,2.707,2.774,2.844,2.91,2.987,3.054,3.131,3.212,3.289,3.369,3.453,3.532,3.625,3.712,3.803,3.899],[0.000123,0.000133,0.000143,0.000154,0.000167,0.00018,0.000194,0.000209,0.000225,0.000243,0.000262,0.000282,0.000304,0.000327,0.000351,0.000378,0.000406,0.000438,0.000472,0.000508,0.000546,0.000586,0.00063,0.000677,0.000729,0.000782,0.00084,0.000905,0.000972,0.00104,0.00112,0.0012,0.00128,0.00137,0.00147,0.00157,0.00168,0.00179,0.00192,0.00205,0.00216,0.00227,0.00238,0.00251,0.00266,0.00282,0.00299,0.00319,0.00337,0.00355,0.00376,0.00396,0.00417,0.00437,0.00446,0.00454,0.00474,0.00498,0.00523,0.00551,0.00578,0.00607,0.00638,0.0067,0.007,0.00737,0.0077,0.00808,0.00836,0.00859,0.00884,0.00909,0.00933,0.00962,0.00988,0.0102,0.0105]
			 @PtImg = @spliner_platina_img[@PtImg_aux].to_c

			 

			elsif (@PtReal_aux >= 3.900 && @PtImg_aux >= 3.900) && (@PtReal_aux <= 44 && @PtImg_aux <= 44) then
			 	
			 @spliner_platina = Spliner::Spliner.new [3.986,4.092,4.188,4.29,4.396,4.508,4.609,4.732,4.843,4.959,5.081,5.209,5.344,5.462,5.61,5.74,5.876,6.018,6.168,6.325,6.491,6.63,6.812,6.965,7.125,7.293,7.469,7.653,7.847,8.051,8.265,8.434,8.67,8.856,9.116,9.322,9.537,9.762,9.998,10.25,10.51,10.78,11.07,11.27,11.59,11.92,12.15,12.4,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44],[0.99155,0.9913,0.99108,0.99085,0.9906,0.99034,0.9901,0.9898,0.9896,0.9893,0.9891,0.9888,0.9885,0.9881,0.9877,0.9874,0.987,0.9866,0.9863,0.9859,0.9855,0.985,0.9841,0.9834,0.9826,0.9816,0.9805,0.9793,0.9778,0.9762,0.9745,0.973,0.9708,0.969,0.9661,0.9637,0.9612,0.9584,0.9553,0.9518,0.9481,0.9442,0.9399,0.9369,0.9322,0.9269,0.9232,0.919,0.885,0.892,0.893,0.892,0.887,0.882,0.877,0.869,0.86,0.881,0.872,0.862,0.85,0.838,0.826,0.81,0.794,0.779,0.762,0.742,0.722,0.696,0.674,0.663,0.675,0.687,0.699,0.709,0.718,0.722]
			 @PtReal = @spliner_platina[@PtReal_aux].to_c

			 @spliner_platina_img = Spliner::Spliner.new [3.986,4.092,4.188,4.29,4.396,4.508,4.609,4.732,4.843,4.959,5.081,5.209,5.344,5.462,5.61,5.74,5.876,6.018,6.168,6.325,6.491,6.63,6.812,6.965,7.125,7.293,7.469,7.653,7.847,8.051,8.265,8.434,8.67,8.856,9.116,9.322,9.537,9.762,9.998,10.25,10.51,10.78,11.07,11.27,11.59,11.92,12.15,12.4,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44],[0.0107,0.0111,0.0114,0.0117,0.012,0.0124,0.0127,0.0131,0.0135,0.0138,0.0141,0.0145,0.0148,0.0151,0.0154,0.0158,0.0161,0.0164,0.0168,0.0169,0.0169,0.0169,0.017,0.017,0.017,0.017,0.017,0.017,0.0171,0.0172,0.0174,0.0175,0.0177,0.0179,0.0181,0.0186,0.0192,0.0199,0.0206,0.0218,0.0234,0.025,0.0271,0.0287,0.0314,0.035,0.0377,0.0408,0.075,0.086,0.099,0.114,0.129,0.145,0.162,0.18,0.198,0.217,0.157,0.166,0.18,0.194,0.21,0.227,0.249,0.272,0.297,0.322,0.35,0.379,0.412,0.447,0.502,0.551,0.581,0.573,0.654,0.579]
			 @PtImg = @spliner_platina_img[@PtImg_aux].to_c

			 

			elsif (@PtReal_aux >= 44.001 && @PtImg_aux >= 44.001) && (@PtReal_aux <= 203.3 && @PtImg_aux <= 203.3) then
			 	
			 @spliner_platina = Spliner::Spliner.new [45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,72,74,76,78,80,82,84,86,88,90,92,94,96,98,100,102,104,106,108,110,112,114,116,118,120,122,124,126,128,130,132,134,136,138,140,142,144,146,148,150,155,160,165,170,175,180,185,190,195,200,200,203.3],[0.722,0.719,0.717,0.714,0.712,0.709,0.709,0.711,0.713,0.72,0.729,0.744,0.763,0.792,0.812,0.835,0.862,0.887,0.914,0.937,0.957,0.975,0.993,1.01,1.02,1.03,1.04,1.05,1.06,1.07,1.08,1.09,1.1,1.12,1.14,1.16,1.18,1.2,1.19,1.17,1.19,1.22,1.22,1.23,1.24,1.24,1.25,1.27,1.28,1.3,1.32,1.35,1.37,1.4,1.42,1.43,1.43,1.43,1.43,1.43,1.45,1.46,1.46,1.47,1.47,1.47,1.46,1.46,1.48,1.49,1.46,1.42,1.38,1.34,1.3,1.24,1.39,1.38]
			 @PtReal = @spliner_platina[@PtReal_aux].to_c

			 @spliner_platina_img = Spliner::Spliner.new [45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,72,74,76,78,80,82,84,86,88,90,92,94,96,98,100,102,104,106,108,110,112,114,116,118,120,122,124,126,128,130,132,134,136,138,140,142,144,146,148,150,155,160,165,170,175,180,185,190,195,200,200,203.3],[0.605,0.622,0.637,0.656,0.679,0.718,0.766,0.797,0.826,0.845,0.867,0.887,0.905,0.924,0.935,0.938,0.937,0.93,0.924,0.915,0.9,0.884,0.869,0.856,0.837,0.826,0.809,0.817,0.835,0.85,0.866,0.878,0.891,0.899,0.903,0.912,0.919,0.93,0.942,0.957,0.981,1.01,1.02,1.04,1.05,1.07,1.09,1.11,1.13,1.15,1.17,1.18,1.18,1.17,1.17,1.16,1.15,1.15,1.14,1.14,1.15,1.15,1.15,1.15,1.15,1.15,1.16,1.22,1.21,1.22,1.23,1.23,1.24,1.26,1.29,1.34,1.35,1.37]
			 @PtImg = @spliner_platina_img[@PtImg_aux].to_c

			 

			elsif (@PtReal_aux >= 203.31 && @PtImg_aux >= 203.31) && (@PtReal_aux <= 12400 && @PtImg_aux <= 12400) then 

			@spliner_platina = Spliner::Spliner.new [205,206.6,210.1,213.8,221.4,229.6,238.4,248,258.3,269.5,281.8,295.2,310,317.9,326.3,335.1,344.4,354.2,364.7,375.7,387.4,399.9,413.3,427.5,442.8,459.2,476.9,495.9,516.6,539,563.6,590.4,619.9,652.5,688.8,729.3,774.9,826.5,885.6,953.7,1033,1078,1127,1181,1240,1305,1378,1409,1459,1512,1550,1610,1653,1698,1771,1823,1907,1968,2066,2175,2254,2384,2455,2638,2755,2883,3100,3263,3542,3875,4133,4428,4959,5636,6199,7293,8266,9537,12400],[1.19,1.38,1.37,1.36,1.36,1.36,1.36,1.36,1.38,1.39,1.43,1.45,1.49,1.51,1.53,1.56,1.58,1.6,1.62,1.65,1.68,1.72,1.75,1.79,1.83,1.87,1.91,1.96,2.03,2.1,2.17,2.23,2.3,2.38,2.51,2.63,2.76,2.92,3.1,3.29,3.55,3.7,3.86,4.03,4.25,4.5,4.77,4.91,5.05,5.17,5.31,5.44,5.57,5.67,5.71,5.66,5.52,5.34,5.13,4.86,4.58,4.3,3.91,3.36,3.03,2.92,2.81,3.03,3.28,3.57,3.92,4.24,4.7,5.24,5.9,6.78,8.18,9.91,13.2]
			 @PtReal = @spliner_platina[@PtReal_aux].to_c

			 @spliner_platina_img = Spliner::Spliner.new [205,206.6,210.1,213.8,221.4,229.6,238.4,248,258.3,269.5,281.8,295.2,310,317.9,326.3,335.1,344.4,354.2,364.7,375.7,387.4,399.9,413.3,427.5,442.8,459.2,476.9,495.9,516.6,539,563.6,590.4,619.9,652.5,688.8,729.3,774.9,826.5,885.6,953.7,1033,1078,1127,1181,1240,1305,1378,1409,1459,1512,1550,1610,1653,1698,1771,1823,1907,1968,2066,2175,2254,2384,2455,2638,2755,2883,3100,3263,3542,3875,4133,4428,4959,5636,6199,7293,8266,9537,12400],[1.4,1.4,1.43,1.47,1.54,1.61,1.67,1.76,1.85,1.95,2.04,2.14,2.25,2.32,2.37,2.42,2.48,2.55,2.62,2.69,2.76,2.84,2.92,3.01,3.1,3.2,3.3,3.42,3.54,3.67,3.77,3.92,4.07,4.26,4.43,4.63,4.84,5.07,5.32,5.61,5.92,6.08,6.24,6.44,6.62,6.77,6.91,6.95,6.98,7.01,7.04,7.04,7.02,6.95,6.83,6.73,6.66,6.7,6.75,6.89,7.14,7.4,7.71,8.4,9.31,10.3,11.4,12.5,13.7,14.9,16.2,17.7,19.4,21.5,24,27.2,31.2,36.7,44.7]
			 @PtImg = @spliner_platina_img[@PtImg_aux].to_c

			 end

			 
			 @Pt = Complex(@PtReal, @PtImg)

			return @Pt

			when 'Os'
			 @OsReal_aux = onda
			 @OsImg_aux = onda

			 if (@OsReal_aux >= 0 && @OsImg_aux >= 0) && (@OsReal_aux <= 2.026 && @OsImg_aux <= 2.026 ) then
			 		
			 @spliner_osmio = Spliner::Spliner.new [0.6199,0.6351,0.6508,0.6665,0.6831,0.6997,0.7166,0.7345,0.7523,0.7705,0.7897,0.8087,0.8287,0.8492,0.8694,0.8906,0.9129,0.935,0.9581,0.9816,1.005,1.03,1.055,1.081,1.108,1.134,1.162,1.191,1.22,1.25,1.281,1.312,1.343,1.376,1.41,1.445,1.479,1.516,1.554,1.592,1.631,1.671,1.71,1.754,1.797,1.839,1.884,1.931,1.977],[0.999626,0.999529,0.999467,0.999405,0.99935,0.999297,0.999245,0.999192,0.99914,0.999087,0.999031,0.99898,0.99892,0.99886,0.9988,0.99873,0.99866,0.9986,0.99852,0.99845,0.99837,0.9983,0.99821,0.99813,0.99804,0.99795,0.99785,0.99775,0.99765,0.99755,0.99744,0.99733,0.99722,0.9971,0.99698,0.99686,0.99674,0.99661,0.99647,0.99634,0.9962,0.99607,0.99594,0.99579,0.99567,0.99556,0.99543,0.99527,0.99509]
			 @OsReal = @spliner_osmio[@OsReal_aux].to_c

			 @spliner_osmio_img = Spliner::Spliner.new [0.6199,0.6351,0.6508,0.6665,0.6831,0.6997,0.7166,0.7345,0.7523,0.7705,0.7897,0.8087,0.8287,0.8492,0.8694,0.8906,0.9129,0.935,0.9581,0.9816,1.005,1.03,1.055,1.081,1.108,1.134,1.162,1.191,1.22,1.25,1.281,1.312,1.343,1.376,1.41,1.445,1.479,1.516,1.554,1.592,1.631,1.671,1.71,1.754,1.797,1.839,1.884,1.931,1.977],[0.000336,0.000125,0.000135,0.000145,0.000157,0.000169,0.000182,0.000196,0.000211,0.000228,0.000245,0.000264,0.000285,0.000306,0.000329,0.000354,0.000382,0.000411,0.000442,0.000476,0.000512,0.000549,0.000591,0.000635,0.000684,0.000734,0.000788,0.000849,0.000912,0.000977,0.00105,0.00112,0.0012,0.00129,0.00138,0.00147,0.00157,0.00168,0.0018,0.00192,0.00205,0.00218,0.00232,0.00248,0.00264,0.0028,0.00291,0.00303,0.00319]
			 @OsImg = @spliner_osmio_img[@OsImg_aux].to_c

			elsif (@OsReal_aux >= 2.027 && @OsImg_aux >= 2.027) && (@OsReal_aux <= 6.812 && @OsImg_aux <= 6.812) then

			 @spliner_osmio = Spliner::Spliner.new [2.077,2.127,2.179,2.234,2.287,2.344,2.403,2.46,2.52,2.583,2.643,2.707,2.774,2.844,2.91,2.987,3.054,3.131,3.212,3.259,3.369,3.453,3.532,3.625,3.712,3.803,3.899,3.986,4.092,4.188,4.29,4.396,4.508,4.609,4.732,4.843,4.959,5.081,5.209,5.344,5.462,5.61,5.74,5.876,6.018,6.168,6.325,6.491,6.63,6.812],[0.99473,0.99456,0.9944,0.99422,0.99408,0.99395,0.99383,0.99369,0.99351,0.99331,0.9931,0.99286,0.99257,0.9923,0.99205,0.99179,0.99159,0.99139,0.99122,0.99101,0.99078,0.99054,0.9903,0.99002,0.9897,0.9895,0.9892,0.9889,0.9886,0.9883,0.988,0.9878,0.9876,0.9873,0.987,0.9867,0.9863,0.986,0.9855,0.9851,0.9846,0.9841,0.9836,0.9832,0.9827,0.982,0.9812,0.9804,0.9796,0.9785]
			 @OsReal = @spliner_osmio[@OsReal_aux].to_c

			 @spliner_osmio_img = Spliner::Spliner.new [2.077,2.127,2.179,2.234,2.287,2.344,2.403,2.46,2.52,2.583,2.643,2.707,2.774,2.844,2.91,2.987,3.054,3.131,3.212,3.259,3.369,3.453,3.532,3.625,3.712,3.803,3.899,3.986,4.092,4.188,4.29,4.396,4.508,4.609,4.732,4.843,4.959,5.081,5.209,5.344,5.462,5.61,5.74,5.876,6.018,6.168,6.325,6.491,6.63,6.812],[0.00358,0.00379,0.00401,0.00425,0.00448,0.00472,0.00493,0.00511,0.00529,0.00549,0.00568,0.00588,0.00614,0.00646,0.00678,0.00716,0.00749,0.00789,0.00823,0.00853,0.00885,0.0092,0.00952,0.00991,0.0103,0.0107,0.0111,0.0115,0.0119,0.0124,0.0128,0.0133,0.0138,0.0141,0.0145,0.0149,0.0153,0.0157,0.0161,0.0165,0.0169,0.0174,0.0178,0.0182,0.0185,0.0189,0.0192,0.0197,0.02,0.0205]
			 @OsImg = @spliner_osmio_img[@OsImg_aux].to_c
			 	
			elsif (@OsReal_aux >= 6.813 && @OsImg_aux >= 6.812) && (@OsReal_aux <= 42.46 && @OsImg_aux <= 42.46) then

		     @spliner_osmio = Spliner::Spliner.new [6.965,7.125,7.293,7.469,7.653,7.847,8.051,8.265,8.434,8.67,8.856,9.116,9.322,9.537,9.762,9.998,10.25,10.51,10.78,11.07,11.27,11.59,11.92,12.15,12.4,30,31,31.79,32,32.63,33,33.51,34,34.44,35,35.4,36,36.46,37,37.57,38,38.74,39,40,40,41,41.33,41.89,42,42.46],[0.9775,0.9764,0.9752,0.9741,0.9729,0.9717,0.9704,0.969,0.968,0.9665,0.9654,0.9639,0.9626,0.9619,0.9608,0.9593,0.9574,0.9552,0.9526,0.9495,0.9471,0.9431,0.9389,0.9356,0.9323,0.833,0.8,0.81,0.768,0.79,0.74,0.77,0.71,0.74,0.685,0.72,0.66,0.7,0.638,0.68,0.62,0.66,0.613,0.59,0.65,0.58,0.65,0.65,0.572,0.65]
			 @OsReal = @spliner_osmio[@OsReal_aux].to_c

			 @spliner_osmio_img = Spliner::Spliner.new [6.965,7.125,7.293,7.469,7.653,7.847,8.051,8.265,8.434,8.67,8.856,9.116,9.322,9.537,9.762,9.998,10.25,10.51,10.78,11.07,11.27,11.59,11.92,12.15,12.4,30,31,31.79,32,32.63,33,33.51,34,34.44,35,35.4,36,36.46,37,37.57,38,38.74,39,40,40,41,41.33,41.89,42,42.46],[0.0209,0.0213,0.022,0.0228,0.0236,0.0245,0.0254,0.0263,0.0271,0.0282,0.0291,0.0303,0.0313,0.0322,0.0324,0.0326,0.0329,0.0331,0.0334,0.0336,0.0338,0.0348,0.0359,0.0366,0.0374,0.127,0.138,0.26,0.155,0.26,0.173,0.27,0.193,0.29,0.222,0.31,0.247,0.34,0.279,0.37,0.315,0.41,0.35,0.39,0.45,0.432,0.49,0.51,0.473,0.53]
			 @OsImg = @spliner_osmio_img[@OsImg_aux].to_c

			elsif (@OsReal_aux >= 42.47 && @OsImg_aux >= 42.47) && (@OsReal_aux <= 65.25 && @OsImg_aux <= 65.25) then 

			 @spliner_osmio = Spliner::Spliner.new [43,43.65,44,44.28,45,45.58,46,46.26,46.96,47,47.68,48,48.43,49,49.99,50,50.81,51,51.66,52,52.53,53,53.44,54,54.38,55,55.35,56,56.35,57,57.4,58,58.48,59,59,59.61,60,60.18,60.77,61,61.38,62,62.62,63,63.26,63.91,64,64.57,65,65.25],[0.566,0.64,0.562,0.64,0.557,0.65,0.557,0.63,0.65,0.56,0.66,0.563,0.67,0.568,0.7,0.578,0.72,0.588,0.73,0.597,0.75,0.603,0.75,0.61,0.77,0.617,0.78,0.627,0.8,0.63,0.83,0.642,0.86,0.653,0.87,0.89,0.667,0.91,0.93,0.687,0.96,0.708,1.02,0.73,1.05,1.07,0.76,1.1,0.788,1.12]
			 @OsReal = @spliner_osmio[@OsReal_aux].to_c

			 @spliner_osmio_img = Spliner::Spliner.new [43,43.65,44,44.28,45,45.58,46,46.26,46.96,47,47.68,48,48.43,49,49.99,50,50.81,51,51.66,52,52.53,53,53.44,54,54.38,55,55.35,56,56.35,57,57.4,58,58.48,59,59,59.61,60,60.18,60.77,61,61.38,62,62.62,63,63.26,63.91,64,64.57,65,65.25],[0.513,0.57,0.552,0.59,0.59,0.62,0.627,0.66,0.69,0.662,0.72,0.695,0.75,0.739,0.8,0.76,0.82,0.79,0.84,0.817,0.86,0.842,0.88,0.868,0.9,0.89,0.93,0.91,0.96,0.932,0.99,0.952,1.02,0.973,1.04,1.05,0.992,1.07,1.09,1.01,1.1,1.03,1.11,1.05,1.11,1.11,1.06,1.1,1.08,1.1]
			 @OsImg = @spliner_osmio_img[@OsImg_aux].to_c


			elsif (@OsReal_aux >= 65.26 && @OsImg_aux >= 65.26) && (@OsReal_aux <= 89.84 && @OsImg_aux <= 89.84) then 
				
			 @spliner_osmio = Spliner::Spliner.new [65.95,66,66.66,67,67.38,68,68.12,68.8,69,69.65,70,70.44,71,71.25,72,72.08,72.93,73,73.8,74,74.69,75,75.6,76,76.5,77,77.49,78,78.47,79,79.47,80,80.51,81,81.57,82,82.65,83,83.77,84,84.92,85,86,86.1,87,87.31,88,88.56,89,89.84],[1.14,0.824,1.17,0.857,1.19,0.887,1.21,1.23,0.92,1.24,0.857,1.26,0.975,1.27,0.995,1.27,1.28,1.01,1.28,1.03,1.28,1.03,1.28,1.03,1.26,1.03,1.25,1.03,1.22,1.03,1.2,1.02,1.18,1.01,1.17,1.01,1.16,1.01,1.16,1,1.15,0.999,0.997,1.16,1,1.17,1,1.17,1.01,1.18]
			 @OsReal = @spliner_osmio[@OsReal_aux].to_c

			 @spliner_osmio_img = Spliner::Spliner.new [65.95,66,66.66,67,67.38,68,68.12,68.8,69,69.65,70,70.44,71,71.25,72,72.08,72.93,73,73.8,74,74.69,75,75.6,76,76.5,77,77.49,78,78.47,79,79.47,80,80.51,81,81.57,82,82.65,83,83.77,84,84.92,85,86,86.1,87,87.31,88,88.56,89,89.84],[1.1,1.08,1.09,1.09,1.08,1.08,1.06,1.04,1.08,1.03,1.09,1.01,1.05,1.01,1.03,0.97,0.96,1.01,0.94,0.993,0.92,0.963,0.9,0.942,0.88,0.922,0.87,0.908,0.86,0.902,0.86,0.902,0.87,0.908,0.89,0.918,0.9,0.927,0.91,0.935,0.93,0.945,0.953,0.94,0.963,0.96,0.972,0.96,0.983,0.96]
			 @OsImg = @spliner_osmio_img[@OsImg_aux].to_c

			elsif (@OsReal_aux >= 89.85  && @OsImg_aux >= 89.85) && (@OsReal_aux <= 151.20 && @OsImg_aux <= 151.20) then 
			
			 @spliner_osmio = Spliner::Spliner.new [90,91,91.16,92,92.52,93,93.92,94,95,95.37,96,96.86,97,98,98.4,99,99.98,100,101.6,103.3,105,105.1,106.9,108.8,110,110.7,112.7,114.8,115,117,119.2,120,120.4,121.6,124,125,126.5,129.2,130,131.9,134.8,135,137.8,140,140.9,144.2,145,147.6,150,151.2],[1.02,1.04,1.17,1.06,1.17,1.07,1.16,1.09,1.1,1.16,1.12,1.15,1.13,1.14,1.17,1.15,1.14,1.16,1.14,1.15,1.21,1.16,1.17,1.19,1.24,1.23,1.24,1.25,1.25,1.24,1.22,1.26,1.2,1.19,1.16,1.25,1.13,1.1,1.23,1.08,1.04,1.2,1.01,1.17,0.98,0.96,1.12,0.94,1.08,0.91]
			 @OsReal = @spliner_osmio[@OsReal_aux].to_c

			 @spliner_osmio_img = Spliner::Spliner.new [90,91,91.16,92,92.52,93,93.92,94,95,95.37,96,96.86,97,98,98.4,99,99.98,100,101.6,103.3,105,105.1,106.9,108.8,110,110.7,112.7,114.8,115,117,119.2,120,120.4,121.6,124,125,126.5,129.2,130,131.9,134.8,135,137.8,140,140.9,144.2,145,147.6,150,151.2],[0.993,1.01,0.97,1.01,0.97,1.03,0.98,1.04,1.05,0.99,1.06,1.01,1.07,1.08,1.12,1.09,1.03,1.1,1.06,1.08,1.14,1.1,1.12,1.15,1.17,1.14,1.13,1.11,1.18,1.1,1.08,1.18,1.08,1.08,1.1,1.19,1.11,1.14,1.23,1.16,1.19,1.27,1.24,1.33,1.29,1.34,1.41,1.4,1.49,1.48]
			 @OsImg = @spliner_osmio_img[@OsImg_aux].to_c

			elsif (@OsReal_aux >= 151.21  && @OsImg_aux >= 151.21 ) && (@OsReal_aux <= 399.90 && @OsImg_aux <= 399.9) then 
			
			 @spliner_osmio = Spliner::Spliner.new [155,155,159,160,163.1,165,167.5,170,172.2,175,177.1,180,182.3,185,187.9,190,193.7,195,200,200,206.6,210.1,213.8,217.5,221.4,225.4,229.6,233.9,238.4,243.1,248,253,258.3,263.8,269.5,275.5,281.8,288.3,295.2,302.4,310,317.9,326.3,335.1,344.4,354.2,364.7,375.7,387.4,399.9],[0.91,1.05,0.9,1.03,0.9,1.02,0.91,1.02,0.92,1.03,0.95,1.05,0.97,1.08,1.01,1.12,1.06,1.16,1.13,1.18,1.2,1.24,1.27,1.32,1.36,1.41,1.46,1.52,1.58,1.65,1.74,1.82,1.88,1.94,2.01,2.11,2.24,2.39,2.53,2.64,2.71,2.73,2.73,2.73,2.75,2.79,2.93,3.15,3.29,3.51]
			 @OsReal = @spliner_osmio[@OsReal_aux].to_c

			 @spliner_osmio_img = Spliner::Spliner.new [155,155,159,160,163.1,165,167.5,170,172.2,175,177.1,180,182.3,185,187.9,190,193.7,195,200,200,206.6,210.1,213.8,217.5,221.4,225.4,229.6,233.9,238.4,243.1,248,253,258.3,263.8,269.5,275.5,281.8,288.3,295.2,302.4,310,317.9,326.3,335.1,344.4,354.2,364.7,375.7,387.4,399.9],[1.55,1.59,1.63,1.69,1.72,1.78,1.81,1.88,1.91,1.97,2,2.05,2.11,2.13,2.21,2.2,2.33,2.27,2.44,2.33,2.54,2.6,2.65,2.71,2.77,2.83,2.88,2.94,3,3.07,3.12,3.15,3.19,3.24,3.31,3.38,3.44,3.47,3.44,3.4,3.34,3.31,3.32,3.37,3.45,3.59,3.79,3.88,3.96,4.21]
			 @OsImg = @spliner_osmio_img[@OsImg_aux].to_c

			elsif (@OsReal_aux >= 399.91 && @OsImg_aux >= 399.91) && (@OsReal_aux <= 12400 && @OsImg_aux <= 12400) then 	

			 @spliner_osmio = Spliner::Spliner.new [413.3,427.5,442.8,459.2,476.9,495.9,516.6,539,563.6,590.4,619.9,635.8,652.5,670.2,688.8,708.5,729.3,751.4,774.9,789.7,810.3,826.5,855,885.6,918.4,953.7,991.8,1033,1078,1127,1181,1240,1305,1378,1459,1550,1653,1771,1907,2066,2254,2480,2755,3100,3542,4133,4959,6199,8265,12400],[4.05,4.65,5.05,5.3,5.36,5.28,5.1,4.84,4.58,4.26,3.98,3.88,3.81,3.79,3.78,3.76,3.7,3.57,3.36,3.21,2.99,2.84,2.65,2.49,2.35,2.25,2.19,2.16,2.17,2.15,2.12,2.09,2.05,2.03,2.01,2,2,2.02,2.11,2.21,2.33,2.41,2.43,2.45,2.33,2.23,2.35,2.44,2.9,4.08]
			 @OsReal = @spliner_osmio[@OsReal_aux].to_c

			 @spliner_osmio_img = Spliner::Spliner.new [413.3,427.5,442.8,459.2,476.9,495.9,516.6,539,563.6,590.4,619.9,635.8,652.5,670.2,688.8,708.5,729.3,751.4,774.9,789.7,810.3,826.5,855,885.6,918.4,953.7,991.8,1033,1078,1127,1181,1240,1305,1378,1459,1550,1653,1771,1907,2066,2254,2480,2755,3100,3542,4133,4959,6199,8265,12400],[4.4,4.18,3.78,3.38,2.82,2.38,2.01,1.76,1.62,1.54,1.6,1.67,1.75,1.81,1.83,1.8,1.75,1.66,1.62,1.63,1.7,1.8,2.01,2.23,2.48,2.77,3.04,3.35,3.59,3.84,4.11,4.41,4.74,5.1,5.51,5.95,6.46,7.04,7.68,8.37,9.12,9.97,11,12.3,14.1,16.6,20,25.1,33.6,50.2]
			 @OsImg = @spliner_osmio_img[@OsImg_aux].to_c

			 end

			 @Os = Complex(@OsReal, @OsImg)

			return @Os

		 when 'Pd'

		   	@PdReal_aux = onda
		   	@PdImg_aux = onda

		   	if (@PdReal_aux >= 0  && @PdImg_aux >= 0) && (@PdReal_aux <= 349.30  && @PdImg_aux <= 349.30) then

		   		@spliner_paladim = Spliner::Spliner.new [190.7,192.2,193.7,195.3,196.8,198.4,200,201.6,203.3,204.9,206.6,208.4,210.1,211.9,213.8,215.6,217.5,219.4,221.4,223.4,225.4,227.5,229.6,231.7,233.9,236.2,238.4,240.7,243.1,245.5,248,250.5,253,255.6,258.3,261,263.8,266.6,269.5,272.5,275.5,278.6,281.8,285,288.3,291.7,295.2,298.8,302.4,306.1,310,313.9,317.9,322,326.3,330.6,335.1,339.7,344.4,349.3],[0.86,0.860125,0.859791,0.859225,0.858657,0.858313,0.858421,0.859211,0.860912,0.863749,0.867952,0.873749,0.881368,0.89,0.898834,0.907061,0.912512,0.916037,0.92,0.927097,0.935963,0.947027,0.959623,0.972195,0.983174,0.994888,1.009475,1.026261,1.043254,1.059154,1.074247,1.088259,1.10095,1.111815,1.120193,1.128302,1.138587,1.150213,1.162977,1.177215,1.188377,1.189116,1.184484,1.1795,1.178177,1.179386,1.185902,1.194739,1.202286,1.206782,1.209622,1.210985,1.210831,1.208071,1.203143,1.200141,1.205492,1.214491,1.222118,1.226471]
		   		@PdReal = @spliner_paladim[@PdReal_aux].to_c

		   		@spliner_paladim_img = Spliner::Spliner.new [190.7,192.2,193.7,195.3,196.8,198.4,200,201.6,203.3,204.9,206.6,208.4,210.1,211.9,213.8,215.6,217.5,219.4,221.4,223.4,225.4,227.5,229.6,231.7,233.9,236.2,238.4,240.7,243.1,245.5,248,250.5,253,255.6,258.3,261,263.8,266.6,269.5,272.5,275.5,278.6,281.8,285,288.3,291.7,295.2,298.8,302.4,306.1,310,313.9,317.9,322,326.3,330.6,335.1,339.7,344.4,349.3],[1.44,1.451825,1.464529,1.478066,1.492398,1.507478,1.523261,1.539707,1.556772,1.574412,1.592583,1.611243,1.630348,1.65,1.6703,1.691348,1.713744,1.736981,1.76,1.781156,1.801802,1.82264,1.843555,1.864033,1.883772,1.902695,1.920437,1.937142,1.953114,1.968473,1.983791,1.999843,2.016525,2.033492,2.050719,2.066947,2.079949,2.091251,2.102228,2.113892,2.127095,2.144085,2.163515,2.183935,2.204134,2.225536,2.249582,2.27483,2.299036,2.321571,2.34507,2.372288,2.401746,2.432239,2.463092,2.493892,2.523045,2.552083,2.582824,2.615891]
		   		@PdImg = @spliner_paladim_img[@PdImg_aux].to_c
		   	 
		   	 elsif (@PdReal_aux >= 349.31 && @PdImg_aux >= 349.3) && (@PdReal_aux <= 2480 && @PdImg_aux <= 2480) then
		   	
				@spliner_paladim = Spliner::Spliner.new [354.2,359.4,364.7,370.1,375.7,381.5,387.5,393.6,399.9,406.5,413.3,420.3,427.5,435,442.8,450.9,459.2,467.9,476.9,486.2,495.9,506.1,516.6,527.6,539.1,551,563.6,576.7,590.4,604.8,619.9,635.8,652.5,670.2,688.8,708.5,729.3,751.4,774.9,799.9,826.6,855.1,885.6,918.4,953.7,991.9,1033,1078,1127,1181,1240,1305,1378,1459,1550,1653,1771,1907,2066,2254,2480],[1.23,1.233242,1.237083,1.242453,1.250032,1.26,1.274589,1.290495,1.304579,1.316247,1.33,1.348044,1.363975,1.378884,1.393861,1.41,1.429733,1.451105,1.474894,1.499026,1.52,1.535173,1.558918,1.587592,1.617553,1.64347,1.658587,1.673256,1.69668,1.726494,1.75427,1.773848,1.792338,1.813076,1.837469,1.866608,1.902401,1.941803,1.974319,2.014081,2.082121,2.200538,2.311263,2.350159,2.349565,2.348156,2.400106,2.466173,2.540223,2.616123,2.679111,2.7293,2.787078,2.860724,2.949509,3.001193,3.038068,3.261611,3.8,3.945625,4.1]
		   		@PdReal = @spliner_paladim[@PdReal_aux].to_c

		   		@spliner_paladim_img = Spliner::Spliner.new [354.2,359.4,364.7,370.1,375.7,381.5,387.5,393.6,399.9,406.5,413.3,420.3,427.5,435,442.8,450.9,459.2,467.9,476.9,486.2,495.9,506.1,516.6,527.6,539.1,551,563.6,576.7,590.4,604.8,619.9,635.8,652.5,670.2,688.8,708.5,729.3,751.4,774.9,799.9,826.6,855.1,885.6,918.4,953.7,991.9,1033,1078,1127,1181,1240,1305,1378,1459,1550,1653,1771,1907,2066,2254,2480],[2.65,2.683969,2.718694,2.75466,2.791976,2.83,2.867824,2.906372,2.945794,2.986595,3.03,3.076908,3.122114,3.166867,3.212413,3.26,3.312518,3.367486,3.424637,3.482723,3.54,3.595024,3.654907,3.718661,3.785291,3.853351,3.92101,3.99105,4.066112,4.145302,4.225546,4.304184,4.385938,4.475248,4.571035,4.669277,4.767202,4.869014,4.978076,5.093524,5.214392,5.340015,5.472562,5.61212,5.761533,5.923958,6.103319,6.299675,6.514457,6.749094,7.00139,7.275399,7.583685,7.936326,8.332067,8.831679,9.410989,9.836403,9.96,10.63688,11.44]
		   		@PdImg = @spliner_paladim_img[@PdImg_aux].to_c
		   	
		   	end
			 
			 @PD = Complex(@PdReal, @PdImg)
			 puts "Resultado do Paladim #{@PD}"

			 return @PD

			 when 'Topas'
			 @Topas_aux1 = 0.000000000000326756760461952;
			 @Topas_aux2 = -0.00000000132078408375583;
			 @Topas_aux3 = 0.00000192107553405275;
			 @Topas_aux4 = -0.00120483299645556;
			 @Topas_aux5 = 1.80502703216582;
			 @TOPAS = @Topas_aux1 * (@comprimento_onda**4) + @Topas_aux2 * (@comprimento_onda**3) + @Topas_aux3 * (@comprimento_onda**2) + @Topas_aux4*@comprimento_onda + @Topas_aux5;
			 return @TOPAS

			when 'PC'
			 @PC_aux1 = 2.633127
			 @PC_aux2 = -0.07937823
			 @PC_aux3 = -0.1734506
			 @PC_aux4 = 0.08609268
			 @PC_aux5 = -0.01617892
			 @PC_aux6 = 0.001128933
			 @onda = @comprimento_onda * 0.001
			 #@PC = Math.sqrt(1 + @PC_aux1/(1 - @PC_aux2/(@onda**2)))
			 @PC = Math.sqrt(@PC_aux1 + @PC_aux2 * (@onda**2) + @PC_aux3*(@onda**(-2)) + @PC_aux4*(@onda**(-4)) + @PC_aux5*(@onda**(-6)) + @PC_aux6*(@onda**(-8)))
			 return @PC

			when 'PMMA'
			 @PMMA_aux1 = 2.399964
			 @PMMA_aux2 = -0.08308636
			 @PMMA_aux3 = -0.1919569
			 @PMMA_aux4 = 0.08720608
			 @PMMA_aux5 = -0.01666411
			 @PMMA_aux6 = 0.001169519
			 @onda = @comprimento_onda * 0.001
			 #@PMMA = Math.sqrt(1+@PMMA_aux1/(1-@PMMA_aux2/(@onda**2)) + @PMMA_aux3/(1 - @PMMA_aux4/(@onda**2)) + @PMMA_aux5/(1-@PMMA_aux6/(@onda**2)))
			 @PMMA = Math.sqrt(@PMMA_aux1 + @PMMA_aux2*(@onda**2) + @PMMA_aux3*(@onda**(-2)) + @PMMA_aux4*(@onda**(-4)) + @PMMA_aux5*(@onda**(-6)) + @PMMA_aux6*(@onda**(-8)))
			 return @PMMA
			
			when 'Agua'
			@B1_w = 0.566695982
			@B2_w = 0.1731900098
			@B3_w = 0.02095951857
			@B4_w = 0.1125228406
			@C1_w = 0.005084151894
			@C2_w = 0.01818488474
			@C3_w = 0.02625439472
			@C4_w = 10.73842352

			@water = Math.sqrt(1 + ((@B1_w*((@comprimento_onda*0.001)**2))/(((@comprimento_onda*0.001)**2) - @C1_w)) + ((@B2_w*((@comprimento_onda*0.001)**2))/(((@comprimento_onda*0.001)**2) - @C2_w)) + ((@B3_w*((@comprimento_onda*0.001)**2))/(((@comprimento_onda*0.001)**2) - @C3_w)) + ((@B4_w*((@comprimento_onda*0.001)**2))/(((@comprimento_onda*0.001)**2) - @C4_w)))
			return @water
			
			when 'Ar'
			  @Ar_aux1 = 0.05792105
			  @Ar_aux2 = 238.0185
			  @Ar_aux3 = 0.00167917
			  @Ar_aux4 = 57.362
			  @Ar = 1 + @Ar_aux1/(@Ar_aux2-(@comprimento_onda**(-2))) + @Ar_aux3/(@Ar_aux4 - (@comprimento_onda ** (-2)))
			return @Ar

		when 'Ciclo-Hexano'
			@A1_h = 1.41545
			@B1_h = 0.00369
			@C1_h = 0.00004
			@onda1 = onda
			@n_ciclo = @A1_h + @B1_h*((@onda1*0.001)**(-2)) + @C1_h *((@onda1*0.001)**(-4));
 				#puts @n_ciclo
		return @n_ciclo

		when 'Custom'
			@refracao = params['refracaos'].to_f
			  	 return @refracao		
	    end
	end

	def graficoSensograma

		@fusioncharts = Fusioncharts::Chart.new({
			  :type => 'zoomscatter',
			  :id => 'chartId',
			  :renderAt => 'chart-container1',
			  :width => '600',
			  :height => '350',
			  :dataFormat => 'json',
			  :dataSource => {
				:chart => {
				    #Theme
				    :theme => "fint",
				    :caption => "SENSOGRAMA",
				    :yaxisname => "Ângulo Minimo (º)",
				    :xaxisname => "Tempo (s)",
				    :xaxismaxvalue => "1000",
				    :xaxisminvalue => "0",
				    :yaxismaxvalue => "80",
				    :yaxisminvalue => "50",
				    :xnumberprefix => "",
				    :ynumbersuffix => "",
				    :showXAxisLine => "0",
				    :showformbtn => "1",
				    :formAction => "#",
				    :exportEnabled => "1",
				    :submitdataasxml =>  "1"
				},
				:categories => [{
				    :verticallinecolor => "666666",
				    :verticallinethickness => "1",
				    :alpha => "20",
				    :category => []
				}],
				:dataset => [{
				    :drawline => "0",
				    :color => "#3052DC",
				    :anchorsides => "2",
				    :anchorradius => "3",
				    :anchorbgcolor => "#3052DC",
				    :anchorbordercolor => "#3052DC",
				    :data => @valorSensograma
				     
				}]
			  }
			  
		    })
			  @fusioncharts.render()
		
	end


	  def mod (a,b)
		if (a != 0 || b != 0)
		    Math.sqrt(a*a + b*b)
	  	end	
	  end

	  def arg(a,b)
		Math.atan2(b,a)
	  end

	  def sqrt2(a,b)
		r = Math.sqrt(mod(a,b))
		theta = (arg(a,b)/(2))
		Complex(r*Math.cos(theta),r*Math.sin(theta))
	  end

end
